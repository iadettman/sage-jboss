![Screen Shot 2017-03-30 at 4.25.03 PM.png](https://bitbucket.org/repo/64KXq6A/images/3655562025-Screen%20Shot%202017-03-30%20at%204.25.03%20PM.png)
## JBoss Environment

### Modified sage jboss with fixes for server startup class definition not found errors

***

#### Configuration and Utility
* [**maven** config with server directory definition](https://bitbucket.org/iadettman/sage-jboss/wiki/MavenConfig.md)
* [**bash** helper scripts for sage and eonline](https://bitbucket.org/iadettman/sage-jboss/wiki/Bash.md)

#### Reference
* [**jboss** config documentation](https://bitbucket.org/iadettman/sage-jboss/wiki/JBoss.md)

#### Issues and Resolutions
* [blocked port on server startup](https://bitbucket.org/iadettman/sage-jboss/wiki/BlockedPort.md)
* [network settings](https://bitbucket.org/iadettman/sage-jboss/wiki/NetworkSettings.md)