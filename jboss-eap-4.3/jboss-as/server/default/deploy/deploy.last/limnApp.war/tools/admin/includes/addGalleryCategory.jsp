<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${not empty param.contentId}">
	<li id="category_${param.contentId}" class="gallery_entry_category">
		<input type="hidden" name="category" value="${param.contentId}" id="category"/>
		<a class="removelink" onclick="LimnGallery.DeleteCategory('${param.contentId}', '${param.index}')" href="javascript:;">[x]</a>
		${param.name}
	</li>
</c:if>