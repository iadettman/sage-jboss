var contextPath = '/limn';
var deletedBlocks = '';
var saving = false;
var reordering = false;
var blockOrderOriginalSequence = '';
var textblockid = '';
var uniqueCategory = true;
var uniqueEdition = true;
var uniqueProperty = true;
var uniqueFranchise = true;
var uniqueSEO = true;
var checkForEmptyMovieMeta = false;
var extraFunctions = '';
var spotsOriginalSequence = '';
var authorCount = -1;
var tinyMCEReplaceCharacters = '\u2005,&thinsp;,\u2011,&#45;,\u2013,&#8211;,\u2014,&#8212;,\u2015,&#151;,\u2017,&#95;,\u2019,&#39;,\u201b,&#145;,\u201c,&#34;,\u201d,&#34;,\u2026,&#8230;,\u2122,&#8482;,\u2212,&#8722;';

LimnAgency = {
	SetDefaultLicense: function(index) {
		var size = $('licenseSize').value;
		for (var i = 0; i <= size; i++) {
			if (i == index) {
				$('defaultLicense'+i).innerHTML = "<span class=\"license-status\">Default License</span>";
				if ($('licenseid'+i) != null) {
					$('defaultLicenseId').value = $('licenseid'+i).value;
				}
				else {
					$('defaultLicenseId').value = '';
				}
			}
			else if ($('defaultLicense'+i) != null) {
				$('defaultLicense'+i).innerHTML 
					= "<a href=\"javascript:;\" onclick=\"LimnAgency.SetDefaultLicense("
					+ i + ")\">Set as default license</a>";
			}
		}
	},
	UpdateLicenseValues: function() {
		var licenseid = $('licenseid').value;
		
		if (licenseid) {
			var chosenExpireDate = $('chosenExpireDate'+licenseid).value;
			var chosenAutoLockDate = $('chosenAutoLockDate'+licenseid).value;
			var sectionUsageCheckBoxes = document.getElementsByName("sectionUsage");
			var sectionUsage = $('siteUsage'+licenseid).value.split(",");
			
			for (var i = 0; i < sectionUsageCheckBoxes.length; i++) {
				sectionUsageCheckBoxes[i].checked = false;
				for (var k = 0; k < sectionUsage.length; k++) {
					if (sectionUsageCheckBoxes[i].value == sectionUsage[k]) {
						sectionUsageCheckBoxes[i].checked = true;
					}
				}
			}
			if (chosenExpireDate) {
				$('expirationDate').value = chosenExpireDate;
				var radios = document.getElementsByName("allowNullExpiration");
				for (var i = 0; i < radios.length; i++) {
					radios[i].checked = (radios[i].value == "no");
				}
			}
			else {
				var radios = document.getElementsByName("allowNullExpiration");
				for (var i = 0; i < radios.length; i++) {
					radios[i].checked = (radios[i].value == "yes");
				}
			}
			if (chosenAutoLockDate) {
				$('autoLockDate').value = chosenAutoLockDate;
				var radios = document.getElementsByName("allowNullAutoLock");
				for (var i = 0; i < radios.length; i++) {
					radios[i].checked = (radios[i].value == "no");
				}		
			}
			else {
				var radios = document.getElementsByName("allowNullAutoLock");
				for (var i = 0; i < radios.length; i++) {
					radios[i].checked = (radios[i].value == "yes");
				}
			}
		}
	},		
	UpdateLicense: function() {
		var agencyid = $('agencyid').value;
		if (agencyid && agencyid > 0) {
			new Ajax.Updater("agencyLicenseDiv", contextPath+'/admin/includes/chooseAgency.jsp?agencyId='+agencyid, {
				onComplete: function(transport) {
					LimnAgency.UpdateLicenseValues();
				}
			});
		}
		else {
			var newOption = document.createElement('option');
			newOption.text = 'None(Manual)';
			newOption.value = '';

			var licenseSelect = document.getElementById('licenseid');
			var i;
			for (i = licenseSelect.length - 1; i>=0; i--) {
				licenseSelect.remove(i);
			}
			try {
				licenseSelect.add(newOption, null); // standards compliant; doesn't work in IE
			}
			catch(ex) {
			   	licenseSelect.add(newOption, 0); // IE only
			}
		}
	},
	AddLicense: function(agencyId) { /* old validation logic, should be removed once all old validations are removed */
		if (confirmAction('addLicense') && valid.validate() && LimnAgency.IsValidExpirationDate()) {
			var agency;
			agency = 'agencyId=';
			LimnAgency.SaveAgency('published', '/admin/editAgency.jsp?'+'isCreateNewLicense=true&'+agency);
		}
	},
	AddLicenseNew: function(agencyId) {
		if (confirmAction('addLicense') && jQuery('#agencyForm').valid() && LimnAgency.IsValidExpirationDate()) {
			var agency;
			agency = 'agencyId=';
			LimnAgency.SaveAgency('published', '/admin/editAgency.jsp?'+'isCreateNewLicense=true&'+agency);
		}
	},
	SaveAgency: function(status, path) {
		var pars = $('agencyForm').serialize();
		pars += "&status=" + status;
		new Ajax.Request(contextPath+'/admin/includes/saveAgency.jsp', {
			method:'post', postBody:pars,
			onSuccess:  function(transport) {
				if (path != null) {
					window.location = contextPath+path+transport.responseText;
				}
				else {
					window.location = contextPath+'/admin/agencies.jsp';
				}	
			},
			on500: function(req) {
				alert('Fail!');
			}
		});			
	},
	FilterBySort: function(sortBy, status) {
		pars = 'status=' + status + '&sortBy=' + sortBy;
		new Ajax.Updater('agency_listings', contextPath+'/admin/includes/updateAgencyListings.jsp', {
			parameters: pars 
		});
	},
	UpdateStatus: function(agencyId, status) {
		if (confirmAction(status + 'Agency')) {
			new Ajax.Request(contextPath+'/admin/includes/updateAgencyStatus.jsp?id='+agencyId
				+ '&status=' + status, {
				onSuccess:  function(transport) {
					window.location = contextPath+'/admin/agencies.jsp';
				},
				on500: function(req) {
					alert('Fail!');
				}
			});	
		}
	},
	IsValidExpirationDate: function() {
		return true;
	}
};
LimnGalleryItem = {
	AddGalleryItems: function(galleryItems) {
		var newArryLength = galleryItems.split(";;").length;
		var size = $('galleryItemSize').value;
		$('galleryItemSize').value = size*1 + newArryLength*1;
		var newDiv = 'addedPhotosSeg' + $('galleryItemSize').value;
		$('addedPhotos').insert("<div id=\"" + newDiv + "\"" + "name=\"" + newDiv + "\" />");
		
		new Ajax.Updater(newDiv, contextPath+'/admin/includes/addGalleryItem.jsp?galleryItems='+galleryItems+'&size='+size, {
			onComplete: function(transport) {
				var i = size*1 + 1;
				for (; i <= $('galleryItemSize').value; ++i) {
					tinyMCE.idCounter=getEditorIndex();
					tinyMCE.execCommand('mceAddControl',false,'temp_photoCaption_seg'+i);											
				}
			}
		});		
	},
	SaveOrder: function(id, start) {
		LimnGalleryItem.DisableLinksByElement('gallery_item_entries');
		var pars = $('galleryForm').serialize();
		pars += '&start=' + start;
		new Ajax.Request(contextPath+'/admin/includes/saveGalleryItemsOrder.jsp', {
			method:'post', postBody:pars,
			onSuccess: function(transport) {
				window.location = contextPath+'/admin/editGallery.jsp?id='+id+'&galleryItemIndex='+start;
			},
			on500: function(req) {
				alert('Fail!');
			}
		});
	},		
	FilterByStartIndex: function(start, id) {
		LimnGalleryItem.DisableLinksByElement('gallery_item_entries');
		var pars = 'start=' + start + '&id=' + id;
		window.location = contextPath+'/admin/editGallery.jsp?id='+id+'&galleryItemIndex='+start;
	},
	DeleteBlock: function(galleryid, galleryitemid, block) {
		var pars = "galleryid=" + galleryid + "&galleryitemid=" + galleryitemid;
		if (confirmAction('deleteGalleryItem')) {
			deletePhotoBlock();
			$('primaryPhotoId_seg'+block).value = '';
			Element.hide('photoGallery_seg'+block);
			new Ajax.Request(contextPath+'/admin/includes/deleteGalleryItem.jsp', {
				asynchronous: true, method:'post', postBody:pars,
				onSuccess: function(transport) {
					document.getElementById('num_of_images').innerHTML = transport.responseText;
					document.getElementById('num_of_images_bottom').innerHTML = transport.responseText;
				}
			});			
		}
	},
	DisableLinksByElement: function(el) {
		  if (document.getElementById && document.getElementsByTagName) {
		    if (typeof(el) == 'string') {
		      el = document.getElementById(el);
		    }
		    var anchors = el.getElementsByTagName('a');
		    var buttons = el.getElementsByTagName('button');
		    var selects = el.getElementsByTagName('select');
		    for (var i=0, end=anchors.length; i<end; i++) {
		      anchors[i].onclick = function() {
		        return false;
		      };
		      anchors[i].className = "disabledlink";
		    }
		    for (var i=0, end=selects.length; i<end; i++) {
		    	selects[i].disabled = true;
		    }
		    for (var i=0, end=buttons.length; i<end; i++) {
		    	buttons[i].disabled = true;
		    	buttons[i].onclick = function() {
			        return false;
				};
		    }
		 }
	} 
};

LimnGallery = {
	DeleteCategory: function(id, index){
		$('category_'+id).remove(); 
	},
	AddCategory: function(el, li){
		if($('category_'+li.id)) {
			alert('You have already added "'+li.innerHTML+'" to this story');
		} else if (!li.id.match('[0-9]+')){
			LimnGallery.CreateCategory($('categorySearch').value.replace('Create new category: ', ''), li.id);
		} else {
			var pars = 'contentId='+li.id+'&name='+li.innerHTML+'&index='+$$('.gallery_entry_category').length;
			new Ajax.Updater('gallery_item_categories', contextPath+'/admin/includes/addGalleryCategory.jsp?'+pars, {
				asynchronous: true, 
				insertion: Insertion.Bottom,
				method: 'post', 
				evalScripts: true
			});
		}
		Field.clear('categorySearch');				
	},
	CreateCategory: function(name, edition){
		var pars = 'name='+name.replace(/&/g,'%26')+'&edition='+edition+'&index='+$$('.gallery_entry_category').length;
		new Ajax.Updater('gallery_item_categories',contextPath+'/admin/includes/createGalleryCategory.jsp?'+pars, {
			asynchronous: true,
			insertion: Insertion.Bottom
  		});
		Field.clear('categorySearch');	
	},
	DuplicateGallery: function(id, toEdition) {
		if (id && toEdition) {
			new Ajax.Request(contextPath+'/admin/includes/copyGallery.jsp?id='+id+'&toEdition='+toEdition, {
				onSuccess:  function(transport) {
					window.location = contextPath+'/admin/galleries.jsp?returnFeedback='+transport.responseText;
				},
				on500: function(req) {
					alert('Fail!');
				}
			});	
		}
		else {
			alert("Select an edition to copy the gallery to.");
		}
	},
	Filter: function() {
		var pars = $('galleriesListingForm').serialize();
		pars += '&' + $('galleriesDropDownForm').serialize();
		new Ajax.Updater('gallery_entries', contextPath+'/admin/includes/updateGalleryEntries.jsp', {
			parameters: pars, 
			onComplete: function(transport) {
				dojo.require('dojo.widget.DropdownDatePicker');
			}
		});
	},
	FilterBySort: function(sortBy, status, category) {
		var pars = $('galleriesListingForm').serialize();
		if (status && status != '(all)') {
			pars = pars + '&status=' + status ;
		}
		pars = pars + '&sortBy=' + sortBy + '&category' + category;
		new Ajax.Updater('gallery_entries', contextPath+'/admin/includes/updateGalleryEntries.jsp', {
			parameters: pars, 
			onComplete: function(transport) {
				dojo.require('dojo.widget.DropdownDatePicker');
			}
		});
	},
	FilterByStartIndex: function(start, status, sortBy, category) {
		var pars = $('galleriesListingForm').serialize();
		if (status && status != '(all)') {
			pars = pars + '&status=' + status ;
		}
		if (category && category != '(all)') {
			pars = pars + '&category=' + category;
		}
		pars = pars + '&start=' + start + '&sortBy=' + sortBy;
		new Ajax.Updater('gallery_entries', contextPath+'/admin/includes/updateGalleryEntries.jsp', {
			parameters: pars, 
			onComplete: function(transport) {
				dojo.require('dojo.widget.DropdownDatePicker');
			}
		});
	},
	PublishOrder: function(edition) {
		var pars = 'ids=' + Sortable.sequence('sortablelist');
		var returnFeedback = "The collection's new order has been published.";
		pars += "&edition=" + edition;
		new Ajax.Request(contextPath+'/admin/includes/publishGalleryOrder.jsp', {
			method:'post', postBody:pars,
			onSuccess:  function(transport) {
				if (transport.responseText == 0) {
					returnFeedback = "The new order was not published due to duplicate galleries in the order.";
				}
				window.location = contextPath+'/admin/galleries.jsp?returnFeedback='+returnFeedback;
			},
			on500: function(req) {
				alert('Fail!');
			}
		});
	},
	UpdateStatus: function(updateStatus, id, title, path) {
		if (!saving && confirmAction(updateStatus + 'Gallery')) {
			saving = true;
			Util.DisableHref();
			var pars = "updateStatus=" + updateStatus + "&id=" + id;
			var returnFeedback;
			if (updateStatus == 'published') {
				returnFeedback = "The gallery '" + title + "' has been published.";
			}
			else if (updateStatus == 'hidden') {
				returnFeedback = "The gallery '" + title + "' has been hidden. You can access it from the archives.";
			}
			else if (updateStatus == 'deleted') {
				returnFeedback = "The gallery '" + title + "' has been deleted.";
			}
			new Ajax.Request(contextPath+'/admin/includes/updateGalleryStatus.jsp', {
				method:'post', postBody:pars,
				onSuccess:  function(transport) {
					saving = false;
					if (path != null) {
						window.location = contextPath+path+'?returnFeedback='+returnFeedback;						
					}
					else {
						window.location = contextPath+'/admin/galleries.jsp?returnFeedback='+returnFeedback;
					}
				},
				on500: function(req) {
					saving = false;
					alert('Fail!');
				}
			});	
		}
	},
	AddAndPublish: function(id, title) {
		if (!saving && confirmAction('publishedGallery')) {
			saving = true;
			Util.DisableHref();
			var pars = "updateStatus=published&id=" + id;
			new Ajax.Request(contextPath+'/admin/includes/updateGalleryStatus.jsp', {
				method:'post', postBody:pars,
				onSuccess:  function(transport) {
					saving = false;
					window.location = contextPath+'/admin/galleries.jsp?reorder=true&reorderId='+id+'&reorderTitle='+title;
				},
				on500: function(req) {
					saving = false;
					alert('Fail!');
				}
			});			
		}
	},
	SaveGallery: function(path) { /* this is function uses old form validation, and should be remove once all pages are switched to jquery validation */
		if (jQuery('#galleryForm').valid() && !saving) {
			saving = true;
			Util.DisableHref();
			var pars = $('galleryForm').serialize();
			new Ajax.Request(contextPath+'/admin/includes/saveGallery.jsp', {
				method:'post', postBody:pars,
				onSuccess:  function(transport) {
					saving = false;
					window.location = contextPath+'/admin/'+path+'&id='+transport.responseText;
				},
				on500: function(req) {
					saving = false;
					alert('Fail!');
				}
			});			
		}
	},
	SaveGalleryNew: function(path) {
		if (jQuery('#galleryForm').valid() && !saving) {
			saving = true;
			Util.DisableHref();
			var pars = $('galleryForm').serialize();
			new Ajax.Request(contextPath+'/admin/includes/saveGallery.jsp', {
				method:'post', postBody:pars,
				onSuccess:  function(transport) {
					saving = false;
					window.location = contextPath+'/admin/'+path+'&id='+transport.responseText;
				},
				on500: function(req) {
					saving = false;
					alert('Fail!');
				}
			});			
		}
	},
	PopUp: function(URL, windowname) {
		var newFeatures = 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=740,height=800';
		var newWin = open('/limn/admin/chooseImage.jsp'+URL, windowname, newFeatures);		
		newWin.focus();
		return newWin;
	},
	RemoveImage: function(el, id) {
		// clear the hidden field that holds the image ID
		$(id).value = '';
		// clear the thumbnail (default image not used anymore)
		Element.update(el, '');
	}
};

LimnImage = {		
	ShowUsageDetail: function() {		
		var allDetails = jQuery('[name=usageDetailHTML]');
		var htmlContent = '';
		allDetails.each(function(){
			htmlContent += "<hr />";
			htmlContent += jQuery(this).html();
		});
		
		jQuery('#detailHTML').html(htmlContent);
	},
	ResetFields: function() {	
		document.forms[0].title.value = '' ;
		document.forms[0].sectionUse.value = 'all' ;
		document.forms[0].agency.value = '' ;
		document.forms[0].keyword.value = '' ;
		document.forms[0].filename.value = '' ;
		$('selectedWidth').value = '';
		$('selectedHeight').value = '';
		$('sizeComparison').value = 'equal';
		$('beginDate').value = '' ;
		$('endDate').value = '' ;
	},
	IsValidExpirationDate: function() {
		//The radio buttons must exist for allowNullExpiration and dojo drop down picker of expirationDate
		var doesNotExpire = $$('input:checked[type="radio"][name="allowNullExpiration"]').pluck('value');
		if (doesNotExpire && doesNotExpire == 'yes') {
			return true;
		}
		else if (Util.IsDate($('expirationDate').value)) {
			return true;
		}
		return false;
	},
	IsValidAutoLockDate: function() {
		//The radio buttons must exist for allowNullAutoLock and dojo drop down picker of autoLockDate
		var doesNotLock = $$('input:checked[type="radio"][name="allowNullAutoLock"]').pluck('value');
		if (doesNotLock && doesNotLock == 'yes') {
			return true;
		}
		else if (Util.IsDate($('autoLockDate').value)) {
			return true;
		}
		return false;
	},
	CreateImages: function() {
		if ($('uploadedImageList').getElementsByTagName("div").length <= 0) {
			alert("Upload at least one image.");
		}
		else {
			Util.DisableHref();
			var pars = $('imageForm').serialize();
			status="created";
			new Ajax.Request(contextPath+'/admin/includes/createImages.jsp', {
				method:'post', postBody:pars,
				onSuccess: function(transport) {
					window.location = contextPath+'/admin/images.jsp?processedStatus=\"'+status+'\"&processedTitle=\"'+transport.responseText+'\"';
				},
				on500: function(req) {
					alert('Fail!');
				}
			});
		}
	},
	LoadUploadedImage: function(imageStr, fileObj) {
		var imageStrArray = imageStr.split(";;");
		$('imageTotal').value = parseInt($('imageTotal').value) + 1;
		var newDivHTML = "<div class='photoblock active' id='imageDivSeg" + $('imageTotal').value + "'>";
		var newDivId = "imageDivSeg" + $('imageTotal').value;
		
		for (var index = 0; index < imageStrArray.length; ++index) {
			//Grab the img href information
			
			if (index == 0) {
				var indexOfForward = imageStrArray[index].lastIndexOf('\\');
				var indexOfBackward = imageStrArray[index].lastIndexOf('/');
				var indexOf = indexOfForward > indexOfBackward ? indexOfForward+1 : indexOfBackward+1;
				var fileName = imageStrArray[index].substring(indexOf);
				newDivHTML += "&nbsp;<input type=\"hidden\" name=\"filename\" id=\"filename\" "
					+ " value=\"" + fileName + "\" />";
				newDivHTML += "<img src=\"" + $('imageHost').value 
					+ imageStrArray[index] + "\" class=\"medium\" />";
				newDivHTML += "&nbsp;&nbsp;" + fileName;
				newDivHTML += "<img align=\"right\" border=\"0\" src=\"/limn/includes/images/cancel.png\"" 
					+ "onclick=\"$('" + newDivId + "').remove()\" />" + "<br />";
			}
			//Grab the width information
			if (index == 1) {
				newDivHTML += "&nbsp;<input type=\"hidden\" name=\"width\" id=\"width\" "
					+ " value=\"" + imageStrArray[index] + "\" />";
				newDivHTML += "&nbsp;" + imageStrArray[index] + "&nbsp;";
			}
			//Grab the height information
			if (index == 2) {
				newDivHTML += "&nbsp;<input type=\"hidden\" name=\"height\" id=\"height\" "
					+ " value=\"" + imageStrArray[index] + "\" />";
				newDivHTML += "x&nbsp;" + imageStrArray[index] + "&nbsp;px,&nbsp;";
			}
		}
		newDivHTML += fileObj.size + "B<br />";
		newDivHTML += "</div>";
		$('uploadedImageList').insert(newDivHTML);
	},
	Filter: function(isImageChooser, total) {
		var pars = $('imagesListingForm').serialize();
		if (isImageChooser == 'true') {
			pars = pars + '&total=' + total;
			new Ajax.Updater('image_entries', contextPath+'/admin/includes/updateImageChooser.jsp', {
				parameters: pars, 
				onComplete: function(transport) {
					initDragDropScript();
                    if (typeof createTooltips == 'function') {
                        createTooltips();
                    }
				}
			});		
		}
		else {
			new Ajax.Updater('image_entries', contextPath+'/admin/includes/updateImageEntries.jsp', {
				parameters: pars, 
				onComplete: function(transport) {
					dojo.require('dojo.widget.DropdownDatePicker');
                    if (typeof createTooltips == 'function') {
                        createTooltips();
                    }
				}
			});
		}
	},
	FilterByStatus: function(status) {
		if (status != '(all)') {
			document.getElementById("status").value = status;
		}
		else {
			document.getElementById("status").value = '';
		}
		var pars = $('imagesListingForm').serialize();
		
		new Ajax.Updater('image_entries', contextPath+'/admin/includes/updateImageEntries.jsp', {
			parameters: pars, 
			onComplete: function(transport) {
				dojo.require('dojo.widget.DropdownDatePicker');
			}
		});
	},
	FilterByStartIndex: function(start, status, sortBy, isImageChooser, total) {
		var pars = $('imagesListingForm').serialize();
		if (status && status != '(all)') {
			pars = pars + '&status=' + status ;
		}
		pars = pars + '&start=' + start + '&sortBy=' + sortBy;
		if (isImageChooser == 'true') {
			pars = pars + '&total=' + total;
			new Ajax.Updater('image_entries', contextPath+'/admin/includes/updateImageChooser.jsp', {
				parameters: pars, 
				onComplete: function(transport) {
					initDragDropScript();
                    if (typeof createTooltips == 'function') {
                        createTooltips();
                    }
				}
			});		
		}
		else {
			new Ajax.Updater('image_entries', contextPath+'/admin/includes/updateImageEntries.jsp', {
				parameters: pars, 
				onComplete: function(transport) {
					dojo.require('dojo.widget.DropdownDatePicker');
                    if (typeof createTooltips == 'function') {
                        createTooltips();
                    }
				}
			});
		}
	},
	FilterBySort: function(sortBy, status) {
		var pars = $('imagesListingForm').serialize();
		if (status && status != '(all)') {
			pars = pars + '&status=' + status ;
		}
		pars = pars + '&sortBy=' + sortBy;
		new Ajax.Updater('image_entries', contextPath+'/admin/includes/updateImageEntries.jsp', {
			parameters: pars, 
			onComplete: function(transport) {
				dojo.require('dojo.widget.DropdownDatePicker');
			}
		});
	},
	CheckAll: function() {
		count = document.getElementById('image_checkbox_form').elements.length;
		for (i=0; i < count; i++) 
		{
 		   document.getElementById('image_checkbox_form').elements[i].checked = 1; 
		}
	},
	UncheckAll: function() {
		count = document.getElementById('image_checkbox_form').elements.length;
		for (i=0; i < count; i++) 
		{
		   document.getElementById('image_checkbox_form').elements[i].checked = 0;
		}
	},
	UpdateImage: function(imageid, status, title, isLocked) {
		var pars = 'imageid=' + imageid + '&status=' + status + '&isLocked=' + isLocked;
		new Ajax.Request(contextPath+'/admin/includes/updateImage.jsp', {
			method:'post', postBody:pars,
			onSuccess:  function(transport) {
				window.location = contextPath+'/admin/images.jsp?processedStatus='+status+'&processedTitle='+title;
			},
			on500: function(req) {
				alert('Fail!');
			}
		});	
	},
	UpdateStatus: function(updateStatus) {
		var totalChecked = 0;
		var count = document.getElementById('image_checkbox_form').elements.length;
		var pars = $('image_checkbox_form').serialize();
		var feedback = "<dl id=\"feedback\" class=\"feedback_publish\" style=\"\">" 
			+ "<dt/> <dd> The following Image(s) have been " + updateStatus + ": </dd><dd> ";
		pars = pars + '&updateStatus=' + updateStatus;
		pars = pars + '&' + $('imagesListingForm').serialize();
		
		for (i=0; i < count; i++) 
		{
			if (document.getElementById('image_checkbox_form').elements[i].checked == 1) {
				feedback += "<img class='thumbnail' src='" 
					+ document.getElementById("imageSource" + (i+1)).href
					+ "' />&nbsp;" + document.getElementById("imageName" + (i+1)).innerHTML
					+ "<br />";
				totalChecked += 1;
			}
		}
		feedback += "<br/></dd></dl>";

		if (totalChecked <= 0) {
			alert("Select at least one image.");
		}
		else if (confirmAction(updateStatus + 'Image')) {
			new Ajax.Updater('image_entries', contextPath+'/admin/includes/updateImageStatus.jsp', {
				parameters: pars, 
				onComplete: function(transport) {
					$('responseDiv').innerHTML= feedback;
					dojo.require('dojo.widget.DropdownDatePicker');
				}
			});
		}
	}
};

function confirmAction(action) {
	switch(action){
		case "addLicense":
			promptAction = confirm('This requires an immediate save, do you want to continue?');
			break;		
		case "deleteLicense":
			promptAction = confirm('Are you sure you want to delete this license?');
			break;
		case "deletedAgency":
			promptAction = confirm('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\nWARNING:\nAre you sure you want to permanently DELETE this agency?\nThere is no undo for this action.\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n');
			break;
		case "duplicateGallery":
			promptAction = confirm('Copy requires immediate save. Do you want to continue?');
			break;
		case "publishedImage":
			promptAction = confirm('Are you sure you want to PUBLISH this image?');
			break;
		case "deletedImage":
			promptAction = confirm('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\nWARNING:\nAre you sure you want to permanently DELETE this image?\nThere is no undo for this action.\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n');
			break;
		case "deleteGalleryItem":
			promptAction = confirm('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\nWARNING:\nAre you sure you want to permanently DELETE this gallery item?\nThere is no undo for this action.\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n');
			break;
		case "expiredImage":
			promptAction = confirm('Are you sure you want to EXPIRE this image?');
			break;
		case "bulkDelete":
			promptAction = confirm('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\nWARNING:\nAre you sure you want to permanently DELETE these items?\nThere is no undo for this action.\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n');
			break;
		case "bulkHide":
			promptAction = confirm('Are you sure you want to HIDE these items?');
			break;
		case "bulkUnhide":
			promptAction = confirm('Are you sure you want to UNHIDE these items?');
			break;
		case "deleteFromQueue":
			promptAction = confirm('Are you sure you want to DELETE this article from the queue?');
			break;
		case "addToQueue":
			promptAction = confirm('Are you sure you want to ADD this article to the Translation Queue?');
			break;
		case "copyGallery":
			promptAction = confirm('Copy requires immediate save. Do you want to continue?');
			break;
		case "publishedGallery":
			promptAction = confirm('Are you sure you want to PUBLISH this gallery? This will update on the live site immediately.');
			break;
		case "hiddenGallery":
			promptAction = confirm('Are you sure you want to HIDE this gallery? This will update on the live site immediately.');
			break;		
		case "deletedGallery":
			promptAction = confirm('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\nWARNING:\nAre you sure you want to permanently DELETE this gallery?\nThere is no undo for this action.\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n');
			break;					
		case "removeArticle":
			promptAction = confirm('Are you sure you want to REMOVE this article from this collection?');
			break;
	}
	return promptAction;
};
Util = {
	DisableHref: function() {
		var input = document.getElementsByTagName("a");
		var count = input.length;

		for(var i =0; i < count; i++){
			document.getElementsByTagName("a")[i].disabled = true;
			document.getElementsByTagName("a")[i].style.cursor='wait';
			document.getElementsByTagName("a")[i].onclick = function() {
		        return false;
		    };
		    document.getElementsByTagName("a")[i].className = "disabledlink";
		}
		return true;
	},
	IsDate: function(sDate) {
	   	var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
	   	if (re.test(sDate)) {
	      	var dArr = sDate.split("/");
	      	var d = new Date(sDate);
	      	return d.getMonth() + 1 == dArr[0] && d.getDate() == dArr[1] && d.getFullYear() == dArr[2];
	   	}
	   	else {
	    	  return false;
	   	}
	},
	/**
	* Returns the value of the selected radio button in the radio group, null if
	* none are selected, and false if the button group doesn't exist
	*
	* @param {radio Object} or {radio id} el
	* OR
	* @param {form Object} or {form id} el
	* @param {radio group name} radioGroup
	*/
	RadioButtonValue: function(el, radioGroup) {
	    if($(el).type && $(el).type.toLowerCase() == 'radio') {
	        var radioGroup = $(el).name;
	        var el = $(el).form;
	    } else if ($(el).tagName.toLowerCase() != 'form') {
	        return false;
	    }
	
	    var checked = $(el).getInputs('radio', radioGroup).find(
	        function(re) {return re.checked;}
	    );
	    return (checked) ? $F(checked) : null;
	},		
	EscapeQuotes: function(someStr) {
		return someStr.replace("'", "\'").replace('"', '\"');
	},
	DisableEnter: function(field, event) {
		var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		if (keyCode == 13) {
			return false;
		}
		else return true;
	},
	InputCount: function (el,val) {
		var countSpans = el.parentNode.getElementsByTagName('span');
		var valLength = countChar(val).toString();
	    if (countSpans[0]) {
	        Element.update(countSpans[0], valLength);
	        while (countChar(el.value) > countSpans[1].innerHTML) {
	        	el.value = el.value.substring(0, el.value.length-1);
	        }
	    }
	},
	OnLoadCharCount: function () {
		var fieldsToCount = [$('requiredTitle'), $('summary'), $('smallTitle'), $('smallBody'), $('promotedTitle'), $('promotedSubhead'), $('section'), $('subsection'), $('title'), $('comment'), $('keywords'), $('description'), $('onAirTitle'), $('heading'), $('subhead')];
		fieldsToCount.each( function(whichField){
			if (whichField) {
				Util.InputCount(whichField, whichField.value);
				new Form.Element.Observer(whichField, 0.1, Util.InputCount);
			}
		});
	},
	EnterPressed: function (e) {
	// Code adapted from Jennifer Madden
	// http://jennifermadden.com/162/examples/stringEnterKeyDetector.html
		var characterCode;
		if(e && e.which) {           // NN4 specific code
			e = e;
			characterCode = e.which;
		} else {
			characterCode = e.keyCode; // IE specific code
		}
		return (characterCode == 13); // Enter key is 13
	},
	SaveMOTD: function() {
		var pars = 'message='+$('message').value;
		new Ajax.Request(contextPath+'/admin/includes/jsp/saveMOTD.jsp', {
			method:'post', postBody:pars,
			on500: function(req) {
				alert('There was an issue saving the motd, please try again later.');
			},
			onSuccess:  function(transport) {
				window.location=contextPath+'/admin/motd.jsp';
			}
		});
	},
	SendBreakingNewsEmail: function() {
		if($('requiredSubject') && $('requiredSubject').value == '') {
			alert ('Please Choose a Subject');
			return false;
		} else if($('requiredBody') && $('requiredBody').value == '') {
			alert ('Please Enter Some Body Text');
			return false;
		} else if($('linkOnline') && $('linkOnline').value == '') {
			alert ('Please Enter a Link');
			return false;
		} else {
			var pars = $('breakingNewsEmailAlert').serialize();
			new Ajax.Request(contextPath+'/admin/includes/jsp/sendBreakingNewsEmail.jsp', {
				method:'post', postBody:pars,
				on500: function(req) {
					alert('There was an issue sending the breaking news email, please try again later.');
				},
				onSuccess:  function(transport) {
					window.location=contextPath+'/admin/breakingNewsEmail.jsp';
				}
			});
		}
	},
	InitializeParentMCE: function() {
		tinyMCE.init({
			button_tile_map : true,
			paste_replace_list : "\u2013,&#8211;,\u2014,&#8212;,\u2015,&#8213;,\u2019,&#39;,\u201c,&#34;,\u201d,&#34;,\u2026,&#8230;,\u2122,&#8482;,\u2212,&#8722;",
			auto_reset_designmode : true,
			cleanup : true,
			cleanup_on_startup : true,
			editor_selector : "initializeOnLoad",
			force_p_newlines : true,
			height: '90',
			invalid_elements : "font",
			mode : "textareas",
			plugins : "",
			strict_loading_mode : true,
			theme : "advanced",
			theme_advanced_buttons1 : "",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_statusbar_location : "",
			theme_advanced_toolbar_location : "",
			theme_advanced_toolbar_align : "left",
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : false,
			theme_advanced_path : "",
			valid_elements : "a[*],strong/b,em/i,strike,br,p,ol,ul,li,u,blockquote",
			width: '100%'
		});
	},
	InitializeStandardMCE: function() {
		tinyMCE.init({
			button_tile_map : true,
			cleanup_on_startup : true,
			convert_fonts_to_spans : false,
			languages : "en",
			mode : "none",
			paste_insert_word_content_callback : "CEG.util.convertWord",
			paste_replace_list : tinyMCEReplaceCharacters,
			plugins : "pdw,inlinepopups,paste,safari,searchreplace",
			remove_trailing_nbsp : true,
			theme : "advanced",
			width: '100%',
			height: '90',
			theme_advanced_buttons1 : "pdw_toggle,bold,italic,underline,strikethrough,separator,bullist,numlist,separator,indent,separator,anchor,link,unlink,separator,search,replace,selectall,separator,pastetext,separator,charmap"+extraFunctions,
			theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,forecolor",
			theme_advanced_buttons3 : "",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : false,
			theme_advanced_path : "",
			
			pdw_toggle_on : 1,
            pdw_toggle_toolbars : "2",
            
			setup: function(ed) {
				ed.onPaste.add(function(ed, e, o) {
					ed.setProgressState(true);
				});
				ed.onSetProgressState.add(function(ed, b) {
					if (b) {
						window.setTimeout("tinyMCE.activeEditor.setProgressState(false)",500);
					} else {
						var rl = tinyMCE.activeEditor.getParam("paste_replace_list", tinyMCEReplaceCharacters).split(',');
						var content = tinyMCE.activeEditor.getContent({format:'raw'}).replace(/<!(?:--[\s\S]*?--\s*)?>\s*/g,'');
						for (var i=0; i<rl.length; i+=2) content = content.replace(new RegExp(rl[i], 'gi'), rl[i+1]);
						tinyMCE.activeEditor.setContent(content,{format:'raw'});
					}
				});
				ed.onExecCommand.add(function(ed, cmd, ui, val) {
				});
			}
		});
	}
};
/*
 * CEG namespace
 */
if (typeof CEG == 'undefined' || !CEG) {
	var CEG = {};
};
CEG.namespace = function() {
	var a=arguments, o=null, i, j, d;
	for (i=0; i<a.length; i=i+1) {
		d=a[i].split('.');
		o=CEG;
		for (j=(d[0] == 'CEG') ? 1 : 0; j<d.length; j=j+1) {
			o[d[j]]=o[d[j]] || {};
			o=o[d[j]];
		}
	}
	return o;
};
CEG.namespace('lib.custom.prototype');
CEG.namespace('util');
CEG.lib.custom.prototype.methods = function() {
	return {
		include:function() {
			var customHash = {};
			for (i = 0; i < arguments.length;i++) {
				customHash[arguments[i]] = this[arguments[i]];
			}
			Element.addMethods(customHash);
		},
		vanished:function(element) {
			return $(element).getStyle('display') == 'none';
		},
		disappear:function(element) {
			$(element).setStyle({display:'none'});
			return element;
		},
		reappear:function(element) {
			$(element).setStyle({display:'block'});
			return element;
		},
		transmogrify:function(element) {
			element = $(element);
			element[element.vanished(element) ? 'reappear' : 'disappear'](element);
			return element;
		}
	}
};
CEG.util.handler = function(handlerType) {
	this.runtime = function(error, url, line) {
		var details =  'A JavaScript Error occurred while attempting to load this page.\n\n';
		details += 'Description: ' + error + '\n';
		details += 'URL: ' + url + '\n';
		details += 'Line: ' + line + '\n\n';
		details += 'Click OK to continue.\n\n';
		alert(details);
		return true;
	};
	return this[handlerType];
};
CEG.util.dropdown = function(theContainerID, theTriggerID, theMechanismID) {
	this.Container = $(theContainerID);
	this.Trigger = $(theTriggerID);
	this.Mechanism = $(theMechanismID);
	if(this.Container && this.Trigger && this.Mechanism) {
		this.Mechanism.disappear();
		this.Trigger.observe('click', this.Mechanism.transmogrify.bindAsEventListener(this.Mechanism));
		this.Mechanism.observe('mouseout', this.Mechanism.disappear.bindAsEventListener(this.Mechanism));
		this.Mechanism.observe('mouseover', this.Mechanism.reappear.bindAsEventListener(this.Mechanism));
		this.Container.reappear();
		return this;
	}
};
CEG.util.preventMultipleEditors = function() {
	if (YAHOO.util.Cookie.get('sessionActive') == 'active') {
		alert('There is already an existing session! Click "OK" to return to Admin Home.');
		location.href='/blogtools/admin/';
	} else {
		Event.observe(window,'load', function() {
			YAHOO.util.Cookie.set('sessionActive', 'active');
			Event.observe(window,'unload', function() { YAHOO.util.Cookie.remove('sessionActive'); });
			Event.observe(window,'close', function() { YAHOO.util.Cookie.remove('sessionActive'); });
		});
	}
};
CEG.util.getBreakingNewsForm = function() {
	var myBreakingNewsData = YAHOO.util.Cookie.getSubs('breakingForm');
	if(myBreakingNewsData){
	document.getElementById('requiredSubject').value = myBreakingNewsData.subject;
	document.getElementById('requiredBody').value = myBreakingNewsData.body;
	document.getElementById('linkOnline').value = myBreakingNewsData.linkonline;
	document.getElementById('linkMobile').value = myBreakingNewsData.linkmobile;
	}
};


CEG.util.setBreakingNewsForm = function(subject, bodytext, linkonline, linkmobile) {
 		var today = new Date();
		var monthname=new Array('January','February','March','April','May','June','July','August','September','October','November','December')
		var currentmonth = monthname[today.getMonth()];
 		var currentday = today.getDate();
 		var currentyear = today.getFullYear()+1;
		var expireDate = currentmonth + ' ' + currentday + ', ' + currentyear;
		YAHOO.util.Cookie.setSub('breakingForm', 'subject', subject, { expires: new Date(expireDate) });
		YAHOO.util.Cookie.setSub('breakingForm', 'body', bodytext, { expires: new Date(expireDate) });
		YAHOO.util.Cookie.setSub('breakingForm', 'linkonline', linkonline, { expires: new Date(expireDate) });
		YAHOO.util.Cookie.setSub('breakingForm', 'linkmobile', linkmobile, { expires: new Date(expireDate) });
};
CEG.util.convertWord = function(type, content) {
	switch (type) {
		// Gets executed before the built in logic performs it's cleanups
		case 'before':
			//content = content.toLowerCase(); // Some dummy logic
			//alert(content);
			break;
		// Gets executed after the built in logic performs it's cleanups
		case 'after':
			//alert(content);
			content = content.replace(/<!(?:--[\s\S]*?--\s*)?>\s*/g,'');
			//content = content.toLowerCase(); // Some dummy logic
			//alert(content);
			break;
		}
	return content;
};
CEG.authoring = function() {
	return {
		init:function() {
			try { document.execCommand('BackgroundImageCache', false, true); } catch(err) {}; // (needed for EditionDropdown, see below) Do this to avoid the IE 6 background images flicker bug <sigh />
			this.RuntimeHandler = new CEG.util.handler('runtime');
			Event.observe(window,'error',this.RuntimeHandler);
			this.customMethods = new CEG.lib.custom.prototype.methods();
			this.customMethods.include('vanished','disappear','reappear','transmogrify');
			this.EditionDropdown = new CEG.util.dropdown('new_edition_container','new_edition_indicator','new_edition_selector');
			return true;
		}
	}
}();

function countChar(string) {
	var count = 0;
	var patt = new RegExp("[a-z]|[A-Z]|[0-9]");
	var validNonAlphaNumeric = " ~`!@#$%^&*()_+-={}|:<>?[]\\;',./";
	
	for (var i=0; i < string.length; i++) {
		var curChar = string.charAt(i);
		
		if ((patt.test(curChar)) || (validNonAlphaNumeric.indexOf(curChar) >= 0)) {
			count++;			
		} else {
			count = count + 8;
		}
	}
	return count;
}

Event.observe(window, 'load', CEG.authoring.init);