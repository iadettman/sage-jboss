<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
     	<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}"  type="text/css">
	</head>
	<body class="mainpage">
		<div class="controls">
			<a class="btnsave btnsavedefault" target="_top" href="${pageContext.request.contextPath}/admin/galleries.jsp">Done</a>
			<a class="btnsave" target="_top" href="${pageContext.request.contextPath}/admin/editGallery.jsp?id=${param.id}">Continue Editing</a>
		</div>
	</body>
</html>