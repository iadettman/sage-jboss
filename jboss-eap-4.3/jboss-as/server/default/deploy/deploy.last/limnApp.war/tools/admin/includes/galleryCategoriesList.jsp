<%@ page import="com.ceg.online.toolbox.StringUtil"%>
<%@ page import="com.ceg.online.categories.beans.CategoryDisplayBean" %>
<%@ page import="com.ceg.online.categories.beans.CategoryTypeBean" %>
<%@ page import="com.ceg.online.categories.beans.CategoryAliasBean" %>
<%@ page import="com.ceg.online.api.editions.beans.EditionLU" %>
<%@ page import="com.ceg.online.api.editions.helpers.EditionHelper" %>
<%@ page import="com.ceg.online.toolbox.StringUtil"%>
<%@ page import="com.ceg.online.categories.helpers.HelperFactory" %>
<%@ page import="com.ceg.online.categories.beans.CategoryBean" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<tools:setEdition />
<getters:getDBProperty attributeName="currentSite" propertyName="tools.config.site"/>
<getters:getToolsSiteId attributeName="siteId" siteName="${currentSite.value}"/>
<%
	String edition = (String)request.getParameter("edition");
	Integer siteId = (Integer)request.getAttribute("siteId");
	edition = edition != null && !edition.trim().equals("") ? edition : (String)request.getAttribute("edition");
	EditionLU ed = null;
	List<CategoryBean> categories = null;
	List<String> categoryResultList = new ArrayList<String>();
	String searchName = request.getParameter("q");
	String searchKey = null;
	boolean showAddNew = true;
	
	ed = com.ceg.online.api.HelperFactory.getEditionHelper().getEdition(edition);
	if (!StringUtil.isNullOrEmpty(searchName)) {
    	categories = HelperFactory.getCategoryHelper().searchCategory(StringUtil.escapeJSAndHTML(searchName));
    	searchKey = StringUtil.createCategoryKey(searchName);
    }
    if (categories != null && categories.size() > 0) {
    	for (CategoryBean cat : categories) {
    		System.out.println("waht cat is it :: " + cat.getKey());
    		if (cat.getKey().equals(searchKey)) {
    			showAddNew = false;
    		}
    		List<CategoryTypeBean> catTypeList = HelperFactory.getCategoryTypeHelper().getCategoryTypes(cat.getId());
    		List<CategoryAliasBean> catAliasList = HelperFactory.getCategoryAliasHelper().getAliases(cat.getId());
    		List<CategoryBean> catRelatedList = HelperFactory.getCategoryHelper().getRelatedCategories(cat.getId());
    		CategoryDisplayBean display = HelperFactory.getCategoryDisplayHelper().getDisplay(cat.getId(), siteId, ed.getId());
    		//Use the string buffer to build the pipe delimited data to return to frontend
    		//format is categoryId|category title|category types|category edition|category displayname|category aliases|category related to|showAddNew
    		StringBuffer addTo = new StringBuffer(cat.getId() + "|");

    		addTo.append(cat.getName() + "|");
    		if (catTypeList != null && catTypeList.size() > 0) {
    			for (CategoryTypeBean catType : catTypeList) {
    				addTo.append(catType.getName() + ", ");
    			}
    			addTo = new StringBuffer(addTo.substring(0, addTo.lastIndexOf(",")));    			
    		}
    		addTo.append("|");
    		addTo.append(ed.getDescr() + "|");
    		if (display != null) {
    			addTo.append(display.getDisplayName());
    		}
    		addTo.append("|");
    		if (catAliasList != null && catAliasList.size() > 0) {
    			for (CategoryAliasBean catAlias : catAliasList) {
    				addTo.append(catAlias.getName() + ", ");
    			}
    			addTo = new StringBuffer(addTo.substring(0, addTo.lastIndexOf(",")));
    			
    		}
    		addTo.append("|");
    		if (catRelatedList != null && catRelatedList.size() > 0) {
    			for (CategoryBean relatedCat : catRelatedList) {
    				addTo.append(relatedCat.getName() + ", ");
    			}
    			addTo = new StringBuffer(addTo.substring(0, addTo.lastIndexOf(",")));
    		}
    		categoryResultList.add(addTo.toString());
    	} 		
    } else if (categories != null && categories.size() == 0){
    	categoryResultList.add("||||||"); // dummy record to trigger add new category	
    }
    showAddNew = request.isUserInRole("edit categories") ? showAddNew : false;
    request.setAttribute("categoryResultList", categoryResultList);
	request.setAttribute("showAddNew", showAddNew);
%>
<c:forEach items="${categoryResultList}" var="displayName">
${displayName}|${showAddNew}
</c:forEach>