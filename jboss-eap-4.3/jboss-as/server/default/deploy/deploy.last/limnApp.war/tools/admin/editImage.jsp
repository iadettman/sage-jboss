<%@ page import="com.ceg.online.limn.helpers.SiteHelper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<%
    pageContext.setAttribute("now", new java.util.Date());
    pageContext.setAttribute("imageHost", SiteHelper.getImageHost());
    pageContext.setAttribute("id", request.getParameter("imageId"));
%>
<tools:setEdition />
<html>
    <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<title>Edit Image</title>
	   	<script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/core/1.4.2/jquery-1.4.2.min.js?x=${version.value}"></script>
	   	<script type="text/javascript">
	   		jQuery.noConflict();
	   	</script>
	   	<script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/plugin/jquery.validate/1.7/jquery.validate.pack.js?x=${version.value}"></script>
	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/editImage.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototype.js?x=${version.value}"></script>     	
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,builder"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/lightbox.js?x=${version.value}"></script>
       	<script src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" type="text/javascript"></script>
 	   	<script src="${pageContext.request.contextPath}/includes/js/css_browser_selector.js?x=${version.value}" type="text/javascript"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/lightbox.css?x=${version.value}" type="text/css" media="screen" />
       	<link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
       	<link href="${pageContext.request.contextPath}/includes/css/validation.css?x=${version.value}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/dojo/dojo.js?x=${version.value}"></script>
  		<script language="Javascript">
       		dojo.require("dojo.widget.DropdownDatePicker");
     	</script>	
       	<style type="text/css">
			.dojoTabPaneWrapper {
		  		padding : 10px 10px 10px 10px;
			}
			.clean td { margin:0; padding:0; padding-top:0; padding-bottom:0;}
	   	</style>
	</head>
	<body class="mainpage editpage">
		<div class="topstrip"><!-- --></div>	
		<jsp:include page="includes/header.jsp" />
		<table cellpadding="0" cellspacing="0" border="0" class="headtable">
			<div class="head">
				<div class="head_fns">&nbsp;&nbsp;<a href="/blogtools/admin/">< Back to Admin Home</a><strong></strong></div>
			   	<h1>Edit Image</h1>			   
				<div class="clear"></div>
			</div>
		</table>
		<form name="imageForm" id="imageForm" method="post" action="${pageContext.request.contextPath}/updateImagePost/" enctype="multipart/form-data">
		<div class="mainbody">		    
		    <h2>Edit Image
			<p class="descr"><span class="req">*</span> required fields for publishing</p>
			</h2>
			<image:imageUpdate id="${id}" status="published" />
			<div class="controls">					
				<a class="newbtn smallbtn action-form-submit">Update Image</a>		
				&nbsp;&nbsp;OR&nbsp;&nbsp;<a href="images.jsp">Cancel</a>			
			</div>
		</div>
		</form>
	<jsp:include page="includes/footer.jsp" />
	</body>
</html>
