<%@ page import="java.util.Date" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="agency" tagdir="/WEB-INF/tags/custom/tools/agency"%>
<%
	String agencyId = request.getParameter("agencyId");	
	request.setAttribute("agencyId", agencyId);
	request.setAttribute("now", new Date());
%>
<getters:getAgencies attributeName="agencies" status="published" />
<getters:getAgencyById attributeName="chosenAgency" id="${agencyId}" />

<select name="agencyid" id="agencyid" onchange="LimnAgency.UpdateLicense()">
	<option value="">None(Manual)</option>				
	<c:forEach items="${agencies}" var="agency">
		<option value="${agency.id}" <c:if test="${agencyId eq agency.id}">selected</c:if>>${agency.title}</option>
	</c:forEach>
</select>
<select name="licenseid" id="licenseid" onchange="LimnAgency.UpdateLicenseValues()">
	<option value="">None(Manual)</option>
	<c:forEach items="${chosenAgency.agencyLicense}" var="agencyLicense">
		<option value="${agencyLicense.id}" <c:if test="${agencyLicense.defaultLicense}">selected</c:if>>
			${agencyLicense.title}
		</option>
	</c:forEach>
</select>	
<c:forEach items="${chosenAgency.agencyLicense}" var="agencyLicense">
	<agency:setDate attributeName="chosenExpireDate" date="${agencyLicense.expireDate}" 
		duration="${agencyLicense.expireDuration}" dateType="${agencyLicense.expireDateType}"/>
	<agency:setDate attributeName="chosenAutoLockDate" date="${agencyLicense.autoLockDate}" 
		duration="${agencyLicense.autoLockDuration}" dateType="${agencyLicense.autoLockDateType}"/>
	<input type="hidden" id="siteUsage${agencyLicense.id}" name="siteUsage${agencyLicense.id}"
			value="${agencyLicense.sectionUseStr}" />
	<input type="hidden" id="chosenExpireDate${agencyLicense.id}" name="chosenExpireDate${agencyLicense.id}"
			value="<fmt:formatDate value='${chosenExpireDate}' type='both' pattern='MM/dd/yyyy' />"/>
	<input type="hidden" id="chosenAutoLockDate${agencyLicense.id}" name="chosenAutoLockDate${agencyLicense.id}"
			value="<fmt:formatDate value='${chosenAutoLockDate}' type='both' pattern='MM/dd/yyyy' />"/>
</c:forEach>