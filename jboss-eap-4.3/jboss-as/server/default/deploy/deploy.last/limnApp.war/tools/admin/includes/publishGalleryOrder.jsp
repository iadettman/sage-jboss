<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ceg.online.limn.helpers.GalleryHelper" %>
<%
String ids = request.getParameter("ids");
String edition = request.getParameter("edition");
int returnStatus = 0;
if (ids != null && !ids.trim().equals("")) {
	String[] galleryids = ids.split(",");
	returnStatus = GalleryHelper.publishGalleryOrder(edition, galleryids);
}
request.setAttribute("returnStatus", returnStatus);
%>${returnStatus}