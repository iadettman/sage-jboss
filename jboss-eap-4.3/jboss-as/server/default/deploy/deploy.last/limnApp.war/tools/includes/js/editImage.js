// JavaScript Document

jQuery(document).ready(function(){
	
	var usageShown = false;
	
	var myValidator = jQuery('form#imageForm').validate({
		rules: {
			expirationDate: {
			  required: function(element) {
				return (jQuery('input[name="allowNullExpiration"]:checked').val() == 'no');
			  },
			  date: true
			},
			autoLockDate: {
			  required: function(element) {
				return (jQuery('input[name="allowNullAutoLock"]:checked').val() == 'yes');
			  },
			  date: true
			}
		},
		errorPlacement: function(error, element) {				
			if (jQuery(element).parents('.datepicker').length > 0) {				
				error.appendTo( jQuery(element).parents('.datepicker') );
			} else {
				element.after(error);
			}
		}
	});	
	
	jQuery('a.action-form-submit').click(function(){
		jQuery('form#imageForm').submit();												  
	});
	
	jQuery('a#image-usage-toggle-link').toggle(function() {
		if (!usageShown) {
			LimnImage.ShowUsageDetail();
			usageShown = true;
		} else {
			jQuery('#detailHTML').show();
		}
		jQuery(this).html('(hide detail)');
	}, function() {
	  jQuery('#detailHTML').hide();
	  jQuery(this).html('(show detail)');
	});
	
	enableAutoLockFields(!jQuery('#isLocked').attr('checked')); // scans once when page loads
	
	jQuery('#isLocked').change(function(){	 // scans when data changed
		enableAutoLockFields(!jQuery(this).attr('checked'));
	});
});

function enableAutoLockFields(isEnable){
	jQuery("tr#autoLock input, tr#autoLock select").each(function(){
		var field = jQuery(this);
		
		if (isEnable) {
			field.removeAttr('disabled');
		} else {
			field.attr('disabled','disabled');
		}
	});
}