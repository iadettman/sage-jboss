<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.ceg.online.limn.helpers.GalleryHelper" %>
<%@ page import="com.ceg.online.limn.beans.impl.ImageImpl" %>
<%@ page import="com.ceg.online.limn.beans.GalleryBean" %>
<%
String galleryitemid = request.getParameter("galleryitemid");
String galleryid = request.getParameter("galleryid");
if (galleryitemid != null && !galleryitemid.trim().equals("") 
		&& galleryid != null && !galleryid.trim().equals("")) {
	Integer id = new Integer(galleryid);
	GalleryBean gallery = GalleryHelper.getGalleryById(id);
	GalleryHelper.deleteGalleryItem(gallery.getId(), new Integer(galleryitemid));
	Long count = GalleryHelper.getGalleryImageCount(Integer.parseInt(galleryid));
	//Update gallery's status to EDITED if it is in PUBLISHED status
	if (gallery.getStatus().equals(GalleryHelper.PUBLISHED)) {
		GalleryHelper.updateStatus(gallery.getId(), GalleryHelper.EDITED);
	}
	request.setAttribute("count", count);
}
%>${count}