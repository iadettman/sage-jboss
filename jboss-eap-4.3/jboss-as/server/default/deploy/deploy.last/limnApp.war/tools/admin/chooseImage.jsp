<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%-- Struts provided Taglibs --%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<tools:setEdition />
<html>
    <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<title>Manage Section: E! Images</title>

	 	<%--<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/lightbox.css?x=${version.value}" type="text/css" media="screen" />--%>
       	<link href="${pageContext.request.contextPath}/includes/css/prototip.css?x=${version.value}" rel="stylesheet" type="text/css">
        <link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
       	<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/dragDrop.css?x=${version.value}" type="text/css" media="screen" />
        <link rel="stylesheet" href="http://scripts.comcastnets.net/client/ajax/jquery/ui/1.7.2/css/ui-lightness/jquery-ui-1.7.2.custom.css?x=${version.value}" type="text/css" media="screen" />

       	<style type="text/css">
			.dojoTabPaneWrapper {
		  		padding : 10px 10px 10px 10px;
			}
			.clean td { margin:0; padding:0; padding-top:0; }
	   	</style>
    </head>
<body class="mainpage imagepopup">
	<table cellpadding="0" cellspacing="0" border="0" class="headtable">
	  <tr>
	    <td>
			<div class="head">
				<div class="head_fns"><a href="javascript:saveDragDropNodes();window.close();" class="newbtn smallerbtn">Choose Image(s)</a>&nbsp;OR&nbsp;<a href="javascript:window.close();">Cancel</a><strong></strong></div>
				<h1>Choose image(s)</h1>
			</div>
	     </td>
	   </tr>
	</table>
	<c:set var="total" value="48" />
	<c:set var="status" value="published" />
	<getters:getImages attributeName="images" size="50" start="0" title="${title}" agency="${agency}" keyword="${keyword}" filename="${filename}"
	 	beginDate="${beginDate}" endDate="${endDate}" dateComparison="${dateComparison}"  width="${width}" height="${height}" sizeComparison="${sizeComparison}" status="${status}"/>
	<getters:getImages attributeName="imageSize" isCount="${true}" title="${title}" agency="${agency}" keyword="${keyword}" filename="${filename}" 
		beginDate="${beginDate}" endDate="${endDate}" dateComparison="${dateComparison}"  width="${width}" height="${height}" sizeComparison="${sizeComparison}" status="${status}"/>
	<image:filterImageHeader processedStatus="${param.processedStatus}" processedTitle="${param.processedTitle}" isImageChooser="${true}" total="${total}" status="${status}"/>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
	 	<td class="leftcol" style="background-color:white;">
	 		<div id="dragDropContainer">
				<form name="openWindow" id="openWindow" >
					<input type="hidden" name="lightboxStr" id="lightboxStr" />
					<input type="hidden" name="selectedPage" id="selectedPage" />
					<input type="hidden" name="currentPage" id="currentPage" value="${imageIndex}"/>
					<input type="hidden" name="callingForm" id="callingForm" value="${param.callingForm}"/>
					<input type="hidden" name="property" id="property" value="${param.property}"/>
					<input type="hidden" name="displayPreview" id="displayPreview" value="${param.displayPreview}"/>
					<input type="hidden" name="displayProperty" id="displayProperty"  value="${param.displayProperty}"/>
					<input type="hidden" name="selectedImagesStr" id="selectedImagesStr" value="${param.selectedImagesStr}"/>
					<input type="hidden" name="numImages" id="numImages" value="${param.numImages}"/>
					<input type="hidden" name="status" id="status" value="${status}"/>
					<input type="hidden" name="pageTo" id="pageTo" />
					<input type="hidden" name="numDisplayRecords" value="${total}" />
				</form>
		 		<div id="image_entries" >
		 			<image:imageChooserEntries images="${images}" size="${imageSize}" total="${total}" status="${status}" />
				</div>
	            <div id="mainContainer">
				<!-- ONE <UL> for each "image" -->
					<div class="">
						<p class="heading">Drag images below:</p>
						<ul id="box1" style="height: 3060px;" >
						</ul>
					</div>
				</div>
				<ul style="left:365px; top:318px; display:block;" id="dragContent"></ul>
				<div style="display:none; left:50px;" id="dragDropIndicator"></div>
		    	<div id="saveContent"></div>
	    	</div>
        <!--  END OF DRAG AND DROP -->
        </td>
        </tr>
    </table>

    <script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/core/1.4/jquery-1.4.min.js?x=${version.value}"></script>
    <script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/plugin/qTip/1.0.0-rc3/jquery.qtip-1.0.0-rc3.min.js?x=${version.value}"></script>
    <script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/ui/1.7.2/js/jquery-ui-1.7.2.custom.min.js?x=${version.value}"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function () {
            jQuery("#beginDate").datepicker({constrainInput: false, showOn: 'button', buttonImage: '/limn/includes/js/dojo/src/widget/templates/images/dateIcon.gif', buttonImageOnly: true});
            jQuery("#endDate").datepicker({constrainInput: false, showOn: 'button', buttonImage: '/limn/includes/js/dojo/src/widget/templates/images/dateIcon.gif', buttonImageOnly: true});
            jQuery('.ui-datepicker-trigger').attr('alt', '').attr('title', '');

            createTooltips();
        });

        function createTooltips() {
            jQuery("img.tooltip").each(function(){
                    jQuery(this).qtip({
                       content: jQuery(this).next(".tooltip-content").html(),
                       show: { 
						   	when: { 
						   		event: 'mouseover' 
						   	}, 
						   	delay: 1000 
                       },
                       hide: 'mouseout',
                       style: {
							width: 250,
                            padding: 5,
                            background: '#FFFFDD',
                            color: '#b4b4b4',
                            border: {
                                width: 2,
                                radius: 2,
                                color: '#CCCBD0'
                            }
                       },
                       position: { target: 'mouse' },
                       api: { beforeShow: function(){jQuery(".qtip").hide();} }
                    });
                }
            );
        }
    </script>
    <script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/prototype/1.6.1/prototype.js?x=${version.value}"></script>
    <%--<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototip.js?x=${version.value}"></script>--%>
    <%--<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,builder"></script>--%>
    <%--<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/lightbox.js?x=${version.value}"></script>--%>
    <script src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" type="text/javascript"></script>
    <%--<script src="${pageContext.request.contextPath}/includes/js/css_browser_selector.js?x=${version.value}" type="text/javascript"></script>--%>
    <script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/dragDropImages.js?x=${version.value}"></script>

</body>
</html>