<%@ page import="java.util.Date" %>
<%@ page import="com.ceg.online.limn.helpers.AgencyHelper" %>
<%@ page import="com.ceg.online.limn.dto.AgencyLU" %>
<%
String id = request.getParameter("id");
String updateStatus = request.getParameter("status");
AgencyLU agency;
Date currentDate = new Date();

if (id != null && !id.trim().equals("") && updateStatus != null) {
	agency = AgencyHelper.getAgencyById(new Integer(id));
	agency.setLastModDate(currentDate);
	if (updateStatus.equals(AgencyHelper.PUBLISHED)) {
		agency.setStatus(AgencyHelper.PUBLISHED);
	} 
	else if (updateStatus.equals(AgencyHelper.HIDDEN)) {
		agency.setStatus(AgencyHelper.HIDDEN);
	}
	else if (updateStatus.equals(AgencyHelper.DELETED)) {
		agency.setStatus(AgencyHelper.DELETED);
	}
	agency = AgencyHelper.saveAgency(agency);
	AgencyHelper.publishAgency(agency);
}
%>