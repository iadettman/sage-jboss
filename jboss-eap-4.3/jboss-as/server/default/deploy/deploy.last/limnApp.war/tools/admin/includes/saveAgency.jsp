<%@ page import="java.lang.String"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@ page import="com.ceg.online.limn.util.SaveUtil"%>
<%@ page import="com.ceg.online.limn.helpers.AgencyHelper"%>
<%
Log log = LogFactory.getLog("saveAgency.jsp");
int id = SaveUtil.saveAgency(request);
//Pulish the AgencyLU to the site db if it has been saved correctly
if (id > 0) {
	AgencyHelper.publishAgency(id);
}
request.setAttribute("id", id);
%>${id}