<%@ page import="java.lang.String"%>
<%@ page import="com.ceg.online.limn.helpers.GalleryHelper"%>
<%@ page import="com.ceg.online.limn.beans.GalleryBean"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%
String id = request.getParameter("id");
String toEdition = request.getParameter("toEdition");
Integer galleryId = id != null && !id.trim().equals("") ? new Integer(id) : null;
GalleryBean gallery = null;
GalleryBean newGallery = null;
Boolean isCopied = false;

if (galleryId != null && toEdition != null && !toEdition.trim().equals("")) {
	gallery = GalleryHelper.getGalleryById(galleryId);
	newGallery = GalleryHelper.copyGallery(gallery, toEdition);
	if (newGallery != null) {
		request.setAttribute("gallery", gallery);
		request.setAttribute("newGallery", newGallery);
		isCopied = true;
	}
}
else {
	isCopied = false;
}
request.setAttribute("isCopied", isCopied);
%>
<c:choose>
	<c:when test="${isCopied eq true}">
		Gallery ${gallery.title} has been copied.  The link to the new gallery is 
		<a href="${pageContext.request.contextPath}/admin/editGallery.jsp?id=${newGallery.id}">here.</a>
	</c:when>
	<c:otherwise>
		Gallery duplication failed.
	</c:otherwise>
</c:choose>