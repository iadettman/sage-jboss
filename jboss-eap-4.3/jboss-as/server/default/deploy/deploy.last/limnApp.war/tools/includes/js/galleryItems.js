function inputCount(el,val) {
	var countSpans = el.parentNode.getElementsByTagName('span');
    if (countSpans[0]) {
        Element.update(countSpans[0], val.length.toString());
        if (el.value.length > countSpans[1].innerHTML) {
            el.value = el.value.substring(0, countSpans[1].innerHTML)
        }
    }
}

function onLoadCharCount() {
	var fieldsToCount = [$("requiredTitle"), $("summary"), $("smallTitle"), $("smallBody"), $("promotedTitle"), $("promotedSubhead"), $("tvheadline"), $("requiredSubject"), $("requiredBody")];
	fieldsToCount.each( function(whichField){
		if (whichField) {
			inputCount(whichField, whichField.value);
			new Form.Element.Observer(whichField, 0.1, inputCount);
		}
	});
}

function trimAll(sString) {
	while (sString.substring(0,2) == ';;') {
		sString = sString.substring(2, sString.length);
	}
	while (sString.substring(sString.length-2, sString.length) == ';;') {
		sString = sString.substring(0,sString.length-2);
	}
	return sString;
}

function popUp(URL, windowname) {
	//window.open(URL, windowname, 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=300,height=300');
  	//URL = URL+'&clear=yes';
  	if(URL.indexOf("getDialogImagesListing.do?")!=-1) {
		URL =URL.replace('getDialogImagesListing.do?','getDialogImagesListing.do?filterExpired=true&');
    }
	var newFeatures = 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=740,height=800';
	var newWin = open(URL, windowname, newFeatures);		
	newWin.focus();
	return newWin;
}

var celebList = null;

function addStory(el,ref) {		
// check if adding a duplicate story
	if (!selectedStoriesList.match(ref)) {
// add story to display list
		var newItem = document.createElement('li');
		newItem.setAttribute('id', ref);
		newItem.innerHTML = '<a href="javascript:;" class="removelink" style="display:;" onclick="removeStory(this)">[x]</a><!-- <a href="javascript:;" class="itemlink">-->' + el + '<!-- </a> -->';
		$('relatedStoriesList').appendChild(newItem);
// increase count
		numRelatedStories++;
		$('storyCount').innerHTML = numRelatedStories;
// now add story to hidden selectedStoriesList input field
		selectedStoriesList = selectedStoriesList + ";;" + ref;
		selectedStoriesList = trimAll(selectedStoriesList);
		var pars = "selectedStoriesList=" + selectedStoriesList;
		new Ajax.Request('updateRelatedStories.do', {asynchronous: true, parameters: pars });
	} else {
		alert("That story has already been selected!");
	}
	
}

function addOneStory(el,ref) {
// check if adding a duplicate story
	if (!selectedStoriesList.match(ref)) {
	
		// add story to display list
		var newItem = document.createElement('li');
		newItem.setAttribute('id', ref);
		newItem.innerHTML = '<a href="javascript:;" class="removelink" style="display:;" onclick="removeStory(this)">[x]</a><!-- <a href="javascript:;" class="itemlink">-->' + el + '<!-- </a> -->';
		
		if ( $('relatedStoriesList').hasChildNodes() ){
    		while ( $('relatedStoriesList').childNodes.length >= 1 ){   		
        		$('relatedStoriesList').removeChild( $('relatedStoriesList').firstChild );       
    		} 
		}
		
		
		$('relatedStoriesList').appendChild(newItem);

		//$('storyCount').innerHTML = 1;
		// now add story to hidden selectedStoriesList input field
		selectedStoriesList = ref;
		selectedStoriesList = trimAll(selectedStoriesList);
		var pars = "selectedStoriesList=" + selectedStoriesList;
		new Ajax.Request('updateRelatedStories.do', {asynchronous: true, parameters: pars });
	} else {
		alert("That story has already been selected!");
	}
}

function removeStory(el) {
// remove story from display list
	var ref = el.parentNode.getAttribute('id');
	Element.remove(el.parentNode);
// decrease count
	numRelatedStories--;
	$('storyCount').innerHTML = numRelatedStories;
// now remove story from hidden selectedStoriesList input field
	if (ref == selectedStoriesList) {
	// element is the ONLY item
		selectedStoriesList = selectedStoriesList.replace(ref, "");
	} else {
		if (selectedStoriesList.match(";;" + ref) != null) {
		// there is more than one item and the element is NOT the first
			selectedStoriesList = selectedStoriesList.replace(";;" + ref, "");
		} else {
		// there is more than one item and the element IS the first
			selectedStoriesList = selectedStoriesList.replace(ref + ";;", "");
		}
	}
	var pars = "selectedStoriesList=" + selectedStoriesList;
	new Ajax.Request('updateRelatedStories.do', {asynchronous: true, parameters: pars });
}

function addCeleb(el,li) {
// check if adding a duplicate story
	if (!selectedCelebList.match(li.id)) {
// add celeb to display list
		var newItem = document.createElement('li');
		newItem.setAttribute('id', li.id);
		newItem.innerHTML = '<a href="javascript:;" class="removelink" style="display:;" onclick="removeCeleb(this)">[x]</a><!-- <a href="javascript:;" class="itemlink"> -->' + el.value + '<!-- </a> -->';
		$('relatedCelebList').appendChild(newItem);
// increase count
		numRelatedCelebs++;
		$('celebsCount').innerHTML = numRelatedCelebs;
// now add celeb to hidden selectedCelebList input field
		selectedCelebList = selectedCelebList + ";;" + li.id;
		selectedCelebList = trimAll(selectedCelebList);
// reset the input box/search field
		Field.clear('relCelebs');
// save new addition (pass selectedCelebList via AJAX)
		var pars = "selectedCelebList=" + selectedCelebList;
		new Ajax.Request('updateCelebs.do', {asynchronous: true, parameters: pars });
	} else {
		alert("That celebrity has already been selected!");
		Field.clear('relCelebs');
	}
}

function removeCeleb(el) {
// remove celeb from display list
	var ref = el.parentNode.getAttribute('id');
	Element.remove(el.parentNode);
// decrease count
	numRelatedCelebs--;
	$('celebsCount').innerHTML = numRelatedCelebs;
// now remove celeb from hidden selectedCelebList input field
	if (ref == selectedCelebList) {
	// element is the ONLY item
		selectedCelebList = selectedCelebList.replace(ref, "");
	} else {
		if (selectedCelebList.match(";;" + ref) != null) {
		// there is more than one item and the element is NOT the first
			selectedCelebList = selectedCelebList.replace(";;" + ref, "");
		} else {
		// there is more than one item and the element IS the first
			selectedCelebList = selectedCelebList.replace(ref + ";;", "");
		}
	}
// save new addition (pass selectedCelebList via AJAX)
	var pars = "selectedCelebList=" + selectedCelebList;
	new Ajax.Request('updateCelebs.do', {asynchronous: true, parameters: pars });
}

function addMovie(el,li,typeOfSearch) {
// check if adding a duplicate movie
	if (!selectedMoviesList.match(li.id)) {
// add movie to display list
		var newItem = document.createElement('li');
		newItem.setAttribute('id', li.id);
		if (typeOfSearch == "review") {
			newItem.innerHTML = '<a href="javascript:;" class="removelink" style="display:;" onclick="removeMovie(this);Element.hide(\'movieTitleSelected\');Element.show(\'movieTitleSearch\');">[x]</a><!-- <a href="javascript:;" class="itemlink"> -->' + el.value + '<!-- </a> -->';
		} else {
			newItem.innerHTML = '<a href="javascript:;" class="removelink" style="display:;" onclick="removeMovie(this)">[x]</a><!-- <a href="javascript:;" class="itemlink"> -->' + el.value + '<!-- </a> -->';
		};
		$('relatedMoviesList').appendChild(newItem);
// increase count
		//numRelatedCelebs++;
		//$('celebsCount').innerHTML = numRelatedCelebs;
// now add movie to hidden selectedMoviesList input field
		selectedMoviesList = selectedMoviesList + ";;" + li.id;
		selectedMoviesList = trimAll(selectedMoviesList);
// reset the input box/search field
		Field.clear('relMovies');
// save new addition (pass selectedMoviesList via AJAX)
		var pars = "selectedMoviesList=" + selectedMoviesList;
		new Ajax.Request('updateRelatedMovies.do', {asynchronous: true, parameters: pars });
	} else {
		alert("That movie has already been selected!");
		Field.clear('relMovies');
	}
}

function removeMovie(el) {
// remove movie from display list
	var ref = el.parentNode.getAttribute('id');
	Element.remove(el.parentNode);
// decrease count
	//numRelatedCelebs--;
	//$('celebsCount').innerHTML = numRelatedCelebs;
// now remove movie from hidden selectedMoviesList input field
	if (ref == selectedMoviesList) {
	// element is the ONLY item
		selectedMoviesList = selectedMoviesList.replace(ref, "");
	} else {
		if (selectedMoviesList.match(";;" + ref) != null) {
		// there is more than one item and the element is NOT the first
			selectedMoviesList = selectedMoviesList.replace(";;" + ref, "");
		} else {
		// there is more than one item and the element IS the first
			selectedMoviesList = selectedMoviesList.replace(ref + ";;", "");
		}
	}
// save new addition (pass selectedMoviesList via AJAX)
	var pars = "selectedMoviesList=" + selectedMoviesList;
	new Ajax.Request('updateRelatedMovies.do', {asynchronous: true, parameters: pars });
}

function removeImage(el, id) {
// clear the hidden field that holds the image ID
	$(id).value = "";
// clear the thumbnail (default image not used anymore)
	Element.update(el, '');
}

function removeGalleryItemImage(el, id, mi) {
// clear the hidden field that holds the image ID
	$(id).value = "";				
	$(mi).value = "";
	
	
// clear the thumbnail (default image not used anymore)
	Element.update(el, '');
}


// initialize selectedStoriesList
var selectedStoriesList = "";

function searchRelatedStories() {
	Element.show('searchIndicator');
	var pars = "selectedNewsSearchType=" + $F('selectedNewsSearchType') + "&relatedStoriesKeyword=" + $F('relatedStoriesKeyword') + "&dateAfter=" + $F('dateAfter') + "&dateBefore=" + $F('dateBefore')+ "&selectedSearchSection=" + $F('selectedSearchSection');
	new Ajax.Updater('panel1', 'searchRelatedStories.do', {asynchronous:true, method:'post', evalScripts: true, parameters: pars})
}

function EnterPressed(e) {
// Code adapted from Jennifer Madden
// http://jennifermadden.com/162/examples/stringEnterKeyDetector.html
	var characterCode;
	if(e && e.which) {           // NN4 specific code
		e = e;
		characterCode = e.which;
	} else {
		characterCode = e.keyCode; // IE specific code
	}
	return (characterCode == 13); // Enter key is 13
}

function disableEnter(field, event) {
	var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	if (keyCode == 13) {
		return false;
	}
	else
	return true;
}

if (document.getElementsByTagName) {
	var inputElements = document.getElementsByTagName("input");
	for (i=0; inputElements[i]; i++) {
		if (inputElements[i].className && (inputElements[i].className.indexOf("disableAutoComplete") != -1)) {
			inputElements[i].setAttribute("autocomplete","off");
		}//if current input element has the disableAutoComplete class set.
	}//loop thru input elements
}//basic DOM-happiness-check

function validate() {
// currently this only checks for the title field
	if ($F('requiredTitle') == "") {
		Element.show('feedback');
		new Element.scrollTo('editNewsForm');
		return false;
	}
}

function removeRelatedImage(el, id, src) {
// remove the image from the display
	Element.remove(el);
// remove the image ID and src from the hidden list
	var imgList = $F('selected_images');
	var toRemove = id + "@@" + src;
	imgList = imgList.replace(toRemove, "");
	imgList = trimAll(imgList);
	$('selected_images').value = imgList;
}

function updateRelatedImages() {
// clear existing images
	Element.update("displayRelatedImages", "");
	var imgList = $F('selected_images');
	var imgArray = imgList.split(";;");
// loop through the array to display the new list of images
	for (var i = 1; i <= imgArray.length; i++) {
		var itemArray = imgArray[i-1].split("@@");
		var itemHTML = '<div id="' + itemArray[0] + '" class="imageFloat"><a href="javascript:;" class="photolink"><img src="' + itemArray[1] + '" class="thumb xsmall"></a><a href="javascript:;" onclick="removeRelatedImage(this.parentNode,\'' + itemArray[0] + '\',\'' + itemArray[1] + '\');" class="deletephotolink">[x]</a></div>';
		new Insertion.Bottom("displayRelatedImages", itemHTML);
	}
}

function changePageTo(pageTo) {
	document.editNewsForm.action = 'changeGalleryPage.do?pageTo='+pageTo;
	document.editNewsForm.submit();
}

function saveAndForwardTo(forwardTo) {
	if( document.getElementById ) {
		document.getElementById('forwardTo').value = forwardTo;
		document.editNewsForm.submit();	
	}
}

function showerrmsg(errid) {
	if( document.getElementById ) {
		document.getElementById(errid).style.display = "block";
	};
}
function hideerrmsg(errid) {
	if( document.getElementById ) {
		document.getElementById(errid).style.display = "none";
	};
}		

var photoBlockOpen = 0;

function editPhotoBlock(blockId) {
// initialize variables
	var ob = "photoOpen_seg" + blockId;
	var cb = "photoClosed_seg" + blockId;
	var mi1 = "primaryPhotoId_seg" + blockId;
	var mi2 = "secondaryPhotoId_seg" + blockId;
	var mt = "photoTitle_seg" + blockId;
	var ms = "photoCaption_seg" + blockId;
	var mr = "photoRating_seg" + blockId;
	var mk = "photoAdKeywords_seg" + blockId;
	var ti1 = "temp_primaryPhotoId_seg" + blockId;
	var ti2 = "temp_secondaryPhotoId_seg" + blockId;
	var tt = "temp_photoTitle_seg" + blockId;
	var ts = "temp_photoCaption_seg" + blockId;
	var tr = "temp_photoRating_seg" + blockId;
	var tk = "temp_photoAdKeywords_seg" + blockId;
	
// copy hidden field values into editable field values
	$(ti1).value = $F(mi1);
	$(ti2).value = $F(mi2);
	$(tt).value = $F(mt);
	$(ts).value = $F(ms);	
	$(tk).value = $F(mk);
	if( document.getElementById && document.getElementById(mr) ) {
		$(tr).value = $F(mr);
	}
// copy summary hidden field value into the editor
	tinyMCE.execInstanceCommand(ts, 'mceSetContent', false, $F(ms));
// toggle div
	Element.show(ob);
	Element.hide(cb);
// flag an open photo block
	photoBlockOpen = photoBlockOpen + 1;
}

function savePhotoBlock(blockId) {
// initialize variables
	var dirty = "isDirty_seg" + blockId;
	var ob = "photoOpen_seg" + blockId;
	var cb = "photoClosed_seg" + blockId;
	var mi1 = "primaryPhotoId_seg" + blockId;
	var mi2 = "secondaryPhotoId_seg" + blockId;
	var mt = "photoTitle_seg" + blockId;
	var ms = "photoCaption_seg" + blockId;
	var mr = "photoRating_seg" + blockId;
	var mk = "photoAdKeywords_seg" + blockId;
	var ti1 = "temp_primaryPhotoId_seg" + blockId;
	var ti2 = "temp_secondaryPhotoId_seg" + blockId;
	var tt = "temp_photoTitle_seg" + blockId;
	var ts = "temp_photoCaption_seg" + blockId;
	var dt = "displayTitle_seg" + blockId;
	var tr = "temp_photoRating_seg" + blockId;
	var tk = "temp_photoAdKeywords_seg" + blockId;
	var csrc1 = "galleryItemThumb_seg" + blockId;
	var csrc2 = "galleryItemThumbSec_seg" + blockId;
	var photoDivContents = document.getElementById('displayPhoto_seg1_'+blockId).getElementsByTagName('img');
	var photoDivContentsSec = document.getElementById('displayPhoto_seg2_'+blockId).getElementsByTagName('img');
	
	// copy editable field values into hidden field values
	tinyMCE.triggerSave();
	if (photoDivContents[0]) {
		$(csrc1).src = photoDivContents[0].src;
		if($(csrc1).hasClassName("invisible")) {
			$(csrc1).removeClassName("invisible");
			$(csrc1).addClassName("thumb vertical horizontal");
		}		
	}
	if (photoDivContentsSec[0]) {
		$(csrc2).src = photoDivContentsSec[0].src;
		if($(csrc2).hasClassName("invisible")) {
			$(csrc2).removeClassName("invisible");
			$(csrc2).addClassName("thumb vertical horizontal");
		}
	}
	$(mi1).value = $F(ti1);
	$(mi2).value = $F(ti2);
	$(mt).value = $F(tt);
	$(ms).value = $F(ts);
	$(mk).value = $F(tk);
	$(dirty).value = true;
	if( document.getElementById && document.getElementById(mr) ) {
		$(mr).value = $F(tr);
	}
// show updated fields
	Element.update(dt, $F(mt));
// toggle div
	Element.show(cb);
	Element.hide(ob);
// clear open photo block flag
	photoBlockOpen = photoBlockOpen - 1;
}


function cancelPhotoBlock(blockId) {
// initialize variables
	var ob = "photoOpen_seg" + blockId;
	var cb = "photoClosed_seg" + blockId;
	var ms = "photoCaption_seg" + blockId;
	var ts = "temp_photoCaption_seg" + blockId;
// discard summary changes (copy hidden field value back into the editor)
	tinyMCE.execInstanceCommand(ts, 'mceSetContent', false, $F(ms));
// toggle div
	Element.show(cb);
	Element.hide(ob);
// clear open photo block flag
	photoBlockOpen = photoBlockOpen - 1;
}

function confirmPhotoBlocks() {
	if (photoBlockOpen > 0) {
	// only prompt to proceed if a photo block is open
		var doProceed = confirm('You have one or more photos that have not been saved. Are you sure you want to continue?');
		if (doProceed == true) {
			return true;
		} else {
			return false;
		}
	} else {
	// if no photo blocks are open, proceed at will
		return true;
	}
}

function deletePhotoBlock() {
// clicking on the delete photo block link triggers openPhotoBlock as well
// so this function is necessary to prevent the photoBlockOpen count from increasing 
	photoBlockOpen = photoBlockOpen - 1;
}

function serializePhotoBlocks() {
	document.editNewsForm.galleryReorders.value = Sortable.serialize('reorderPhotos');
}


function serializeBlogTextBlocks() {
	document.editNewsForm.blogTextBlockReorders.value = Sortable.serialize('reorderBlogTextBlocks');
}

var editorCounter = -1;

function getEditorIndex() {
	editorCounter = editorCounter + 1;
	return editorCounter;
}

function removeEditorIndex() {
	editorCounter = editorCounter - 1;
	return editorCounter;
}

function addCategory(el, li) {
	new Ajax.Updater('dynaCatagoriesDiv', 'addCategory.do?contentId='+li.id, {asynchronous: true, method: 'post', evalScripts: true});
	Field.clear('categorySearch');				
}

function createCategory() {
	if( document.getElementById ) {
		var newCat = document.getElementById('categorySearch').value;
		new Ajax.Updater('dynaCatagoriesDiv', 'addCategory.do?newCat='+newCat, {asynchronous: true, method: 'post', evalScripts: true});
		Field.clear('categorySearch');	
	}
}

function addRelatedPoll(el, li) {	
	var elID = el.parentNode.getAttribute('id');
	var elName = "pollSearch_seg";
	var elNameLength = elName.length;	
	var index = elID.substring(elNameLength, elNameLength+1);					
	
	var pars = "pollId=" + li.id + "&" + "index=" + index;	
	//new Ajax.Request('updateRelatedPoll.do', {asynchronous: true, parameters: pars });
	new Ajax.Updater('pollTitle_seg'+index, 'updateRelatedPoll.do', {asynchronous: true, parameters: pars });
	
	Element.hide(elID);
	if(index >= 0) Element.show('pollTitle_seg'+index);			
		
	//alert("------- li value = " + li.innerHTML);
	//var pollTitleParent = document.getElementById('pollTitle_seg'+index);
	//var pollTitle = pollTitleParent.getElementsByTagName('text');
	//pollTitle[0].innerHTML = li.innerHTML;
	Field.clear('txtPollSearch_seg'+index);		
}

//blog item on-air headline pulled from title
function insertTvHeadline() {
			$("tvheadline").value = $("requiredTitle").value;	
		}
		function updateTvHeadline(el,value) {
			$("tvheadline").value = value;	
		}
		
		function observeTvHeadline() {
			var fieldsToCount = $("requiredTitle");
			updateTvHeadline(fieldsToCount, fieldsToCount.value);
			new Form.Element.Observer( fieldsToCount, 0.1, updateTvHeadline );
}

function checkForPhotoBlocks() {
	if( $$('.photoblock').length > 0 ) {
		return true;
	}
	else {
		alert('You must have at least 1 gallery item to preview!!');
		return false;
	}
}