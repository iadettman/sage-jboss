<%@ page import="com.ceg.online.limn.helpers.SiteHelper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<%
    pageContext.setAttribute("now", new java.util.Date());
    pageContext.setAttribute("imageHost", SiteHelper.getImageHost());
    pageContext.setAttribute("id", request.getParameter("id"));
%>
<tools:setEdition />
<html>
    <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<c:choose>
	   		<c:when test="${not empty id}">
	   			<title>Edit Gallery</title>
	   		</c:when>
	   		<c:otherwise>
	   			<title> Create New Gallery</title>
	   		</c:otherwise>
	   	</c:choose>
	   	<script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/core/1.4.2/jquery-1.4.2.min.js?x=${version.value}"></script>
	   	<script type="text/javascript">
	        jQuery.noConflict();
	        jQuery(document).ready(function () {
	            jQuery("#publishDateStr").datepicker({constrainInput: false, showOn: 'button', buttonImage: '/limn/includes/js/dojo/src/widget/templates/images/dateIcon.gif', buttonImageOnly: true});
	            jQuery('.ui-datepicker-trigger').attr('alt', '').attr('title', '');
	        });
	    </script>
	    <script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/ui/1.8/js/jquery-ui-1.8.custom.min.js?x=${version.value}"></script>
	   	<script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/plugin/jquery.validate/1.7/jquery.validate.pack.js?x=${version.value}"></script>
	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/jquery.autocomplete/1.1/jquery.autocomplete.js?x=${version.value}"></script>
	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/editGallery.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototype.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,controls,builder,dragdrop"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/lightbox.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" ></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/galleryItems.js?x=${version.value}" ></script>
 	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/css_browser_selector.js?x=${version.value}" ></script>
 	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/yui/yahoo/yahoo-min.js?x=${version.value}"></script> 
		<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/yui/cookie/cookie-beta-min.js?x=${version.value}"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/tiny_mce_337/jscripts/tiny_mce/tiny_mce.js?x=${version.value}" ></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/dojo/dojo.js?x=${version.value}"></script>
  		<script language="Javascript">
       		dojo.require("dojo.widget.DropdownDatePicker");
     	</script>	
       	<style type="text/css">
			.dojoTabPaneWrapper {
		  		padding : 10px 10px 10px 10px;
			}
			.clean td { margin:0; padding:0; padding-top:0; padding-bottom:0;}
	   	</style>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/lightbox.css?x=${version.value}" type="text/css" media="screen" />
       	<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}"  type="text/css">
       	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/includes/css/validation.css?x=${version.value}" >
       	<link rel="stylesheet" href="http://scripts.comcastnets.net/client/ajax/jquery/ui/1.8/css/ui-lightness/jquery-ui-1.8.custom.css?x=${version.value}" type="text/css" media="screen" />
		<script type="text/javascript">
			CEG.util.preventMultipleEditors();
			var windowLoaded;  //this checks to see if the window has been fully loaded
			var tinyMCEReplaceCharacters = '\u2005,&thinsp;,\u2011,&#45;,\u2013,&#8211;,\u2014,&#8212;,\u2015,&#151;,\u2017,&#95;,\u2019,&#39;,\u201b,&#145;,\u201c,&#34;,\u201d,&#34;,\u2026,&#8230;,\u2122,&#8482;,\u2212,&#8722;';
			
			window.onload = function() {
				Util.OnLoadCharCount();
				windowLoaded = "true";
			};

			tinyMCE.init({
				button_tile_map : true,
				cleanup_on_startup : true,
				editor_selector : "initializeOnLoad",
				invalid_elements : "font",
				languages : "en",
				mode : "textareas",
				paste_insert_word_content_callback : "CEG.util.convertWord",
				paste_replace_list : tinyMCEReplaceCharacters,
				plugins : "pdw,inlinepopups,paste,safari,searchreplace",
				theme : "advanced",
				<c:choose>
					<c:when test="${editNewsForm.editorModel.photoGallery}">
						width: '380',
						height: '65',
					</c:when>
					<c:otherwise>
						width: '100%',
						height: '65',
					</c:otherwise>
				</c:choose>
				theme_advanced_buttons1 : "pdw_toggle,bold,italic,underline,strikethrough,separator,bullist,numlist,separator,indent,separator,link,unlink,separator,search,replace,selectall,separator,pastetext,separator,charmap,code,cleanup",
				theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,forecolor",
				theme_advanced_buttons3 : "",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_resizing : true,
				theme_advanced_resize_horizontal : false,
				theme_advanced_path : "",

				pdw_toggle_on : 1,
	            pdw_toggle_toolbars : "2",
	            
				setup: function(ed) {
					ed.onPaste.add(function(ed, e, o) {
						ed.setProgressState(true);
					});
					ed.onSetProgressState.add(function(ed, b) {
						if (b) {
							window.setTimeout("tinyMCE.activeEditor.setProgressState(false)",500);
						} else {
							var rl = tinyMCE.activeEditor.getParam("paste_replace_list", tinyMCEReplaceCharacters).split(',');
							var content = tinyMCE.activeEditor.getContent({format:'raw'}).replace(/<!(?:--[\s\S]*?--\s*)?>\s*/g,'');
							for (var i=0; i<rl.length; i+=2) content = content.replace(new RegExp(rl[i], 'gi'), rl[i+1]);
							tinyMCE.activeEditor.setContent(content,{format:'raw'});
						}
					});
					ed.onExecCommand.add(function(ed, cmd, ui, val) {
					});
					ed.onInit.add(function(){
						jQuery('iframe').css('height','auto');
					});
				}
			});
		</script>
	</head>
	<body class="mainpage editpage">
		<div class="topstrip"><!-- --></div>
		<jsp:include page="includes/header.jsp" />	
		<table cellpadding="0" cellspacing="0" border="0" class="headtable">
			<div class="head">
				<div class="head_fns">&nbsp;&nbsp;<a href="/blogtools/admin/">< Back to Admin Home</a><strong></strong></div>
			   	<h1>Photo Gallery</h1>			   
	   			<div class="clear"></div>
			</div>
		</table>		
		<form name="galleryForm" id="galleryForm" method="post" action="">
		<div class="mainbody">		
			<h2>    
	   		<c:choose>
	   			<c:when test="${not empty id}">
		 			Edit Gallery		   
	   			</c:when>
	   			<c:otherwise>
		   			Create New Gallery		   
  				</c:otherwise>
   			</c:choose>	
   			<p class="descr"><span class="req">*</span> required fields for publishing</p>
			</h2>
			<div class="controls">					
				<a class="newbtn smallbtn" href="javascript:;" onClick="LimnGallery.SaveGalleryNew('preview.jsp?');">Save & Preview</a>
				<a class="newbtn smallbtn btnnormal" href="javascript:;" onClick="LimnGallery.SaveGalleryNew('editGallery.jsp?');">Save & Continue Editing</a>
				&nbsp;&nbsp;OR&nbsp;&nbsp;<a href="galleries.jsp">Cancel</a>			
			</div>
			<gallery:galleryUpdate id="${id}" edition="${edition}" reorder="${param.reorder}" galleryItemIndex="${param.galleryItemIndex}" />		
			<div class="controls">					
				<a class="newbtn smallbtn" href="javascript:;" onClick="LimnGallery.SaveGalleryNew('preview.jsp?');">Save & Preview</a>
				<a class="newbtn smallbtn btnnormal" href="javascript:;" onClick="LimnGallery.SaveGalleryNew('editGallery.jsp?');">Save & Continue Editing</a>
				&nbsp;&nbsp;OR&nbsp;&nbsp;<a href="galleries.jsp">Cancel</a>			
			</div>
		</div>
		</form>	
		<jsp:include page="includes/footer.jsp" />
	</body>
</html>