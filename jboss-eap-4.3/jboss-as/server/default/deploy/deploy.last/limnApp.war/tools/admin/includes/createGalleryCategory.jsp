<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ page import="com.ceg.online.toolbox.StringUtil"%>
<%@ page import="com.ceg.online.categories.beans.CategoryTypeBean" %>
<%@ page import="com.ceg.online.categories.helpers.CategoryTypeHelper" %>
<%@ page import="com.ceg.online.categories.beans.CategoryBean" %>
<%@ page import="com.ceg.online.categories.beans.CategoryDisplayBean" %>
<%@ page import="com.ceg.online.categories.beans.impl.CategoryBeanImpl" %>
<%@ page import="com.ceg.online.categories.beans.impl.CategoryDisplayBeanImpl" %>
<%@ page import="com.ceg.online.categories.beans.dtos.SiteEditionDisplayDTO " %>
<%@ page import="com.ceg.online.categories.helpers.HelperFactory" %>
<%@ page import="com.ceg.online.api.editions.helpers.EditionHelper" %>
<%@ page import="com.ceg.online.categories.helpers.CategoryHelper" %>
<%@ page import="com.ceg.online.categories.beans.Status" %>
<%@ page import="com.ceg.online.api.editions.beans.EditionLU" %>
<%@ page import="com.ceg.online.categories.helpers.CategoryDisplayHelper" %>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.ArrayList"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="currentSite" propertyName="tools.config.site"/>
<getters:getToolsSiteId attributeName="siteId" siteName="${currentSite.value}"/>
<%! static Log log = LogFactory.getLog("createGalleryCategory.jsp"); %>
<%
	String catName = (String) request.getParameter("name");
	String edition = (String) request.getParameter("edition");
	Integer siteId = (Integer)request.getAttribute("siteId");
	final String US_EDITION = "USA";
    final Integer catTypeId = 5; // category type "other" 
    Date now = new Date();

	log.info("Category Name to be added is : "+ catName + " edition: " + edition);
	if (!StringUtil.isNullOrEmpty(catName)) {
		CategoryHelper helper = HelperFactory.getCategoryHelper();
		CategoryDisplayHelper displayHelper = HelperFactory.getCategoryDisplayHelper();
		EditionHelper editionHelper = com.ceg.online.api.HelperFactory.getEditionHelper();
		List<EditionLU> editions = new ArrayList<EditionLU>();
        List<SiteEditionDisplayDTO> siteEditionDTOs = new ArrayList<SiteEditionDisplayDTO>();
		EditionLU currentEdition = editionHelper.getEdition(edition);
		CategoryBean cat = new CategoryBeanImpl();
		
		//Either all all autopublished editions including US or add just the Current Edition
		if (currentEdition.getDescr().equals(US_EDITION)) {
			log.info("US edition, getting all auto published editions along with US");
			editions = editionHelper.getAutoPublishedEditions();
			editions.add(currentEdition);
		}
		else {
			log.info("Edition is : " + currentEdition.getEdition());
			editions.add(currentEdition);
		}
		log.info("size of editions are : " + editions.size());
		cat.setName(catName);
		cat.setStatus(Status.published);
		cat.setLastModBy(request.getRemoteUser());
		cat.setCreateDate(now);
		cat.setLastModDate(now);
		
		int catId = helper.saveCategory(cat);
		//Save the display name
		String newDisplayName = catName;
		CategoryDisplayBean display = new CategoryDisplayBeanImpl();
        display.setDisplayName(catName);
        display.setCategoryId(catId);
        
        //Save the category type
        CategoryTypeHelper typeHelper = HelperFactory.getCategoryTypeHelper();
        CategoryTypeBean catType = typeHelper.getCategoryType(catTypeId);
        List<CategoryTypeBean> types = new ArrayList<CategoryTypeBean>();
        types.add(catType);
        Integer returnCode = typeHelper.saveCategoryTypes(catId, types);

        //Create site edition display relationship for all English speaking editions
        if (editions != null && editions.size() > 0) {
       		for (EditionLU ed : editions) {
       			SiteEditionDisplayDTO sedDTO = new SiteEditionDisplayDTO(siteId, ed.getId());
       			sedDTO.setCategoryDisplayBean(display);
       			siteEditionDTOs.add(sedDTO);
       		}
		}

        Integer displayId = displayHelper.saveDisplayAndEditions(display, siteEditionDTOs);
        request.setAttribute("catId", catId);
        request.setAttribute("catName", catName);
%>
${catId}
<%
	}
%>