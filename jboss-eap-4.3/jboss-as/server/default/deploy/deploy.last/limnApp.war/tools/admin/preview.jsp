<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="previewBody" propertyName="tools.config.gallery.preview"/>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<tools:setEdition />
<c:choose>
<c:when test="${empty previewBody}" >
	<c:set var="previewBody" value="${pageContext.request.contextPath}/admin/includes/previewBody.jsp?id=" />
</c:when>
<c:otherwise>
	<c:set var="previewBody" value="${previewBody.value}" />
</c:otherwise>
</c:choose>
<html>
	<head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
     	<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}"  type="text/css">
	</head>
	<frameset rows="*,90" frameborder="0" border="0">
		<frame src="${previewBody}${param.id}&edition=${edition}" marginheight="0" marginwidth="0" noresize="noresize" />
		<frame scrolling="no" src="${pageContext.request.contextPath}/admin/includes/previewControl.jsp?id=${param.id}"
				marginheight="0" marginwidth="0" noresize="noresize" />
	</frameset>
</html>