var HackedAutocompleter = null;
if (Prototype.Browser.IE && "undefined" == typeof document.documentMode || Prototype.Browser.IE && document.documentMode < 8) {
  // In IE7 and earlier and IE8 in quirks mode, the Ajax.Autocompleter will disappear if the user
  // clicks on the window's scrollbar since this causes the auto-completer to blur. Under IE8 and
  // and later in standards mode and Firefox the scrollbar does not cause the auto-completer to blur.
  // This fix works around this issue by delaying the blur by 200ms and determining if the user has
  // clicked on a real HTML element or not.
  // See http://www.nabble.com/AutoComplete-results-disappear-when-using-scrollbar-in-IE-td23612503.html
  // for discussion of this issue.
	
  // Source: https://prototype.lighthouseapp.com/projects/8887/tickets/248-results-popup-from-ajaxautocompleter-disappear-when-user-clicks-on-scrollbars-in-ie6ie7
  HackedAutocompleter = Class.create(Ajax.Autocompleter, {
    initialize: function($super, element, update, url, options) {
      $super(element, update, url, options);
      this.clicked_outside = false;
      Event.observe(document, "click", this.onDocumentClick.bindAsEventListener(this));
      this.element.stopObserving("blur");
      this.element.observe("blur", this.onBlurOverride.bindAsEventListener(this));
    },
    onDocumentClick: function(event) {
      if (this.element.id != event.target.id) {
        this.clicked_outside = true;
      }
    },
    onBlurOverride: function(event) {
      var thisObject = this;
      var callback = function() {
        if (thisObject.clicked_outside) {
          thisObject.clicked_outside = false;
          thisObject.onBlur(event);
        }
        else {
          thisObject.element.focus();
        }
      }
      setTimeout(callback, 200);
    },
    show: function() {
    	try{ if (Element.getStyle(this.update, 'display') == 'none') this.options.onShow(this.element, this.update); } catch(e) { this.options.onShow(this.element, this.update); }
        if(!this.iefix &&
          (Prototype.Browser.IE) &&
          (Element.getStyle(this.update, 'position')=='absolute')) {
          new Insertion.After(this.update,
           '<iframe id="' + this.update.id + '_iefix" '+
           'style="display:none;position:absolute;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" ' +
           'src="javascript:false;" frameborder="0" scrolling="no"></iframe>');
          this.iefix = $(this.update.id+'_iefix');
        }
        if(this.iefix) setTimeout(this.fixIEOverlapping.bind(this), 50);
      }
  });
}
else {
  HackedAutocompleter = Ajax.Autocompleter;
}