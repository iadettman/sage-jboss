<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<c:set var="id" value="${param.id}" scope="session"/>
<c:set var="start" value="${param.start}" scope="session"/>

<getters:getGalleryById attributeName="gallery" id="${id}" />
<c:set var="size" value="${fn:length(gallery.galleryItemIds)}" />
<getters:getGalleryItems attributeName="galleryItems" id="${id}"
	start="${start}" end="${start+15}" />

<gallery:galleryItemPaging id="${id}" galleryItemIndex="${start}"
	size="${size}" total="15" />
<gallery:galleryItemBlocks galleryItems="${galleryItems}" id="${id}" />
<gallery:galleryItemPaging id="${id}" galleryItemIndex="${start}" 
	size="${size}" total="15" />