// JavaScript Document

jQuery(document).ready(function(){								
	
	var myValidator = jQuery('form#galleryForm').validate({
		rules: {
			thumbnailLink: {
			  required: function(element) {
				return (jQuery('input#thumbnailContentID').val() == '');
			  }
			}
		},
		errorPlacement: function(error, element) {				
			if (jQuery(element).parents('.datepicker').length > 0) {				
				error.appendTo( jQuery(element).parents('.datepicker') );
			} else {
				element.after(error);
			}
		}
	});	
	
	jQuery("#categorySearchKeyword").autocomplete("/limn/admin/includes/galleryCategoriesList.jsp", {
		width: 320,
		delay: 500,
		minChars: 2,
		cacheLength: 1,
		max: 5000, // some very large number
		highlight: false,
		scrollHeight: 300,
		formatItem: function(data, i, n, value) {
			var resultsList = jQuery('div.ac_results');
			
			if (( n == 1) && ( data[0] == '') && (data[7] == 'true')) { // when no results but allow user to add new item
				addNewLink(resultsList);
				return 'No record found';
			} else {								
				if ((data[7] == 'true') && (jQuery('ul',resultsList).length < 2)) {
					addNewLink(resultsList);
				} else if ((data[7] == 'false') && (jQuery('ul',resultsList).length > 1)) {
					jQuery('.add-new',resultsList).remove();
				}
				return searchResultItemFormatting(data);					
			}
			
		}
	}).result(updateList).focus(function(){
		jQuery(this).val('');
	});
	
});

function searchResultItemFormatting(data){
	//format is categoryId|category title|category types|category edition|category displayname|category aliases|category related to|showAddNew
			
	var html = '';
	
	html += '<span class="title">'+data[1]+'</span>';
	
	if ((typeof data[2] != 'undefined') && (data[2] != '')) {
		html += ' <span class="types">('+data[2]+')</span>';
	}
	
	if ((typeof data[4] != 'undefined')) {
		if (data[4] == '') { // if no display name
			html += '<span class="missingDisplayName">* Does not display in '+data[3]+' edition.</span>';
		} else if (data[4] != data[1]) { // if display name differs than title
			html += '<span class="displayName">'+data[3]+' Display: '+data[4]+'</span>';
		} // if display name is same as title, display nothing
	}
	
	if ((typeof data[5] != 'undefined') && (data[5] != '')) {
		html += '<span class="aliases">Aliases: '+data[5]+'</span>';
	}
	
	if ((typeof data[6] != 'undefined') && (data[6] != '')) {
		html += '<span class="relatedTo">Related to: '+data[6]+'</span>';
	}
	
	return html;
}

function updateList(event, data, formatted){
	if (data[0] != '') { // if this is a valid search result
		var html = searchResultItemFormatting(data);
		html += '<a class="removelink" onclick="removeThis(this)" title="Remove"></a>';
		html += '<input id="category" type="hidden" name="category" value="'+data[0]+'">';
		
		if (jQuery('#category_'+data[0]).length < 1) {
			jQuery("<li>").attr('id','category_'+data[0]).addClass('gallery_entry_category').html(html).css('display','none').appendTo("#gallery_item_categories").fadeIn('fast');
		} else {
			alert('You have already added "'+data[1]+'" to this story');
		}
	}
}

function removeThis(itemObj){
	var removeTarget = jQuery(itemObj).parents('.gallery_entry_category');
	jQuery(removeTarget).fadeOut('fast',function(){ jQuery(this).remove(); });
}

function addNewLink(itemObj){
	if (jQuery('.add-new',itemObj).length < 1) { // if no add new link
		jQuery('<ul class="add-new"/>').html('<li class="add-new-trigger">Create a new category</li>').click(function(){
			var name = jQuery('#categorySearchKeyword').val();
			var edition = jQuery('input[name="edition"]').val();
			var index = jQuery('.gallery_entry_category').length;
			
			var pars = 'name='+name.replace(/&/g,'%26')+'&edition='+edition+'&index='+index;
			jQuery.post('/blogtools/admin/includes/jsp/createBlogCategory.jsp?'+pars, function(data){
				var newlyCreated = new Array();
				newlyCreated.push(data.replace(/^\s+|\s+$/g,"")); // data[0], trimmed
				newlyCreated.push(name); // date[1]
				updateList(null, newlyCreated, null);
			});
			
		}).appendTo(itemObj);	
	}
}