	/************************************************************************************************************
	
	Script based on one found at (C) www.dhtmlgoodies.com. 
	Modified by Ariel Gamino to use for the Image Selector, Summer 2006
	
	--------------------------------------Original Copyright Message------------------------------------------------
	This is a script from www.dhtmlgoodies.com. You will find this and a lot of other scripts at our website.		
	Terms of use:
	You are free to use this script as long as the copyright message is kept intact. However, you may not
	redistribute, sell or repost it without our permission.	
	Thank you!	
	www.dhtmlgoodies.com
	Alf Magne Kalleland
	----------------------------------------------------------------------------------------------------------------
	
	************************************************************************************************************/
		
	/* VARIABLES YOU COULD MODIFY */
	var boxSizeArray = [20,5,3,3,3,3,3];	// Array indicating how many items there is rooom for in the right column ULs
	
	var arrow_offsetX = -5;	// Offset X - position of small arrow
	var arrow_offsetY = 0;	// Offset Y - position of small arrow
	
	var arrow_offsetX_firefox = -6;	// Firefox - offset X small arrow
	var arrow_offsetY_firefox = -13; // Firefox - offset Y small arrow
	
	var verticalSpaceBetweenListItems = 3;	// Pixels space between one <li> and next	
											// Same value or higher as margin bottom in CSS for #dragDropContainer ul li,#dragContent li
											
	var initShuffleItems = false;	// Shuffle items before staring

	var indicateDestionationByUseOfArrow = false;	// Display arrow to indicate where object will be dropped(false = use rectangle)
	
	var lockedAfterDrag = false;	/* Lock items after they have been dragged, i.e. the user get's only one shot for the correct answer */
	
	/* END VARIABLES YOU COULD MODIFY */

	var dragDropTopContainer    = false;
	var dragTimer               = -1;
	var dragContentObj          = false;
	var contentToBeDragged      = false;	// Reference to dragged <li>
	var contentToBeDragged_src  = false;	// Reference to parent of <li> before drag started
	var contentToBeDragged_next = false; 	// Reference to next sibling of <li> to be dragged
	var destinationObj          = false;	// Reference to <UL> or <LI> where element is dropped.
	var dragDropIndicator       = false;	// Reference to small arrow indicating where items will be dropped
	var ulPositionArray         = new Array();
	var mouseoverObj            = false;	// Reference to highlighted DIV
	
	var MSIE                   = navigator.userAgent.indexOf('MSIE')>=0?true:false;
	var navigatorVersion       = navigator.appVersion.replace(/.*?MSIE (\d\.\d).*/g,'$1')/1;
	var destinationBoxes       = new Array();
	var indicateDestinationBox = false;
		
	function getTopPos(inputObj)
	{		
	  var returnValue = inputObj.offsetTop;
	  while((inputObj = inputObj.offsetParent) != null){
	  	if(inputObj.tagName!='HTML')returnValue += inputObj.offsetTop;
	  }
	  return returnValue;
	}
	
	function getLeftPos(inputObj)
	{
	  var returnValue = inputObj.offsetLeft;
	  while((inputObj = inputObj.offsetParent) != null){
	  	if(inputObj.tagName!='HTML')returnValue += inputObj.offsetLeft;
	  }
	  return returnValue;
	}

	function cancelEvent()
	{
		return false;
	}

	function initDrag(e)	// Mouse button is pressed down on a LI
	{
		if(document.all)e = event;
		if(lockedAfterDrag && this.parentNode.id!='allItems')return;
		
		var st = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
		var sl = Math.max(document.body.scrollLeft,document.documentElement.scrollLeft);
		
		dragTimer = 0;
		dragContentObj.style.left = e.clientX + sl + 'px';
		dragContentObj.style.top = e.clientY + st + 'px';
		contentToBeDragged = this;
		contentToBeDragged_src = this.parentNode;
		contentToBeDragged_next = false;
		if(this.nextSibling){
			contentToBeDragged_next = this.nextSibling;
			if(!this.tagName && contentToBeDragged_next.nextSibling)contentToBeDragged_next = contentToBeDragged_next.nextSibling;
		}
		//Disable drag/drop for locked images
		if (contentToBeDragged.getAttribute("status") != 'locked' 
				&& contentToBeDragged.getAttribute("status") != 'noUsage') {
			timerDrag();
		}

        //Tips.hideAll(); // remove all tool tips when dragging starts  (prototip)
        jQuery(".qtip").hide(); // remove all tool tips when dragging starts  (qTip)
        
		return false;
	}

	function timerDrag()
	{
		if(dragTimer>=0 && dragTimer<10){
			dragTimer++;
			setTimeout('timerDrag()',10);
			return;
		}
		if(dragTimer==10){
			dragContentObj.style.display='block';
			dragContentObj.appendChild(contentToBeDragged);
		}
	}
	
	function moveDragContent(e)
	{
		if(dragTimer<10){
			if(contentToBeDragged){
				if(contentToBeDragged_next){
					contentToBeDragged_src.insertBefore(contentToBeDragged,contentToBeDragged_next);
				}else{
					contentToBeDragged_src.appendChild(contentToBeDragged);
				}	
			}
			return;
		}
		if(document.all)e = event;
		var st = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
		var sl = Math.max(document.body.scrollLeft,document.documentElement.scrollLeft);
		
		
		dragContentObj.style.left = e.clientX + sl + 'px';
		dragContentObj.style.top = e.clientY + st + 'px';
		
		if(mouseoverObj)mouseoverObj.className='';
		destinationObj = false;
		dragDropIndicator.style.display='none';
		if(indicateDestinationBox)indicateDestinationBox.style.display='none';
		var x = e.clientX + sl;
		var y = e.clientY + st;
		var width = dragContentObj.offsetWidth;
		var height = dragContentObj.offsetHeight;
		
		var tmpOffsetX = arrow_offsetX;
		var tmpOffsetY = arrow_offsetY;
		if(!document.all){
			tmpOffsetX = arrow_offsetX_firefox;
			tmpOffsetY = arrow_offsetY_firefox;
		}

		for(var no=0;no<ulPositionArray.length;no++){
			var ul_leftPos = ulPositionArray[no]['left'];	
			var ul_topPos = ulPositionArray[no]['top'];	
			var ul_height = ulPositionArray[no]['height'];
			var ul_width = ulPositionArray[no]['width'];
			
			if((x+width) > ul_leftPos && x<(ul_leftPos + ul_width) && (y+height)> ul_topPos && y<(ul_topPos + ul_height)){
				var noExisting = ulPositionArray[no]['obj'].getElementsByTagName('LI').length;
				if(indicateDestinationBox && indicateDestinationBox.parentNode==ulPositionArray[no]['obj'])noExisting--;
				if(noExisting<boxSizeArray[no-1] || no==0){
					dragDropIndicator.style.left = ul_leftPos + tmpOffsetX + 'px';
					var subLi = ulPositionArray[no]['obj'].getElementsByTagName('LI');
					for(var liIndex=0;liIndex<subLi.length;liIndex++){
						var tmpTop = getTopPos(subLi[liIndex]);
						if(!indicateDestionationByUseOfArrow){
							if(y<tmpTop){
								destinationObj = subLi[liIndex];
								indicateDestinationBox.style.display='block';
								subLi[liIndex].parentNode.insertBefore(indicateDestinationBox,subLi[liIndex]);
								break;
							}
						}else{							
							if(y<tmpTop){
								destinationObj = subLi[liIndex];
								dragDropIndicator.style.top = tmpTop + tmpOffsetY - Math.round(dragDropIndicator.clientHeight/2) + 'px';
								dragDropIndicator.style.display='block';
								break;
							}	
						}					
					}
					
					if(!indicateDestionationByUseOfArrow){
						if(indicateDestinationBox.style.display=='none'){
							indicateDestinationBox.style.display='block';
							ulPositionArray[no]['obj'].appendChild(indicateDestinationBox);
						}
						
					}else{
						if(subLi.length>0 && dragDropIndicator.style.display=='none'){
							dragDropIndicator.style.top = getTopPos(subLi[subLi.length-1]) + subLi[subLi.length-1].offsetHeight + tmpOffsetY + 'px';
							dragDropIndicator.style.display='block';
						}
						if(subLi.length==0){
							dragDropIndicator.style.top = ul_topPos + arrow_offsetY + 'px'
							dragDropIndicator.style.display='block';
						}
					}
					
					if(!destinationObj)destinationObj = ulPositionArray[no]['obj'];
					mouseoverObj = ulPositionArray[no]['obj'].parentNode;
					mouseoverObj.className='mouseover';
					return;
				}
			}
		}
	}

	/* End dragging 
	Put <LI> into a destination or back to where it came from.
	*/	
	function dragDropEnd(e)	
	{
	   //document.getElementById('saveContent').innerHTML = ''+dragTimer+' ';
		if(dragTimer==-1)return;
		if(dragTimer<10){
			dragTimer = -1;
			return;
		}
		
		dragTimer = -1;
		if(document.all)e = event;		
		if(destinationObj){
			
			jQuery('.tooltip-content',contentToBeDragged).remove();
			var groupId = contentToBeDragged.getAttribute('groupId');
			if(!groupId)groupId = contentToBeDragged.groupId;
			var destinationToCheckOn = destinationObj;
			if(destinationObj.tagName!='UL'){
				destinationToCheckOn = destinationObj.parentNode;
			}
			
			var answerCheck = false;
			if(groupId == destinationToCheckOn.id){
				contentToBeDragged.className = 'correctAnswer';		
				answerCheck=true;	
			}else{
				contentToBeDragged.className = 'wrongAnswer';
			}
			if(destinationObj.id=='allItems' || destinationObj.parentNode.id=='allItems')contentToBeDragged.className='';
			
			
			if(destinationObj.tagName=='UL'){
				destinationObj.appendChild(contentToBeDragged);
			}else{
				destinationObj.parentNode.insertBefore(contentToBeDragged,destinationObj);
			}
			mouseoverObj.className='';
			destinationObj = false;
			dragDropIndicator.style.display='none';
			if(indicateDestinationBox){
				indicateDestinationBox.style.display='none';
				document.body.appendChild(indicateDestinationBox);
			}
						
			contentToBeDragged = false;
			//Insert images selected into hidden field
			saveLightbox();
			
			return;
		}	
		if(contentToBeDragged_next){
			contentToBeDragged_src.insertBefore(contentToBeDragged,contentToBeDragged_next);
		}else{
			contentToBeDragged_src.appendChild(contentToBeDragged);
		}
		contentToBeDragged = false;
		dragDropIndicator.style.display='none';
		if(indicateDestinationBox){
			indicateDestinationBox.style.display='none';
			document.body.appendChild(indicateDestinationBox);
			
		}		
		mouseoverObj = false;
		
	}

	/* 
	  Preparing data to be saved 
	  Pass values of images selected back to calling (parent) page.
	*/
	function saveDragDropNodes()
	{
		var saveString = ""; //use for debugging
		//Obtain Values from parameters passed by string
		var query = window.location.search.substring(1);
    	var parms = query.split('&');
	    var qsParm = new Array();

	    for (var i=0; i<parms.length; i++) {
   			var pos = parms[i].indexOf('=');
   			if (pos > 0) {
      			var key = parms[i].substring(0,pos);
	      		var val = parms[i].substring(pos+1);
	      		qsParm[key] = val;      		
    	  	}//pos>0
	   	}//for i=0

	    //Added to obatin values from POST request, as suppose to GET which limits url to 2,083 chars
	   	var callingForm            = document.openWindow.elements["callingForm"].value;
	   	var property               = document.openWindow.elements["property"].value;
		var displayPreview         = document.openWindow.elements["displayPreview"].value;
		var displayProperty        = document.openWindow.elements["displayProperty"].value;
		var paramSelectedImagesStr = document.openWindow.elements["selectedImagesStr"].value;
		var numImages              = document.openWindow.elements["numImages"].value;			
        var currentProperty   = "";
        var currentImageDiv   = "";
        var currentDisplayProperty   = "";
        var selectedImagesStr = "";
		var uls = dragDropTopContainer.getElementsByTagName('UL');
		
		for(var no=0;no<uls.length;no++){	// Looping through all <ul>
			var lis = uls[no].getElementsByTagName('LI');			
			for(var no2=0;no2<lis.length;no2++){
				if (lis[no2].id && lis[no2].id != "") {
					if(saveString.length>0)saveString = saveString + ";";
					if(uls[no].id=='allItems') {
						//Do nothing
					} else {
						var currentIndex = (no2 + 1);
					   //Add handle of that image
					   saveString = saveString + lis[no2].id + ">>" + no2 + "<br>";
					   //Paste values to calling pages				   
					   if(numImages=='1') {
					   	   currentProperty  = property;
					   	   currentImageDiv  = displayPreview;
					   	   currentDisplayProperty = displayProperty;
					   } else {
					   		currentProperty = property + currentIndex;
					        currentImageDiv = displayPreview + currentIndex;
					   	   	currentDisplayProperty = displayProperty + currentIndex;
					   }
					   //Paste content into calling page
					   //UUID of image
					   var itemID  = lis[no2].id;
					   //Source path for this image
					   var itemIMGCollection= lis[no2].getElementsByTagName('IMG'); 
					   var itemIMG = itemIMGCollection[0]; //HTMLImageElement
					   var itemSRC = "";
	
					   //Get image source
					   try {
					   	  itemSRC = itemIMG.getAttribute('src');
					   }catch(err){
					   	  itemSRC = "";
					   }
	
					   if(selectedImagesStr.length>0){
					      //Add token to separate images
		  			   	  selectedImagesStr = selectedImagesStr + ';;';
					   }
					   selectedImagesStr = selectedImagesStr + itemID + '@@' +itemSRC;
					   //check to see if image selection is for the thumbnail or related images
					   if(!window.opener.document.forms[callingForm].elements[paramSelectedImagesStr]){
						   //Set values on calling page only if they exist					   
						   if(window.opener.document.forms[callingForm].elements[currentProperty]){					   
						      window.opener.document.forms[callingForm].elements[currentProperty].value = itemID;
						   }
						   if(window.opener.document.getElementById(currentImageDiv)) {
						      window.opener.document.getElementById(currentImageDiv).innerHTML = "<img src='" + itemSRC + "' class='thumb thumbnail' />";
	                          window.opener.document.getElementById(currentImageDiv).style.display = "block"; 
						   }
						   if(window.opener.document.forms[callingForm].elements[currentDisplayProperty]){
						      window.opener.document.forms[callingForm].elements[currentDisplayProperty].value = itemSRC;
						   }
					   }
					}//else		
				}//if
			}//for
		}//for
   	if(window.opener.document.forms[callingForm].elements[paramSelectedImagesStr]){
		    //send selected images as a string
            window.opener.document.forms[callingForm].elements[paramSelectedImagesStr].value = selectedImagesStr;
            if (paramSelectedImagesStr == 'selected_images') {
	            //update the div to show the thumbnails of the selected images contained in the above string
    	        window.opener.updateRelatedImages();
    	    }
    	    else if (paramSelectedImagesStr == 'photo_gallery') {
    	    	window.opener.LimnGalleryItem.AddGalleryItems(selectedImagesStr);
    	    }
    }     
		//FOR DEBUGGING ONLY//document.getElementById('saveContent').innerHTML = '<h1>Ready to save these nodes:</h1> ' + saveString.replace(/;/g,';<br>') + '<p>Format: ID of ul |(pipe) ID of li;(semicolon)</p><p>You can put these values into a hidden form fields, post it to the server and explode the submitted value there</p>';
	}//saveDragDropNodes()

   /* 
	Populates a hidden field in the image selector to 
	*/
	function saveLightbox()
	{
		var saveString = ""; //use for debugging

        var currentProperty   = "";
        var currentImageDiv   = "";
        var selectedImagesStr = "";
		var uls = dragDropTopContainer.getElementsByTagName('UL');

		for(var no=0;no<uls.length;no++){	// Looping through all <ul>
			var lis = uls[no].getElementsByTagName('LI');
			for(var no2=0;no2<lis.length;no2++){
				if(saveString.length>0)saveString = saveString + ";";
				if(uls[no].id=='allItems') {
					//Do nothing
				} else {
					 var currentIndex = (no2 + 1);

				   //Paste content into calling page
				   //UUID of image
				   var itemID  = lis[no2].id;
				   //Source path for this image
				   var itemIMGCollection = lis[no2].getElementsByTagName('IMG'); 				   
      			   var itemACollection   = lis[no2].getElementsByTagName('A');

				   var itemIMG   = itemIMGCollection[0]; //HTMLImageElement

       			   var itemTitle     = "";
				   var itemSRC       = "";
				   var itemIMGWidth  = "";
				   var itemIMGHeight = "";
				   var itemClass     = "";				   

				   //Get image source
				   try {
				   	  itemSRC = itemIMG.getAttribute('src');
				   } catch(err) {
				   	  itemSRC = "";
				   }
				   //Get image height
				   try {
				   	  itemIMGHeight = "height="+itemIMG.getAttribute('height');
				   } catch(err) {
				   	  itemIMGHeight = "";
				   }
				   //Get image width
				   try {
				   	  itemIMGWidth = "width="+itemIMG.getAttribute('width');
				   } catch(err) {
				   	  itemIMGWidth = "";
				   }
				   //Get image class
				   try {
				   	  itemClass = "class=\""+itemIMG.getAttribute('class')+"\"";
				   } catch(err) {
				   	  itemClass = "";
				   }

				   if(selectedImagesStr.length>0){
				     //Add token to separate images
  				     selectedImagesStr = selectedImagesStr + ';;';
				   }

				   selectedImagesStr = selectedImagesStr + itemID + '@@' +itemSRC + '@@' + itemTitle + '@@' + itemIMGWidth + '@@' + itemIMGHeight + '@@' + itemClass;
				}//else
			}//for
		}//for

		if(document.forms[0].elements['lightboxStr']){
		    //send selected images as a string
	        document.forms[0].elements['lightboxStr'].value = selectedImagesStr;
        }
		//document.getElementById('saveContent').innerHTML = selectedImagesStr;
	}//saveLightbox()

	function initDragDropScript()
	{
		dragContentObj = document.getElementById('dragContent');
		dragDropIndicator = document.getElementById('dragDropIndicator');
		dragDropTopContainer = document.getElementById('dragDropContainer');
		if(!MSIE){
			document.documentElement.onselectstart = cancelEvent;;
		}
		var listItems = dragDropTopContainer.getElementsByTagName('LI');	// Get array containing all <LI>
		var itemHeight = false;
		for(var no=0;no<listItems.length;no++){
			listItems[no].onmousedown = initDrag;
			listItems[no].onselectstart = cancelEvent;
			if(!itemHeight)itemHeight = listItems[no].offsetHeight;
			if(MSIE && navigatorVersion/1<6){
				listItems[no].style.cursor='hand';
			}			
		}
		
		var mainContainer = document.getElementById('mainContainer');
		var uls = mainContainer.getElementsByTagName('UL');
		itemHeight = itemHeight + verticalSpaceBetweenListItems;
		for(var no=0;no<uls.length;no++){
			uls[no].style.height = itemHeight * boxSizeArray[no]  + 'px';
			destinationBoxes[destinationBoxes.length] = uls[no];
		}
		
		var leftContainer = document.getElementById('listOfItems');
		var itemBox = leftContainer.getElementsByTagName('UL')[0];
		
		document.documentElement.onmousemove = moveDragContent;	// Mouse move event - moving draggable div
		document.documentElement.onmouseup = dragDropEnd;	// Mouse move event - moving draggable div
		
		var ulArray = dragDropTopContainer.getElementsByTagName('UL');
		for(var no=0;no<ulArray.length;no++){
			ulPositionArray[no] = new Array();
			ulPositionArray[no]['left'] = getLeftPos(ulArray[no]);	
			ulPositionArray[no]['top'] = getTopPos(ulArray[no]);	
			ulPositionArray[no]['width'] = ulArray[no].offsetWidth;
			ulPositionArray[no]['height'] = ulArray[no].clientHeight;
			ulPositionArray[no]['obj'] = ulArray[no];
		}
		
		if(initShuffleItems){
			var allItemsObj = document.getElementById('allItems');
			var initItems = allItemsObj.getElementsByTagName('LI');
			
			for(var no=0;no<(initItems.length*10);no++){
				var itemIndex = Math.floor(Math.random()*initItems.length);
				allItemsObj.appendChild(initItems[itemIndex]);
			}
		}
		if(!indicateDestionationByUseOfArrow){
			indicateDestinationBox = document.createElement('LI');
			indicateDestinationBox.id = 'indicateDestination';
			indicateDestinationBox.style.display='none';
			document.body.appendChild(indicateDestinationBox);
		}

		//Added for paging
		saveLightbox();
	}
	
	window.onload = initDragDropScript;