<%@ page import="com.ceg.online.limn.helpers.SiteHelper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<%
    pageContext.setAttribute("now", new java.util.Date());
    pageContext.setAttribute("imageHost", SiteHelper.getImageHost());
%>
<tools:setEdition />
<html>
    <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<title>Manage Section: Images</title>
       	<script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/core/1.4.2/jquery-1.4.2.min.js?x=${version.value}"></script>
	    <script type="text/javascript" src="http://scripts.comcastnets.net/client/ajax/jquery/ui/1.8/js/jquery-ui-1.8.custom.min.js?x=${version.value}"></script>
	    <script type="text/javascript">
	        jQuery.noConflict();
	        jQuery(document).ready(function () {
	            jQuery("#beginDate").datepicker({constrainInput: false, showOn: 'button', buttonImage: '/limn/includes/js/dojo/src/widget/templates/images/dateIcon.gif', buttonImageOnly: true});
	            jQuery("#endDate").datepicker({constrainInput: false, showOn: 'button', buttonImage: '/limn/includes/js/dojo/src/widget/templates/images/dateIcon.gif', buttonImageOnly: true});
	            jQuery('.ui-datepicker-trigger').attr('alt', '').attr('title', '');
	        });
	    </script>       	
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototype.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,builder"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/lightbox.js?x=${version.value}"></script>
       	<script src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" type="text/javascript"></script>
 	   	<script src="${pageContext.request.contextPath}/includes/js/css_browser_selector.js?x=${version.value}" type="text/javascript"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/lightbox.css?x=${version.value}" type="text/css" media="screen" />
		<link rel="stylesheet" href="http://scripts.comcastnets.net/client/ajax/jquery/ui/1.7.2/css/ui-lightness/jquery-ui-1.7.2.custom.css?x=${version.value}" type="text/css" media="screen" />
       	<link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
       	<link rel="stylesheet" href="http://scripts.comcastnets.net/client/ajax/jquery/ui/1.8/css/ui-lightness/jquery-ui-1.8.custom.css?x=${version.value}" type="text/css" media="screen" />
       	<style type="text/css">
			.dojoTabPaneWrapper {
		  		padding : 10px 10px 10px 10px;
			}
			.clean td { margin:0; padding:0; padding-top:0; }
	   	</style>
	</head>
	<body class="mainpage imagepage">
		<div class="topstrip"><!-- --></div>
		<jsp:include page="includes/header.jsp" />	
		<table cellpadding="0" cellspacing="0" border="0" class="headtable">
			<div class="head">
				<div class="head_fns">&nbsp;&nbsp;<a href="/blogtools/admin/">< Back to Admin Home</a><strong></strong></div>
			   	<h1>Image Management</h1>			   
				<div class="clear"></div>
			</div>
		</table>
		<div class="mainbody">		
		<image:filterImageHeader processedStatus="${param.processedStatus}" processedTitle="${param.processedTitle}" />
		<getters:getImages attributeName="images" size="50" start="0" title="${title}" agency="${agency}" keyword="${keyword}" filename="${filename}" 
			beginDate="${beginDate}" endDate="${endDate}" dateComparison="${dateComparison}" width="${width}" height="${height}" sizeComparison="${sizeComparison}"/>
		<getters:getImages attributeName="imageSize" isCount="${true}" title="${title}" agency="${agency}" keyword="${keyword}" filename="${filename}" 
			beginDate="${beginDate}" endDate="${endDate}" dateComparison="${dateComparison}" width="${width}" height="${height}" sizeComparison="${sizeComparison}"/>
		<image:imageEntries images="${images}" size="${imageSize}" />
		</div>
	<jsp:include page="includes/footer.jsp" />
	</body>
</html>
