<%@ page import="com.ceg.online.limn.helpers.SiteHelper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="agency" tagdir="/WEB-INF/tags/custom/tools/agency"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<%
    pageContext.setAttribute("now", new java.util.Date());
    pageContext.setAttribute("imageHost", SiteHelper.getImageHost());
%>
<tools:setEdition />
<html>
    <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<title>Manage Section: Agencies</title>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototype.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,builder"></script>
       	<script src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" type="text/javascript"></script>
 	   	<script src="${pageContext.request.contextPath}/includes/js/css_browser_selector.js?x=${version.value}" type="text/javascript"></script>
       	<link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
	</head>
	<body class="mainpage imagepage">
		<div class="topstrip"><!-- --></div>
		<jsp:include page="includes/header.jsp" />	
		<table cellpadding="0" cellspacing="0" border="0" class="headtable">
			<div class="head">
				<div class="head_fns">&nbsp;&nbsp;<a href="/blogtools/admin/">< Back to Admin Home</a><strong></strong></div>
			   	<h1>Agency Management</h1>			   
				<div class="clear"></div>
			</div>
		</table>
		<getters:getAgencies attributeName="agencies" />
		<div id="agency_listings">
		<agency:agencyListings agencies="${agencies}" />
		</div>
	<jsp:include page="includes/footer.jsp" />
	</body>
</html>
