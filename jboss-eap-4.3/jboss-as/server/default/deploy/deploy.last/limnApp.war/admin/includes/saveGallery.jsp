<%@ page import="java.lang.String"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@ page import="com.ceg.online.limn.util.SaveUtil"%>
<%
Log log = LogFactory.getLog("saveGallery.jsp");
int id = SaveUtil.saveGallery(request);
request.setAttribute("id", id);
%>${id}