<%@ page import="com.ceg.online.limn.helpers.SiteHelper" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<%
    pageContext.setAttribute("now", new java.util.Date());
    pageContext.setAttribute("imageHost", SiteHelper.getImageHost());
%>
<tools:setEdition />
<html>
  <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<title>Manage Section: Images</title>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototype.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,builder"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/lightbox.js?x=${version.value}"></script>
       	<script src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" type="text/javascript"></script>
 	   	<script src="${pageContext.request.contextPath}/includes/js/css_browser_selector.js?x=${version.value}" type="text/javascript"></script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/lightbox.css?x=${version.value}" type="text/css" media="screen" />
       	<link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
       	<style type="text/css">
			.dojoTabPaneWrapper {
		  		padding : 10px 10px 10px 10px;
			}
			.clean td { margin:0; padding:0; padding-top:0; }
	   	</style>
	</head>
	<body class="mainpage imagepage">
	<div class="topstrip"><!-- --></div>
	<jsp:include page="includes/header.jsp" />
	<div class="head">
		<div class="head_fns">&nbsp;&nbsp;<a href="/blogtools/admin/">< Back to Admin Home</a><strong></strong></div>
		<h1>Photo Galleries</h1>			   
		<div class="clear"></div>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="headtable"></table>
	<div class="mainbody">
		<div class="pagesubnav">
			<a href="/limn/admin/galleries.jsp" title="Manage the current content collection">
				Landing Page
			</a> 
			|
			<a href="#" class="current">			
		    	All ${fn:toUpperCase(edition)} Galleries
			</a>  
		</div>	

        <c:if test="${not empty param.returnFeedback}">
            <dl id="feedback" class="feedback_publish" style="">
                <dt/> 
                <dd> ${param.returnFeedback}</dd>
            </dl>
        </c:if>

		<gallery:filterGalleryHeader edition="${edition}" />
		<getters:getGalleries attributeName="galleries" edition="${edition}"
			status="edited,published,hidden" size="45" start="0" />
		<getters:getGalleries attributeName="gallerySize" edition="${edition}" 
			status="edited,published,hidden" isCount="${true}" />
		<gallery:galleryEntries edition="${edition}" galleries="${galleries}" size="${gallerySize}" />
	</div>
	<jsp:include page="includes/footer.jsp" />
	</body>
</html>