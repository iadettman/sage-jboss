<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ceg.online.limn.helpers.GalleryHelper" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%
String[] galleryItemIds = request.getParameterValues("galleryItemId");
String start = request.getParameter("start");
String galleryId = request.getParameter("galleryid");

if (galleryItemIds != null && galleryItemIds.length > 0
		&& start != null && !start.trim().equals("")) {
	List<Integer> ids = new ArrayList<Integer>();
	for (int index = 0; index < galleryItemIds.length; ++index) {
		ids.add(new Integer(galleryItemIds[index]));
	}
	GalleryHelper.saveGalleryItemsOrder(new Integer(galleryId), ids, new Integer(start));
}
%>