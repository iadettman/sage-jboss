<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<tools:setEdition />
<getters:getToolsEditions attributeName="editions"/>
<html>
    <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<title>Duplicate Gallery</title>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototype.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,builder"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" ></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/galleryItems.js?x=${version.value}" ></script>
 	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/css_browser_selector.js?x=${version.value}" ></script>
 	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/yui/yahoo/yahoo-min.js?x=${version.value}"></script> 
		<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/yui/cookie/cookie-beta-min.js?x=${version.value}"></script>
       	<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}"  type="text/css">
	</head>
	<body class="mainpage editpage">
		<div class="topstrip"><!-- --></div>
		<jsp:include page="includes/header.jsp" />	
		<table cellpadding="0" cellspacing="0" border="0" class="headtable">
			<div class="head">
				<div class="head_fns">&nbsp;&nbsp;<a href="/blogtools/admin/">< Back to Admin Home</a><strong></strong></div>
			   	<h1>Duplicate Gallery</h1>			   
	   			<div class="clear"></div>
			</div>
		</table>		
		<form name="galleryDuplicateForm" id="galleryDuplicateForm" method="post" action="">
		<div class="mainbody">		
			<h2>    
	   		Copy Gallery to Other Editions<br />
			</h2>
			<br /><br /><br />
			<getters:getGalleryById attributeName="gallery" id="${param.id}" />
			<table border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftcol">			
						Title: ${gallery.title}<br /><br />
						Current Edition: ${gallery.edition.descr}
						<img src="${pageContext.request.contextPath}/includes/images/flag_${gallery.edition.edition}.gif" />
						<br /><br />
						Destination Edition:<br />
						<input type="hidden" id="editionId" name="editionId" />
						<c:forEach items="${editions}" var="ed">
							<c:choose>
								<c:when test="${ed.edition eq gallery.edition.edition}">
									<c:set var="disabled" value="disabled" />
								</c:when>
								<c:otherwise>
									<c:set var="disabled" value="" />
								</c:otherwise>
							</c:choose>
							<input id="edition" name="edition" type="radio" ${disabled} onclick="$('editionId').value='${ed.edition}';" />-
							<img src="${pageContext.request.contextPath}/includes/images/flag_${ed.edition}.gif" />
							${ed.descr}
							<br /><br />
						</c:forEach>
					</td>
					<td class="rightcol">
					</td>
				</tr>
			</tbody>
			</table>
			<div class="controls">					
				<a class="newbtn smallbtn" href="javascript:;" onClick="LimnGallery.DuplicateGallery('${gallery.id}', $('editionId').value);">Duplicate</a>
				&nbsp;&nbsp;OR&nbsp;&nbsp;<a href="editGallery.jsp?id=${param.id}">Cancel</a>			
			</div>
		</div>
		</form>	
		<jsp:include page="includes/footer.jsp" />
	</body>
</html>