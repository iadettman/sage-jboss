<%@ page import="java.util.Date" %>
<%@ page import="com.ceg.online.limn.helpers.GalleryHelper" %>
<%@ page import="com.ceg.online.limn.beans.impl.GalleryImpl" %>
<%
String id = request.getParameter("id");
String updateStatus = request.getParameter("updateStatus");
GalleryImpl gallery;
Date currentDate = new Date();

if (id != null && !id.trim().equals("") && updateStatus != null) {
	gallery = (GalleryImpl)GalleryHelper.getGalleryById(new Integer(id));

	if (updateStatus.equals(GalleryHelper.PUBLISHED)) {
		gallery.setStatus(GalleryHelper.PUBLISHED);
		gallery.setPublishDate(currentDate);
		gallery = (GalleryImpl)GalleryHelper.saveGallery(gallery);
		GalleryHelper.publishGallery(gallery);
	} 
	else if (updateStatus.equals(GalleryHelper.HIDDEN)) {
		//Update status to Hidden and publish it
		gallery.setStatus(GalleryHelper.HIDDEN);
		gallery = (GalleryImpl)GalleryHelper.saveGallery(gallery);
		GalleryHelper.deleteSiteGallery(gallery.getId());
	}
	else if (updateStatus.equals(GalleryHelper.DELETED)) {
		//Delete gallery from site
		gallery.setStatus(GalleryHelper.DELETED);
		gallery = (GalleryImpl)GalleryHelper.saveGallery(gallery);
		GalleryHelper.deleteSiteGallery(gallery.getId());
	}
	
}
%>