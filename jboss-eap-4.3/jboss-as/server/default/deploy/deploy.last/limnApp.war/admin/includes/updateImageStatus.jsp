<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ page import="java.util.Date" %>
<%@ page import="com.ceg.online.limn.helpers.ImageHelper" %>
<%@ page import="com.ceg.online.limn.beans.impl.ImageImpl" %>
<c:set var="title" value="${param.title}" scope="session"/>
<c:set var="status" value="${param.status}" scope="session"/>
<c:set var="start" value="${param.start}" scope="session"/>
<c:set var="agency" value="${param.agency}" scope="session"/>
<c:set var="keyword" value="${param.keyword}" scope="session"/>
<c:set var="beginDate" value="${param.beginDate}" scope="session"/>
<c:set var="endDate" value="${param.endDate}" scope="session"/>
<c:set var="dateComparison" value="${param.dateComparison}" scope="session"/>
<c:set var="sizeComparison" value="${param.sizeComparison}" scope="session"/>
<c:set var="width" value="${param.selectedWidth}" scope="session"/>
<c:set var="height" value="${param.selectedHeight}" scope="session"/>
<%
String[] ids = request.getParameterValues("imagesSelected");
String updateStatus = request.getParameter("updateStatus");
String author = request.getRemoteUser();
ImageImpl image;
Date currentDate = new Date();

if (ids != null) {
	for (String id : ids) {
		image = (ImageImpl)ImageHelper.getImageById(new Integer(id));
		image.setAuthor(author);
		if (updateStatus.equals(ImageHelper.PUBLISHED)) {
			image.setStatus(ImageHelper.PUBLISHED);
			image.setPublishDate(currentDate);
		} 
		else if (updateStatus.equals(ImageHelper.EXPIRED)) {
			image.setStatus(ImageHelper.EXPIRED);
			image.setExpirationDate(currentDate);		
		}
		else if (updateStatus.equals(ImageHelper.DELETED)) {
			image.setStatus(ImageHelper.DELETED);
		}
		ImageHelper.editImage(image);
		ImageHelper.publishImage(image);
	}
}
%>
<getters:getImages attributeName="images" size="50" start="${start}" 
	title="${title}" status="${status}" agency="${agency}" 
	keyword="${keyword}" beginDate="${beginDate}" endDate="${endDate}" 
	dateComparison="${dateComparison}" width="${width}" height="${height}"
	sizeComparison="${sizeComparison}" />
<getters:getImages attributeName="imageSize" isCount="${true}"
	title="${title}" status="${status}" agency="${agency}" 
	keyword="${keyword}" beginDate="${beginDate}" endDate="${endDate}" 
	dateComparison="${dateComparison}" width="${width}" height="${height}"
	sizeComparison="${sizeComparison}" />
<image:imageEntries images="${images}" size="${imageSize}" status="${status}" imageIndex="${start}" />