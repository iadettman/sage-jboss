<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="agency" tagdir="/WEB-INF/tags/custom/tools/agency"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<c:set var="status" value="${param.status}" scope="session"/>
<c:set var="sortBy" value="${param.sortBy}" scope="session"/>
<getters:getAgencies attributeName="agencies" status="${status}" sortBy="${sortBy}" />
<agency:agencyListings agencies="${agencies}" />