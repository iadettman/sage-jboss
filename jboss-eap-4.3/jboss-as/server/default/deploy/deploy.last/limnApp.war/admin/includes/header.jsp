<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getToolsEditions attributeName="editions"/>
<div class="header">
	<div class="header_container" >
		<c:if test="${not empty editions and fn:length(editions) gt 1}">
		<div id="new_edition_container">Edition: <a id="new_edition_indicator" href="javascript:;" class="${edition}">${edition}</a>
			<ul id="new_edition_selector">
			<c:forEach items="${editions}" var="ed">
				<li class="${ed.edition}"><a href="/blogtools/admin/index.jsp?edition=${ed.edition}">${ed.descr}</a></li>
			</c:forEach>
			</ul>
		</div>
		</c:if>
		Logged in as: ${pageContext.request.remoteUser} (<a href="/blogtools/logout.jsp" title="Log out of the Admin Tool">log out</a>)&nbsp;&nbsp;
	</div>
</div>