<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ page import="java.util.Date" %>
<%@ page import="com.ceg.online.limn.helpers.ImageHelper" %>
<%@ page import="com.ceg.online.limn.beans.impl.ImageImpl" %>
<%
Date currentDate = new Date();
String id = request.getParameter("imageid");
String status = request.getParameter("status");
String author = request.getRemoteUser();
boolean isLocked = request.getParameter("isLocked") != null 
		? ((String)request.getParameter("isLocked")).trim().equals("true") : false;
ImageImpl image;

if (id != null && !id.trim().equals("")) {
	image = (ImageImpl)ImageHelper.getImageById(new Integer(id));
	image.setAuthor(author);
	if (isLocked) {
		image.setAutoLockDate(new Date());
	}
	else {
		image.setAutoLockDate(null);
	}
	if (status.equals(ImageHelper.PUBLISHED)) {
		image.setStatus(ImageHelper.PUBLISHED);
		image.setPublishDate(currentDate);
	} 
	else if (status.equals(ImageHelper.EXPIRED)) {
		image.setStatus(ImageHelper.EXPIRED);
		image.setExpirationDate(currentDate);		
	}
	else if (status.equals(ImageHelper.DELETED)) {
		image.setStatus(ImageHelper.DELETED);
	}
	ImageHelper.editImage(image);
	ImageHelper.publishImage(image);
}
%>