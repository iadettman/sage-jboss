<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@ page import="com.ceg.online.limn.beans.impl.GalleryItemImpl" %>
<%@ page import="com.ceg.online.limn.beans.impl.ImageImpl" %>
<%! static Log log = LogFactory.getLog("addGalleryItem.jsp");%>
<%
	String galleryItemStr = request.getParameter("galleryItems");	
	String[] imageArray;
	String[] imageVar;
	List<ImageImpl> imageItems = new ArrayList<ImageImpl>();
	ImageImpl image = null;

	if (galleryItemStr != null && galleryItemStr.length() > 0) {
		imageArray = galleryItemStr.split(";;");
		for (int index = 0; index < imageArray.length; ++index) {
			imageVar = imageArray[index].split("@@");
			//Check that the imageVar array has at least 2 elements
			if (imageVar != null && imageVar.length > 1) {
				image = new ImageImpl();
				image.setId(new Integer(imageVar[0]));
				image.setFilePath(imageVar[1]);
				imageItems.add(image);
			}
			else {
				if (imageVar == null) {
					log.error("imageVar in addGalleryItem is null for galleryItemStr: " + galleryItemStr);
				}
				else {
					log.error("imageVar in addGalleryItem length is not at least 2 for galleryItemStr: " 
							+ galleryItemStr + " with imageVar content of " + imageVar.toString());
				}
			}
		}
	}
	request.setAttribute("imageItems", imageItems);
	request.setAttribute("newItem", true);
%>
<c:forEach var="image" items="${imageItems}" varStatus="status">
<div id="photoGallery_seg${status.count+param.size}">
	<div class="photoblock active" id="photoClosed_seg${status.count+param.size}" onClick="editPhotoBlock('${status.count+param.size}');"<c:if test="${newItem}">style="display:none"</c:if>>
		<table><tbody><tr>
			<td class="photo">
				<c:if test="${not empty image.id}">		
					<img id="galleryItemThumb_seg${status.count+param.size}" name="galleryItemThumb_seg${status.count+param.size}"
						src="${image.filePath}" class="thumb vertical">							
				</c:if>			
				<img id="galleryItemThumbSec_seg${status.count+param.size}" name="galleryItemThumbSec_seg${status.count+param.size}" class="invisible" />									
				<input type="hidden" value="false" id="isDirty_seg${status.count+param.size}" name="isDirty_seg${status.count+param.size}"/>	
				<input type="hidden" value="${image.id}" id="primaryPhotoId_seg${status.count+param.size}" name="primaryPhotoId_seg${status.count+param.size}"/>	
				<input type="hidden" value="${secondary}" id="secondaryPhotoId_seg${status.count+param.size}" name="secondaryPhotoId_seg${status.count+param.size}"/>	
				<input type="hidden" value="<c:out value="" />" id="photoTitle_seg${status.count+param.size}" name="photoTitle_seg${status.count+param.size}"/>
				<input type="hidden" value="<c:out value="" />" id="photoCaption_seg${status.count+param.size}" name="photoCaption_seg${status.count+param.size}"/>
				<input type="hidden"  value="" name="galleryItemId_seg${status.count+param.size}" id="galleryItemId_seg${status.count+param.size}"/>
				<c:if test="${isRating}">
					<input type="hidden"  value="" id="photoRating_seg${status.count+param.size}" name="photoRating_seg${status.count+param.size}"/>
				</c:if>
				<input type="hidden"  value="" id="photoAdKeywords_seg${status.count+param.size}" name="photoAdKeywords_seg${status.count+param.size}"/>
			</td>
			<td class="descr">
				<a href="javascript:;" onclick="LimnGalleryItem.DeleteBlock('${id}','', '${status.count+param.size}');" class="removelink">[x]</a>
				<span id="displayTitle_seg${status.count+param.size}"></span>						
			</td>						  	
			</tr></tbody>
		</table>
	</div>			
	<div class="photoblock textblock" id="photoOpen_seg${status.count+param.size}" <c:if test="${!newItem}">style="display:none;"</c:if> >
		<table>
			<tr>
				<td class="label"><p>Photo #1:</p></td>
				<td class="field">
					<div id="displayPhoto_seg1_${status.count+param.size}" class="imageFloat">
						<c:if test="${not empty image.id}">		
							<img id="openGalleryItemThumb_seg${status.count+param.size}" name="openGalleryItemThumb_seg${status.count+param.size}"
								src="${image.filePath}" class="thumb xsmall" />																
						</c:if>
					</div>		
					<input type="button" value="choose..." onclick="LimnGallery.PopUp('?callingForm=galleryForm&sectionUse=Galleries&sectionUse=Entire Site&property=temp_primaryPhotoId_seg${status.count+param.size}&displayPreview=displayPhoto_seg1_${status.count+param.size}&numImages=1', 'imageSelector')" class="choose">		
					<input type="hidden" id="temp_primaryPhotoId_seg${status.count+param.size}" value="${image.id}"/>				
				</td>
			</tr>
			<tr>	
				<td class="label"><p>Photo #2<br>(optional):</p></td>
				<td class="field">			
					<div id="displayPhoto_seg2_${status.count+param.size}" class="imageFloat">					
					</div>			
					<input type="button"  value="choose..." onclick="LimnGallery.PopUp('?callingForm=galleryForm&sectionUse=Galleries&sectionUse=Entire Site&property=temp_secondaryPhotoId_seg${status.count+param.size}&displayPreview=displayPhoto_seg2_${status.count+param.size}&numImages=1', 'imageSelector')" class="choose">							
					<input type="hidden" id="temp_secondaryPhotoId_seg${status.count+param.size}" value=""/>																													
				</td>
			</tr>			
			<tr>
				<td class="label"><p>Title:</p></td>
				<td class="field">
					<input type="text" class="textfield large" id="temp_photoTitle_seg${status.count+param.size}" />				
				</td>
			</tr>
			<tr>
				<td class="label"><p>Description:</p></td>
				<td class="field">
					<textarea class="synbody mceEditor" id="temp_photoCaption_seg${status.count+param.size}"></textarea>
					<p class="descr">0 characters, 150 characters suggested</p>
				</td>
			</tr>
			<c:if test="${isRating}">
				<tr>
	           		<td class="label"><p>Rating:</p></td>
          			<td class="field">
            			<select id="temp_photoRating_seg${status.count+param.size}">
            				<option>(choose...)</option>
            				<option value="1">1 star</option>
            				<option value="2">2 stars</option>
            				<option value="3">3 stars</option>
            				<option value="4">4 stars</option>
            				<option value="5">5 stars</option>
            			</select>
            		</td>
              	</tr>
			</c:if>	
       		<tr>              		              		
           		<td class="label"><p>Ad keyword:</p></td>
				<td class="field">
					<input class="textfield" type="text" id="temp_photoAdKeywords_seg${status.count+param.size}">
					<p class="descr">&nbsp;</p>
				</td>
        	</tr>
			<tr>
          		<td class="label">&nbsp;</td>
          		<td class="field">
           			<input type="button" value="Save" onClick="savePhotoBlock('${status.count+param.size}');">
           			&nbsp;&nbsp;OR&nbsp;&nbsp;
           			<a href="javascript:;" onclick="cancelPhotoBlock('${status.count+param.size}');">Cancel</a>
         		</td>
        	</tr>
       	</table>
	</div>
</c:forEach>