<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<c:set var="title" value="${param.title}" scope="session"/>
<c:set var="status" value="${param.status}" scope="session"/>
<c:set var="start" value="${param.start}" scope="session"/>
<c:set var="agency" value="${param.agency}" scope="session"/>
<c:set var="keyword" value="${param.keyword}" scope="session"/>
<c:set var="filename" value="${param.filename}" scope="session"/>
<c:set var="beginDate" value="${param.beginDate}" scope="session"/>
<c:set var="endDate" value="${param.endDate}" scope="session"/>
<c:set var="dateComparison" value="${param.dateComparison}" scope="session"/>
<c:set var="sizeComparison" value="${param.sizeComparison}" scope="session"/>
<c:set var="width" value="${param.selectedWidth}" scope="session"/>
<c:set var="height" value="${param.selectedHeight}" scope="session"/>
<c:set var="sortBy" value="${param.sortBy}" scope="session"/>
<getters:getImages attributeName="images" size="50" start="${start}" 
	title="${title}" status="${status}" agency="${agency}" 
	keyword="${keyword}" beginDate="${beginDate}" endDate="${endDate}" 
	dateComparison="${dateComparison}" width="${width}" height="${height}"
	sizeComparison="${sizeComparison}" sortBy="${sortBy}" filename="${filename}" />
<getters:getImages attributeName="imageSize" isCount="${true}"
	title="${title}" status="${status}" agency="${agency}" 
	keyword="${keyword}" beginDate="${beginDate}" endDate="${endDate}" 
	dateComparison="${dateComparison}" width="${width}" height="${height}"
	sizeComparison="${sizeComparison}" filename="${filename}" />
<image:imageChooserEntries images="${images}" total="${param.total}" size="${imageSize}" status="${status}" 
	imageIndex="${start}" sortBy="${sortBy}" />