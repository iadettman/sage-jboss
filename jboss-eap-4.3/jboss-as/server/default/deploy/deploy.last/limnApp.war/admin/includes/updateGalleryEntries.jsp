<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<c:set var="title" value="${param.title}" scope="session"/>
<c:set var="keyword" value="${param.keyword}" scope="session"/>
<c:set var="category" value="${param.category}" scope="session"/>
<c:set var="edition" value="${param.edition}" scope="session"/>
<c:set var="status" value="${param.status}" scope="session"/>
<c:set var="start" value="${param.start}" scope="session"/>
<c:set var="beginDate" value="${param.beginDate}" scope="session"/>
<c:set var="endDate" value="${param.endDate}" scope="session"/>
<c:set var="dateComparison" value="${param.dateComparison}" scope="session"/>
<c:set var="sortBy" value="${param.sortBy}" scope="session"/>
<getters:getGalleries attributeName="galleries" size="45" start="${start}"
	status="${status}" beginDate="${beginDate}" endDate="${endDate}" 
	dateComparison="${dateComparison}" sortBy="${sortBy}" edition="${edition}"
	title="${title}" keyword="${keyword}" category="${category}" />
<getters:getGalleries attributeName="gallerySize" isCount="${true}"
	status="${status}" beginDate="${beginDate}" endDate="${endDate}" 
	dateComparison="${dateComparison}" sortBy="${sortBy}" edition="${edition}"
	title="${title}"  keyword="${keyword}" category="${category}" />
<gallery:galleryEntries status="${status}" sortBy="${sortBy}" edition="${edition}"
	galleryIndex="${start}" category="${category}" galleries="${galleries}" size="${gallerySize}" />
