<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<html>
<head>
	<meta http-equiv="Content-Language" content="en">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}"  type="text/css">
</head>
<body>
	<tools:setEdition />
	<getters:getGalleryById attributeName="gallery" id="${param.id}" />
	<div>		
	<h3>
		Default Preview Page that shows the Gallery's information.<br />
		The preview body path can be configured by editing the property 'tools.config.gallery.preview'.
	</h3>
		<table cellspacing="0" cellpadding="0" border="0">
		<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td></td><td></td></tr>
		<tr><td></td><td>id:</td><td>${gallery.id}</td></tr>
		<tr><td></td><td>title:</td><td>${gallery.title}</td></tr>
		<tr><td></td><td>thumbnailId:</td><td>${gallery.imageAPI.id}</td></tr>
		<tr><td></td><td>thumbnail</td><td><image:imageSrc attributeName="imageSrcThumb" image="${gallery.imageAPI}" />      
		<img src="${imageSrcThumb}" class="thumb thumbnail" /> </td></tr>
		<tr><td></td><td>cssfile:</td><td>${gallery.cssFile}</td></tr>
		<tr><td></td><td>status:</td><td> ${gallery.status}</td></tr>
		<tr><td></td><td># of galleryitems:</td><td>${fn:length(gallery.galleryItemIds)}</td></tr>
		<tr><td></td><td>galleryitems:</td><td><c:forEach items="${gallery.galleryItemIds}" var="galleryItemIds" >${galleryItemIds.id},&nbsp;</c:forEach></td></tr>
		<tr><td></td><td>published on:</td><td>${gallery.publishDate}</td></tr>
		<tr><td></td><td>last modified on:</td><td> ${gallery.lastModDate}</td></tr>
		<tr><td></td><td>shared editions:</td><td><c:forEach items="${gallery.sharedEditionIds}" var="sharedIds" >${sharedIds},&nbsp;</c:forEach></td></tr>
		<tr><td></td><td>categories: </td><td><c:forEach items="${gallery.categoryIds}" var="galleryCat" >${galleryCat},&nbsp;</c:forEach></td></tr>
		<tr><td></td><td>keywords: </td><td>${gallery.keywordStr}</td></tr>
		<tr><td></td><td>adkeywords: </td><td>${gallery.adKeywordStr}</td></tr>
		<tr><td></td><td>display options:</td><td>${gallery.displayOptions}</td></tr>
		<tr><td></td><td>unique name:</td><td>${gallery.uniqueName}</td></tr>
		</table>
	</div>
</body>
</html>