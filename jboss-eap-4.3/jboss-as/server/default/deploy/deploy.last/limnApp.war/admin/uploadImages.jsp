<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<tools:setEdition />
<html>
    <head>
	   	<meta http-equiv="Content-Language" content="en">
	   	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	   	<title>Upload Images</title>
	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/jquery-1.3.2.min.js?x=${version.value}"></script> 
	   	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/jquery.uploadify.v2.0.3.min.js?x=${version.value}"></script> 
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/prototype.js?x=${version.value}"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/validation.js?x=${version.value}" ></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/scriptaculous.js?load=effects,builder"></script>
       	<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/lightbox.js?x=${version.value}"></script>
       	<script src="${pageContext.request.contextPath}/includes/js/tools.js?x=${version.value}" type="text/javascript"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/swfobject.js?x=${version.value}"></script> 
		<link rel="stylesheet" href="${pageContext.request.contextPath}/includes/css/lightbox.css?x=${version.value}" type="text/css" media="screen" />
       	<link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
       	<link href="${pageContext.request.contextPath}/includes/css/uploadify.css?x=${version.value}" rel="stylesheet" type="text/css">
       	<link href="${pageContext.request.contextPath}/includes/css/validation.css?x=${version.value}" rel="stylesheet" type="text/css">
	   	<style type="text/css">
			.dojoTabPaneWrapper {
		  		padding : 10px 10px 10px 10px;
			}
			.clean td { margin:0; padding:0; padding-top:0; }
	   	</style>
	   	<script type="text/javascript"> 
			jQuery(document).ready(function() { 
 				jQuery('#uploadifyitem').uploadify({ 
  					'uploader':  '${pageContext.request.contextPath}/includes/swf/uploadify.swf', 
  					'script':    '${pageContext.request.contextPath}/multipleImageUpload/', 
  					'folder':    '/tmp/', 
  					'queueID': 'fileQueue',
  					'multi':     true,
  					'auto':     true,
 					'cancelImg': '${pageContext.request.contextPath}/includes/images/cancel.png',
 					
 					//event: The event object.
 					//queueID: The unique identifier of the file that was cancelled.  
 					//fileObj: An object containing details about the file that was selected. 
 					//response: The data sent back from the server. 
 					//data: Details about the file queue. 
					onComplete: function(event, queueId, fileObj, response, data){
						if(response) {
							LimnImage.LoadUploadedImage(response, fileObj);
						}
           			}
				}); 
			}); 
		</script> 
	</head>
	<body class="mainpage editpage">
		<div class="topstrip"><!-- --></div>
		<jsp:include page="includes/header.jsp" />
		<table cellpadding="0" cellspacing="0" border="0" class="headtable">
			<div class="head">
				<div class="head_fns">&nbsp;&nbsp;<a href="/blogtools/admin/">< Back to Admin Home</a><strong></strong></div>
			   	<h1>Edit Image</h1>			   
				<div class="clear"></div>
			</div>
		</table>
		<form name="imageForm" id="imageForm" method="post" action="" />
		<div class="mainbody">		    
		    <h2>Upload Images
			<p class="descr"><span class="req">*</span> required fields for publishing</p>
			</h2>
			<image:imageUploadEdit id="${param.imageId}"/>
			<script type="text/javascript">
   				var valid = new Validation('imageForm', {onSubmit:false});
			</script>		
			<div class="controls">		
				<a class="newbtn smallbtn" href="javascript:;" onclick="if (valid.validate())LimnImage.CreateImages();">Create Images</a>		
				&nbsp;&nbsp;OR&nbsp;&nbsp;<a href="images.jsp">Cancel</a>			
			</div>
		</div>
		</form>
	<jsp:include page="includes/footer.jsp" />
	</body>
</html>
