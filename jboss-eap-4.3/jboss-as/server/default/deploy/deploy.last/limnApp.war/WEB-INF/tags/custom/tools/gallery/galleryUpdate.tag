<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ attribute name="edition" type="java.lang.String" required="true"%>
<%@ attribute name="id" type="java.lang.Integer" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<%@ attribute name="galleryItemIndex" type="java.lang.Integer" required="false"%>
<%@ attribute name="reorder" type="java.lang.Boolean" required="false"%>
<getters:getDBProperty attributeName="useTeaser" propertyName="tools.config.modules.teaser"/>
<getters:getDBProperty attributeName="useBackground" propertyName="tools.config.modules.background"/>
<getters:getDBProperty attributeName="useFrontDoorDisplay" propertyName="tools.config.modules.frontdoordisplay"/>
<c:if test="${empty galleryItemIndex}" >
	<c:set var="galleryItemIndex" value="0"/>
</c:if>
<c:if test="${not empty id}" >
	<getters:getGalleryById attributeName="gallery" id="${id}" />
</c:if>
<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td class="leftcol">			
		<table class="formtable" border="0" cellpadding="0" cellspacing="0">
			<input type="hidden" name="user" id="user" value="${pageContext.request.remoteUser}" />
			<input type="hidden" name="galleryid" id="galleryid" value="${gallery.id}" />
			<input type="hidden" name="edition" id="edition" value="${edition}" />
			<gallery:titleRow title="${gallery.title}" />
			<c:if test="${not empty gallery.publishDate}" >
				<gallery:publishDateRow date="${gallery.publishDate}" />
			</c:if>
			<gallery:thumbnailRow thumbnail="${gallery.imageAPI}" />
			<c:if test="${useTeaser.value}">
				<gallery:teaserRow teaser="${gallery.teaserImage}" />
			</c:if>
			<c:if test="${useBackground.value}">
				<gallery:backgroundRow background="${gallery.backgroundImage}" />
			</c:if>
			<gallery:cssFileRow cssFile="${gallery.cssFile}" />
			<c:choose>
				<c:when test="${reorder eq true}">
					<gallery:reorderPhotos total="30" gallery="${gallery}" galleryItemIndex="${galleryItemIndex}"/>
				</c:when>
				<c:otherwise>
					<gallery:photosRow total="15" gallery="${gallery}" galleryItemIndex="${galleryItemIndex}"/>
				</c:otherwise>
			</c:choose>
			<c:if test="${useFrontDoorDisplay.value}">
				<gallery:frontDoorDisplay frontDoorTitle="${gallery.promotedTitle}" 
					frontDoorSubhead="${gallery.promotedSubhead}" isPromoted="${gallery.promoted}" />
			</c:if>
		</table>
		</td>
		<td class="rightcol">
			<gallery:galleryRightCol gallery="${gallery}" edition="${edition}" />
		</td>
	</tr>
	</tbody>
</table>	