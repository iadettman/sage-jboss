<%@ tag import="java.util.Map"%>
<%@ tag import="java.util.HashMap"%>
<%@ tag import="com.ceg.online.limn.helpers.QueryHelper"%>
<%@ attribute name="attributeName" type="java.lang.String" required="true"%>
<%
	request.setAttribute(attributeName, QueryHelper.executeGalleryHQLQuery("from EditionLU as el where el.isToolsEnabled = true order by id", null, 0, Integer.MAX_VALUE, 2));
%>