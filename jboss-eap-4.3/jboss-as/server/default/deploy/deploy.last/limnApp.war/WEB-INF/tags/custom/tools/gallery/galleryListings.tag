<%@ tag import="java.util.List" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ attribute name="galleries" type="java.util.List" required="true"%>
<%@ attribute name="edition" type="java.lang.String" required="true"%>
<%@ attribute name="status" type="java.lang.String" required="true"%>
<%@ attribute name="category" type="java.lang.String" required="false"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<!--  Listing -->
<c:choose>
   <c:when test="${(sortBy eq 'title desc')}">
		<c:set var="titleSort" scope="page" value="title asc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'title asc')}">
		<c:set var="titleSort" scope="page" value="title desc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:otherwise>
		<c:set var="titleSort" scope="page" value="title desc"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${(sortBy eq 'lastmoddate desc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate asc"/>
		<c:set var="lastModClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'lastmoddate asc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>		
		<c:set var="lastModClass" scope="page" value="sorted"/>				
   </c:when>
   <c:otherwise>
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${(sortBy eq 'publishdate desc')}">
		<c:set var="publishSort" scope="page" value="publishdate asc"/>
		<c:set var="publishClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'publishdate asc')}">
		<c:set var="publishSort" scope="page" value="publishdate desc"/>
		<c:set var="publishClass" scope="page" value="sorted"/>				
   </c:when>
   <c:otherwise>
		<c:set var="publishSort" scope="page" value="publishdate desc"/>
   </c:otherwise>
</c:choose>
<table id="episode_list" border="0" cellpadding="0" cellspacing="0" width="100%">
 	<tbody>
	<tr>
  		<th class="${titleClass}"><a href="javascript:;" onClick="LimnGallery.FilterBySort('${titleSort}', '${status}');" >Title</a></th>
  		<th class="${publishClass}"><a href="javascript:;" onClick="LimnGallery.FilterBySort('${publishSort}', '${status}');" >Published On</a></th>
  		<th class="${lastModClass}"><a href="javascript:;" onClick="LimnGallery.FilterBySort('${expireSort}', '${status}');" >Last Modified</a></th>
  		<th> </th>
	</tr>
	<getters:getTopGalleries attributeName="topGalleryIds" edition="${edition}" isIdsOnly="${true}" />
   	<c:forEach var="gallery" items="${galleries}" varStatus="status">
	<tools:escapeQuotes attributeName="galleryTitle" originalStr="${gallery.title}" />
  	<c:set var="rowClass"  scope="page" value="row_one"/>
  	<c:if test="${(status.count % 2) == 0}"> 
		<c:set var="rowClass" scope="page" value="row_two"/>
	</c:if>
	<c:if test="${(gallery.status eq 'draft')}">
         <c:set var="rowClass" scope="page" value="${rowClass} draft"/>
	</c:if>
	<tr id="${gallery.id}" class="rowClass">
		<td class="title">
			<a title="Edit this" href="editGallery.jsp?id=${gallery.id}">${gallery.title}</a>
			<c:if test="${gallery.status ne 'published'}" >
				<span class="brick ${gallery.status}">${gallery.status}</span>
			</c:if>
		</td>
		<td class="pastdate"><fmt:formatDate pattern="MM/dd/yyyy, hh:mm a"
			          value="${gallery.publishDate}" /></td>
		<td class="recentdate"><fmt:formatDate pattern="MM/dd/yyyy, hh:mm a"
			          value="${gallery.lastModDate}" /></td>
		<td class="fns">
			<c:if test="${gallery.status eq 'edited'}">
				<a class="publink" onclick="LimnGallery.UpdateStatus('published', '${gallery.id}',
				'${galleryTitle}', '/admin/allGalleries.jsp')" href="javascript:;">Publish</a>
				|
			</c:if>
			<c:if test="${gallery.status eq 'edited' or gallery.status eq 'published'}">
				<a onclick="LimnGallery.UpdateStatus('hidden', '${gallery.id}', '${galleryTitle}', '/admin/allGalleries.jsp')" href="javascript:;">Hide</a>
				|
				<a href="preview.jsp?id=${gallery.id}">Preview</a>
				|
				<gallery:isTopGallery attributeName="isTopGallery" ids="${topGalleryIds}" id="${gallery.id}" />
				<c:choose>
					<c:when test="${isTopGallery}">
						<a class="disabledlink" href="javascript:;">[+]</a>
					</c:when>
					<c:otherwise>
						<a href="galleries.jsp?reorder=true&reorderId=${gallery.id}&reorderTitle=${galleryTitle}">[+]</a>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${gallery.status eq 'hidden'}">
				<a class="publink" onclick="LimnGallery.UpdateStatus('published', '${gallery.id}',
				'${galleryTitle}', '/admin/allGalleries.jsp')" href="javascript:;">Unhide</a>
			</c:if>
		</td>
	</tr>
    </c:forEach>
    <!--  End of Listing -->
   </tbody>
</table>