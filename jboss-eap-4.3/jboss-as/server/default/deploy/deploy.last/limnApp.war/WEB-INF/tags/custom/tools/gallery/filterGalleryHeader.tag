<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="edition" type="java.lang.String" required="true"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/dojo/dojo.js?x=${version.value}"></script>
<script language="Javascript">
    dojo.require("dojo.widget.DropdownDatePicker");
</script>	
<form action="" method="post" name="galleriesListingForm" id="galleriesListingForm">  
<div id="filteropen">
	<div class="listfilter">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr><td><strong>Filter:</strong></td><td></td></tr>
			<tr>
				<td style="padding-right:1em;">
					<div id="filterFields" style="margin:0 auto;">		
						<input type="hidden" name="edition" id="edition" value="${edition}" />									
						<select class="filterselect field"><option>title</option></select>
						<select class="filterselect operator"><option>contains</option></select>
						<input type="text" name="title" id="title" value="${title}" class="valuefield" size="30">
						<br /><br />
						<select class="filterselect field"><option>keyword</option></select>
						<select class="filterselect operator"><option>contains</option></select>
						<input type="keyword" name="keyword" id="keyword" value="${keyword}" class="valuefield" size="30">
						<br /><br />

						<div style="clear:left;width:100%;">
							<select name="dateComparison" id="dateComparison" class="filterselect field">
		                    	<option value="modified">last modified</option>
                            	<option value="published">publish date</option>
                            </select>
														
							<div id="searchByDate">
								<div class="dateLabel">&nbsp;from&nbsp;</div> 
								<div dojoType="dropdowndatepicker" inputName="beginDate" inputId="beginDate" date="${beginDate}" class="datepicker">
						    	</div>
								<div class="dateLabel">&nbsp;to&nbsp;</div> 
								<div dojoType="dropdowndatepicker" inputName="endDate" inputId="endDate" date="${filterDateBefore}" class="datepicker">
								</div>
							</div>
							<br />							
						</div>
					</div>
				</td>
				<td>
					<a class="newbtn smallerbtn" title="Filter Galleries" href="javascript:;" onClick="LimnGallery.Filter();" >Filter</a>
				</td>
			</tr>
		</table>
	</div>
</div>
</form>