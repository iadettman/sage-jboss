<%@ tag import="java.util.List" %>
<%@ tag import="java.util.Calendar" %>
<%@ tag import="java.util.Date" %>
<%@ tag import="com.ceg.online.limn.dto.ImageUsage" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true"%>
<%@ attribute name="imageUsage" type="java.util.List" required="true" %>
<%@ attribute name="days" type="java.lang.Integer" required="true"%>
<%
	int size = 0;
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DAY_OF_MONTH, days*-1);
	Date compareTime = cal.getTime();
	
	if (imageUsage != null) {
		for (Object usage : imageUsage) {
			if (((ImageUsage)usage).getPublishDate() != null 
					? compareTime.before(((ImageUsage)usage).getPublishDate()) : false) {
				size += 1;
			}
		}
	}
	request.setAttribute(attributeName, size);
%>