<%@ attribute name="image" type="com.ceg.online.limn.beans.ImageBean" required="true"%>
<%@ attribute name="editions" type="java.util.List" required="true"%>
<%@ attribute name="sites" type="java.util.List" required="true"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getImageUsageImageId attributeName="imageUsage" id="${image.id}" />
<getters:getDBProperty attributeName="currentSite" propertyName="tools.config.site"/>
<c:set var="htmlDetail" value="" />
<c:if test="${not empty imageUsage}"> 
<h4>Image Usage</h4>
<div class="related">
   	<b>Total: ${fn:length(imageUsage)}</b> 
   	<a id="image-usage-toggle-link" href="javascript:;" title="Show Usage Detail" >(show detail)</a>&nbsp;&nbsp;
   		<c:forEach var="siteItem" items="${sites}" >
   			<img src="/limn/includes/images/${fn:toLowerCase(siteItem[1])}.gif" alt="${siteItem[1]}"/>
   			<c:choose>
   				<c:when test="${fn:length(editions) > 1 and
   						(fn:toLowerCase(siteItem[1]) eq fn:toLowerCase(currentSite.value))}">
   					<c:forEach var="edItem" items="${editions}" >
   						<c:set var="usageCount" value="0" />
   						<c:forEach var="usage" items="${imageUsage}" >
   							<c:if test="${usage.usageSiteId eq siteItem[0] and usage.editionId eq edItem.id}">
   								<c:set var="usageCount" value="${usageCount +1}" />  
   								<div name="usageDetailHTML" style="display: none">
   								<img src="/limn/includes/images/${edItem.edition}.gif" />&nbsp;
   								<c:choose>
									<c:when test="${usage.galleryType}">
										<span class="descr"><fmt:formatDate value='${usage.publishDate}' type="both" dateStyle="short" timeStyle="short" /></span> 
										<a href="/limn/admin/editGallery.jsp?id=${usage.galleryId}&edition=${edItem.edition}">
											Gallery - ${usage.title}
										</a>
									</c:when>
									<c:when test="${usage.blogType}">
										<span class="descr"><fmt:formatDate value='${usage.publishDate}' type="both" dateStyle="short" timeStyle="short" /></span>
										<a href="/blogtools/admin/editBlogItem.jsp?contentId=${usage.postId}&edition=${edItem.edition}">
											Blog - ${usage.title}
										</a>
									</c:when>
								</c:choose>
								<br />
								</div>	
   							</c:if>
   						</c:forEach>
   						&nbsp;&nbsp;<img src="/limn/includes/images/${edItem.edition}.gif" />
   						${usageCount}
   					</c:forEach>
   				</c:when>
   				<c:otherwise>
   					<c:set var="usageCount" value="0" />
   					<c:forEach var="usage" items="${imageUsage}" >
   						<c:if test="${usage.usageSiteId eq siteItem[0]}">
   							<c:set var="usageCount" value="${usageCount +1}" />
   							<div name="usageDetailHTML" style="display: none">
   							<hr /><img src="/limn/includes/images/${fn:toLowerCase(siteItem[1])}.gif" alt="${siteItem[1]}" />&nbsp;
   								<c:choose>
									<c:when test="${usage.galleryType}">
										<span class="descr"><fmt:formatDate value='${usage.publishDate}' type="both" dateStyle="short" timeStyle="short" /></span> 
										Gallery - ${usage.title}
									</c:when>
									<c:when test="${usage.blogType}">
										<span class="descr"><fmt:formatDate value='${usage.publishDate}' type="both" dateStyle="short" timeStyle="short" /></span>
										Blog - ${usage.title}
									</c:when>
								</c:choose>
								<br />
							</div>	
   						</c:if>
   					</c:forEach>
   					${usageCount}
   				</c:otherwise>
   			</c:choose>&nbsp;&nbsp;&nbsp;
   		</c:forEach>
	<div name="detailHTML" id="detailHTML" />
</div>
</c:if>