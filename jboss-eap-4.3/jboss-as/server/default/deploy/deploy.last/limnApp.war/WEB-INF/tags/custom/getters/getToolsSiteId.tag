<%@ tag import="java.util.List"%>
<%@ tag import="com.ceg.online.limn.helpers.QueryHelper"%>
<%@ attribute name="attributeName" type="java.lang.String" required="true"%>
<%@ attribute name="siteName" type="java.lang.String" required="true"%>
<%
	if (siteName != null && !siteName.equals("")) {
		List results = QueryHelper.executeGallerySQLQuery("select siteid from site_lu where sitename = '" + siteName + "'", null, 0, Integer.MAX_VALUE, 2);
		Integer siteId = results != null && results.size() > 0 ? (Integer)results.get(0) : 1;
		request.setAttribute(attributeName, siteId);
	}
%>