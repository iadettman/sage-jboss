<%@ tag import="java.util.List" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ attribute name="agencies" type="java.util.List" required="true"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<c:if test="${empty status}">
	<c:set var="status" value="published" />
</c:if>
<div class="mainbody">
<!--  Listing -->
<c:choose>
   <c:when test="${(sortBy eq 'title desc')}">
		<c:set var="titleSort" scope="page" value="title asc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'title asc')}">
		<c:set var="titleSort" scope="page" value="title desc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:otherwise>
		<c:set var="titleSort" scope="page" value="title desc"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${(sortBy eq 'lastmoddate desc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate asc"/>
		<c:set var="lastModClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'lastmoddate asc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>		
		<c:set var="lastModClass" scope="page" value="sorted"/>				
   </c:when>
   <c:otherwise>
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>
   </c:otherwise>
</c:choose>
<a href="${pageContext.request.contextPath}/admin/editAgency.jsp" class="newbtn smallbtn">Add New Agency</a>
<br/><br/>
<table id="episode_list" border="0" cellpadding="0" cellspacing="0" width="100%">
 	<tbody>
	<tr>
  		<th class="${titleClass}"><a href="javascript:;" onClick="LimnAgency.FilterBySort('${titleSort}', '${status}');" >Agency Name</a></th>
  		<th class="${expireClass}"><a href="javascript:;" onClick="LimnAgency.FilterBySort('${lastModSort}', '${status}');" >Last Modified</a></th>
  		<th></th>
	</tr>
   	<c:forEach var="agency" items="${agencies}" varStatus="status">
   	<tools:escapeQuotes attributeName="agencyTitle" originalStr="${agency.title}" />
  	<c:set var="rowClass"  scope="page" value="row_one"/>
  	<c:if test="${(status.count % 2) == 0}"> 
		<c:set var="rowClass" scope="page" value="row_two"/>
	</c:if>
    <tr class="${rowClass}">
		<td class="title">
			<a href="editAgency.jsp?agencyId=${agency.id}" class="articlelink">${agency.title}</a>
 		</td>
        <td class="pastdate">
			<fmt:formatDate value="${agency.lastModDate}" type="both" pattern="MM/dd/yyyy" />
        </td>      
        <td class="fns">
			<a onClick="LimnAgency.UpdateStatus(${agency.id}, 'deleted')" href="javascript:;">
				<img width="9" height="10" border="0" src="/limn/includes/images/icon_delete.gif"/>
			</a>
		</td>               
    </tr>
    </c:forEach>
    <!--  End of Listing -->
   </tbody>
</table>
</div>