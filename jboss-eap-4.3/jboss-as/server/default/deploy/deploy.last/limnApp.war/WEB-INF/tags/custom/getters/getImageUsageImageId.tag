<%@ tag import="com.ceg.online.limn.helpers.ImageHelper" %>
<%@ tag import="com.ceg.online.helpers.BlogDataHelper" %>
<%@ tag import="com.ceg.online.limn.dto.ImageUsage" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.ArrayList" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="id" type="java.lang.Integer" required="true" %>
<%
if (id != null && id.intValue() >= 0) {
	List<ImageUsage> results = ImageHelper.getImageUsage(id);
	request.setAttribute(attributeName, results);
}
%>