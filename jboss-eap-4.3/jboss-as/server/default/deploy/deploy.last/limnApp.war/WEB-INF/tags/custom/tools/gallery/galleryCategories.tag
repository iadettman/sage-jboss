<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ attribute name="gallery" type="com.ceg.online.limn.beans.GalleryBean" required="true" %>
<%@ attribute name="edition" type="java.lang.String" required="true" %>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/><%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<h4>Categories<p class="descr">Used to categorize this gallery entry. Start typing a category and choose from the popup list.</p></h4>
<table id="galleryItemCategories">
<tbody>
	<tr>
		<td>
			<div class="searchcontrols">
				<input type="text" id="categorySearchKeyword" name="categorySearchKeyword"/>
			</div>

			<div id="dynaCatagoriesDiv">
				<gallery:dynaCategories gallery="${gallery}" />
			</div>
		</td>
	</tr>
</tbody>
</table>
<!--<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/hackedAutocompleter.js?x=${version.value}"></script>
<script type="text/javascript">
	new HackedAutocompleter('categorySearch','categorySearchUpdate', '${pageContext.servletContext.contextPath}/admin/includes/galleryCategoriesList.jsp', {afterUpdateElement: LimnGallery.AddCategory});
</script>-->