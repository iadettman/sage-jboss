<%@ tag import="com.ceg.online.limn.beans.ImageBean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ attribute name="teaser" type="com.ceg.online.limn.beans.ImageBean" required="true"%>
<tr>
	<td class="label"><p>Teaser</p></td>
	<td class="field">
		<div id="displayTeaser" class="imageFloat">
            <c:if test="${not empty teaser}">
              	<image:imageSrc attributeName="imageSrc" image="${teaser}" />      
		        <img src="${imageSrc}" class="thumb thumbnail" />
		        <a class="deletephotolink" onclick="LimnGallery.RemoveImage('displayTeaser', 'teaserContentID');" href="javascript:;">[x]</a>
            </c:if>
        </div>
        <input type="hidden" name="teaserLink" value="${teaser.link}" id="teaserLink">
		<input type="button" name="x" value="choose..." onclick="LimnGallery.PopUp('?callingForm=galleryForm&property=teaserContentID&displayPreview=displayTeaser&numImages=1&displayProperty=teaserLink', 'imageSelector')" class="choose">
		<div>
		<input type="hidden" name="teaserContentID" value="${teaser.id}" id="teaserContentID">
		</div>
	</td>
</tr>