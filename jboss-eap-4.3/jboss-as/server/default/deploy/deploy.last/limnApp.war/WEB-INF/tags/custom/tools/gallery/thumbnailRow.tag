<%@ tag import="com.ceg.online.limn.beans.ImageBean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ attribute name="thumbnail" type="com.ceg.online.limn.beans.ImageBean" required="true"%>
<tr>
	<td class="label"><p>Thumbnail<span class="req">*</span></p></td>
	<td class="field">
		<div id="displayThumbnail" class="imageFloat">
            <c:if test="${not empty thumbnail}">
              	<image:imageSrc attributeName="imageSrc" image="${thumbnail}" />      
		        <img src="${imageSrc}" class="thumb thumbnail" />
		        <a class="deletephotolink" onclick="LimnGallery.RemoveImage('displayThumbnail', 'thumbnailContentID');" href="javascript:;">[x]</a>
            </c:if>
        </div>
        <input type="hidden" name="thumbnail.link" value="${thumbnail.link}" id="thumbnailLink">
		<input type="button" name="x" value="choose..." onclick="LimnGallery.PopUp('?callingForm=galleryForm&property=thumbnailContentID&displayPreview=displayThumbnail&numImages=1&displayProperty=thumbnailLink', 'imageSelector')" class="choose">
		<div>
		<input type="hidden" class="required" name="thumbnail.contentID" value="${thumbnail.id}" id="thumbnailContentID">
		</div>
	</td>
</tr>