<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="size" type="java.lang.Integer" required="true"%>
<%@ attribute name="galleries" type="java.util.List" required="true"%>
<%@ attribute name="edition" type="java.lang.String" required="true"%>
<%@ attribute name="category" type="java.lang.String" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<%@ attribute name="galleryIndex" type="java.lang.String" required="false"%>
<div id="gallery_entries">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
	<tr>
		<td>
			<table class="listcontrols listcontrolstop">
			<tbody>
				<tr>
					<td>
						<form action="" method="post" name="galleriesDropDownForm" id="galleriesDropDownForm">  
						<div class="paging">
						<strong>Show:</strong>
		       	 		<select name="status" id="status" class="" onChange="LimnGallery.Filter()">
	            			<option value="published,edited,hidden">(all)</option>
                  	  		<option <c:if test="${status eq 'hidden'}">selected</c:if>>hidden</option>
	              	  		<option <c:if test="${status eq 'edited'}">selected</c:if>>edited</option>
                		</select> 
						<select class="named" id="named" onchange="LimnGallery.Filter()" >
							<option selected="selected" value="">(all)</option>
							<option value="named">named</option>
						</select>
						<span class="lowercase">Galleries</span>
						</div>
						</form>
					</td>
					<td align="right">
						<gallery:galleryPaging size="${size}" status="${status}" 
							category="${category}" galleryIndex="${galleryIndex}" total="45"/>
					</td>
				</tr>
			</tbody>
			</table>
			<gallery:galleryListings galleries="${galleries}" sortBy="${sortBy}"
				status="${status}" category="${category}" edition="${edition}" />
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
 				<tr>
	       			<td>
 			   		</td>
   		   			<td align="right">
						<gallery:galleryPaging size="${size}" status="${status}" 
							category="${category}" galleryIndex="${galleryIndex}" total="45"/>
   		   			</td>
				</tr>
    		</table>
		</td>
	</tr>
</tbody>
</table>
</div>