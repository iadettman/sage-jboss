<%@ tag import="java.util.ArrayList" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.Hashtable" %>
<%@ tag import="com.ceg.online.limn.beans.ImageBean" %>
<%@ tag import="com.ceg.online.limn.helpers.ImageHelper" %>
<%@ tag import="com.ceg.online.limn.util.HibernateUtil" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="start" type="java.lang.Integer" required="false" %>
<%@ attribute name="size" type="java.lang.Integer" required="false" %>
<%@ attribute name="title" type="java.lang.String" required="false" %> 
<%@ attribute name="keyword" type="java.lang.String" required="false" %> 
<%@ attribute name="filename" type="java.lang.String" required="false" %> 
<%@ attribute name="width" type="java.lang.String" required="false" %> 
<%@ attribute name="height" type="java.lang.String" required="false" %> 
<%@ attribute name="sizeComparison" type="java.lang.String" required="false" %> 
<%@ attribute name="agency" type="java.lang.String" required="false" %> 
<%@ attribute name="dateComparison" type="java.lang.String" required="false" %> 
<%@ attribute name="beginDate" type="java.lang.String" required="false" %> 
<%@ attribute name="endDate" type="java.lang.String" required="false" %> 
<%@ attribute name="status" type="java.lang.String" required="false" %> 
<%@ attribute name="isCount" type="java.lang.Boolean" required="false" %>
<%@ attribute name="sortBy" type="java.lang.String" required="false" %> 
<%
List<ImageBean> imageList = null;
Hashtable<String, String> searchParams = new Hashtable<String, String>();

if (title != null && !title.trim().equals("")) {
	searchParams.put("title", title.trim());
}
if (width != null && !width.trim().equals("")) {
	searchParams.put("width", width);
}
if (height != null && !height.trim().equals("")) {
	searchParams.put("height", height);
}
if (keyword != null && !keyword.trim().equals("")) {
	searchParams.put("keyword", keyword.trim());
}
if (agency != null && !agency.trim().equals("")) {
	searchParams.put("agency", agency.trim());
}
if (sortBy != null && !sortBy.trim().equals("")) {
	searchParams.put("sortBy", sortBy);
}
if (filename != null && !filename.trim().equals("")) {
	searchParams.put("filename", filename);
}
if (sizeComparison != null && !sizeComparison.trim().equals("")) {
	if (sizeComparison.trim().equals(HibernateUtil.EQUAL) ||
			sizeComparison.trim().equals(HibernateUtil.GREATER) ||
			sizeComparison.trim().equals(HibernateUtil.LESSER)) {
		searchParams.put("sizeComparison", sizeComparison);
	}
}
if (dateComparison != null && !dateComparison.trim().equals("")) {
	if (dateComparison.trim().equals(HibernateUtil.EXPIRED) ||
			dateComparison.trim().equals(HibernateUtil.PUBLISHED) ||
			dateComparison.trim().equals(HibernateUtil.MODIFIED)) {
		searchParams.put("dateComparison", dateComparison);
	}
}
if (beginDate != null && !beginDate.trim().equals("")) {
	searchParams.put("beginDate", HibernateUtil.formatHibernateDate(beginDate));
}
if (endDate != null && !endDate.trim().equals("")) {
	searchParams.put("endDate", HibernateUtil.formatHibernateDate(endDate));
}
if (status != null && !status.trim().equals("")) {
	searchParams.put("status", status);
}
if (isCount != null && isCount.booleanValue()) {
	request.setAttribute(attributeName, ImageHelper.countImages(searchParams));
}
else {
	request.setAttribute(attributeName, ImageHelper.searchImages(searchParams, start, size));
}
%>