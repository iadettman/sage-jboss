<%@ attribute name="image" type="com.ceg.online.limn.beans.ImageBean" required="true"%>
<%@ attribute name="editions" type="java.util.List" required="true"%>
<%@ attribute name="sites" type="java.util.List" required="true"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="currentSite" propertyName="tools.config.site"/>
<c:if test="${not empty image.relatedImageIds}"> 
<h4>Related Images</h4>
<div class="related">
<c:forEach var="relatedImage" items="${image.relatedImages}" >
	<getters:getImageUsageImageId attributeName="imageUsage" id="${relatedImage.id}" />
	<a href="editImage.jsp?imageId=${relatedImage.id}" class="articlelink">${fn:substring(relatedImage.title,0,80)}</a>
	<span class="descr">${relatedImage.sourceWidth} x ${relatedImage.sourceHeight} px</span>&nbsp;&nbsp;
   		<c:forEach var="siteItem" items="${sites}" >
   			<img src="/limn/includes/images/${fn:toLowerCase(siteItem[1])}.gif" alt="${siteItem[1]}"/>
   			<c:choose>
   				<c:when test="${fn:length(editions) > 1 and
   						(fn:toLowerCase(siteItem[1]) eq fn:toLowerCase(currentSite.value))}">
   					<c:forEach var="edItem" items="${editions}" >
   						<c:set var="usageCount" value="0" />
   						<c:forEach var="usage" items="${imageUsage}" >
   							<c:if test="${usage.usageSiteId eq siteItem[0] and usage.editionId eq edItem.id}">
   								<c:set var="usageCount" value="${usageCount +1}" />  
   							</c:if>
   						</c:forEach>
   						&nbsp;&nbsp;<img src="/limn/includes/images/${edItem.edition}.gif" />
   						${usageCount}
   					</c:forEach>
   				</c:when>
   				<c:otherwise>
					<c:set var="usageCount" value="0" />
   					<c:forEach var="usage" items="${imageUsage}" >
   						<c:if test="${usage.usageSiteId eq siteItem[0]}">
   							<c:set var="usageCount" value="${usageCount +1}" />
   						</c:if>
   					</c:forEach>
   					${usageCount}
   				</c:otherwise>
   			</c:choose>&nbsp;&nbsp;&nbsp;
   		</c:forEach>
   	<br />
</c:forEach>
</div>
</c:if>