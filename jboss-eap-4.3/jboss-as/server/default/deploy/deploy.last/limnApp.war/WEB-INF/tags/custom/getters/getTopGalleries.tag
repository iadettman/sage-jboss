<%@ tag import="com.ceg.online.limn.helpers.GalleryHelper"%>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="edition" type="java.lang.String" required="true" %> 
<%@ attribute name="isIdsOnly" type="java.lang.Boolean" required="false" %> 
<%
if (isIdsOnly != null && isIdsOnly) {
	request.setAttribute(attributeName, GalleryHelper.getTopGalleryIds(edition));
}
else {
	request.setAttribute(attributeName, GalleryHelper.getTopGalleries(edition));
}
%>