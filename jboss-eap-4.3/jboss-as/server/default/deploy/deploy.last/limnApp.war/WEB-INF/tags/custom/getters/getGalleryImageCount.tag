<%@ tag description="Gets the number of images in gallery" body-content="empty" language="java" dynamic-attributes="dynamicAttributes" %>
<%@ tag import="com.ceg.online.limn.helpers.GalleryHelper" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true"%>
<%@ attribute name="galleryId" type="java.lang.Integer" required="true" rtexprvalue="true" %>
<%
	request.setAttribute(attributeName, GalleryHelper.getGalleryImageCount(galleryId));
%>