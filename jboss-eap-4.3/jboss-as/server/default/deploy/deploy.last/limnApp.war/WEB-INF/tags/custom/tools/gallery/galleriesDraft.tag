<%@ tag import="java.util.List" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ attribute name="edition" type="java.lang.String" required="true"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<getters:getGalleries attributeName="galleries" edition="${edition}"
	sortBy="${sortBy}" status="draft" size="1000" />
<!--  Listing -->
<c:choose>
   <c:when test="${(sortBy eq 'title desc')}">
		<c:set var="titleSort" scope="page" value="title asc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'title asc')}">
		<c:set var="titleSort" scope="page" value="title desc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:otherwise>
		<c:set var="titleSort" scope="page" value="title desc"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${(sortBy eq 'lastmoddate desc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate asc"/>
		<c:set var="lastModClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'lastmoddate asc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>		
		<c:set var="lastModClass" scope="page" value="sorted"/>				
   </c:when>
   <c:otherwise>
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>
   </c:otherwise>
</c:choose>
<div id="panel1" class="panel">
<table id="episode_list" width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr>
		<th class="${titleClass}">
			<a href="galleries.jsp?draftSortBy=${titleSort}">Title</a>
		</th>
		<th class="${lastModClass}">
			<a href="galleries.jsp?draftSortBy=${lastModSort}">Last Modified</a>
		</th>
		<th> </th>
	</tr>
	<c:forEach items="${galleries}" var="gallery">
	<tools:escapeQuotes attributeName="galleryTitle" originalStr="${gallery.title}" />
	<tr id="${gallery.id}" class="row_one draft">
		<td class="title">
			<a title="Edit this" href="editGallery.jsp?id=${gallery.id}">${gallery.title}</a>
			<span class="brick ${gallery.status}">${gallery.status}</span>
		</td>
		<td class="recentdate"><fmt:formatDate pattern="MM/dd/yyyy, hh:mm a"
			value="${gallery.lastModDate}" /></td>
		<td class="fns">
			<a onClick="LimnGallery.AddAndPublish('${gallery.id}', '${galleryTitle}')"  href="javascript:;">[+] Add & Publish</a>
			|
			<a onClick="LimnGallery.UpdateStatus('deleted', '${gallery.id}', '${galleryTitle}')" href="javascript:;">
				<img width="9" height="10" border="0" src="/limn/includes/images/icon_delete.gif"/>
			</a>
		</td>
	</tr>
	</c:forEach>
	</tbody>
</table>
</div>