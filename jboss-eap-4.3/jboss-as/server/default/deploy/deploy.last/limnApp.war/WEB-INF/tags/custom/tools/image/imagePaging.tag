<%@ tag import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="size" type="java.lang.Integer" required="true"%>
<%@ attribute name="total" type="java.lang.Integer" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="true"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<%@ attribute name="imageIndex" type="java.lang.Integer" required="false"%>
<%@ attribute name="isImageChooser" type="java.lang.Boolean" required="false"%>
<c:if test="${empty imageIndex}" >
	<c:set var="imageIndex" value="0"/>
</c:if>
<c:if test="${empty total}" >
	<c:set var="total" value="50"/>
</c:if>
<div class="paging">
	<c:choose>
		<c:when test="${imageIndex eq 0}">
			<a href="javascript:;" class="disabledlink" >First</a>&nbsp;|&nbsp;
			<a href="javascript:;" class="disabledlink" >< Prev</a>
		</c:when>
		<c:otherwise>
			<a href="javascript:;" class="next" onClick="LimnImage.FilterByStartIndex('0', '${status}', '${sortBy}', '${isImageChooser}', '${total}')">First</a>&nbsp;|&nbsp;
			<a href="javascript:;" class="next" onClick="LimnImage.FilterByStartIndex('${imageIndex - total}', '${status}', '${sortBy}', '${isImageChooser}', '${total}')">< Prev</a>
		</c:otherwise>
	</c:choose>		
    |
   	<c:choose>
		<c:when test="${size < total}">
			${imageIndex}&nbsp;of&nbsp;${size}
		</c:when>
		<c:otherwise>	
			<select name="selectImageIndex" id="selectImageIndex" onChange="LimnImage.FilterByStartIndex(this.options[selectedIndex].value, 
				'${status}', '${sortBy}', '${isImageChooser}', '${total}')" >
				<c:forEach begin="0" end="${(size-1) / total}" varStatus="index" >
					<option value="${(total*index.count) - total}" <c:if test="${(total*index.count) - total eq imageIndex}">SELECTED</c:if>>
						${(total*index.count) - total + 1}&nbsp;-&nbsp;
						<c:choose>
							<c:when test="${imageIndex + (total*index.count) > size}">
								${size}
							</c:when>
							<c:otherwise>
								${(total*index.count)}
							</c:otherwise>
						</c:choose>		
					</option>
				</c:forEach>				
			</select>
			&nbsp;of&nbsp;${size}
            |
			<c:choose>
				<c:when test="${imageIndex + total >=  size}">
					<a href="javascript:;" class="disabledlink">Next ></a>&nbsp;|&nbsp;
					<a href="javascript:;" class="disabledlink">Last</a>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${(size % total) eq 0}">
							<c:set var="lastIndex" value="${size - total}" scope="page" />
						</c:when>
						<c:otherwise>
							<c:set var="lastIndex" value="${size - (size % total)}" scope="page" />
						</c:otherwise>
					</c:choose>	
					<a href="javascript:;" class="next" onClick="LimnImage.FilterByStartIndex('${imageIndex + total}', '${status}', '${sortBy}', '${isImageChooser}', '${total}')">Next ></a>&nbsp;|&nbsp;
					<a href="javascript:;" class="next" onClick="LimnImage.FilterByStartIndex('${lastIndex}', '${status}', '${sortBy}', '${isImageChooser}', '${total}')">Last</a>
				</c:otherwise>
			</c:choose>		
		</c:otherwise>	
	</c:choose>
</div>