<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="size" type="java.lang.Integer" required="true"%>
<%@ attribute name="total" type="java.lang.Integer" required="false"%>
<%@ attribute name="images" type="java.util.List" required="true"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<%@ attribute name="imageIndex" type="java.lang.String" required="false"%>
<getters:getDBProperty propertyName="tools.config.site" attributeName="currentSite" />
<%--${currentSite.value}--%>
<div id="listOfItems" class="imagepanel">
	<image:imagePaging size="${imageSize}" total="${total}" imageIndex="${imageIndex}"
		isImageChooser="${true}" status="${status}" sortBy="${sortBy}" />
	<c:set var="MAX_USAGE" value="10" />
	<div class="">
		<ul id="allItems">
    		<c:forEach var="image" items="${images}" varStatus="status">
    			<getters:getImageUsageImageId attributeName="imageUsage" id="${image.id}" />
    			<image:imageSrc attributeName="imageSrc" image="${image}" />
		        <c:set var="imageWidth"  scope="page" value="${image.sourceWidth}"/>
			    <c:set var="imageHeight" scope="page" value="${image.sourceHeight}"/>
				<c:set var="imageClass"  scope="page" value="vertical"/>
				<c:if test="${(image.sourceWidth >= image.sourceHeight)}">
					<c:set var="imageClass" scope="page" value="horizontal"/>
				</c:if>
				<c:choose>
	        		<c:when test="${image.locked}">
	        			<c:set var="liTitle" value="locked" />
	        		</c:when>
	        		<c:when test="${useImage eq false}">
	        			<c:set var="liTitle" value="noUsage" />
	        		</c:when>
	        		<c:otherwise>
	        			<c:set var="liTitle" value="${image.status}" />
	        		</c:otherwise>
				</c:choose>
	        	<li id="${image.id}" groupId="box2" status="${liTitle}">
	        		<div class="thumbnail-wrapper">
                        <img src="${imageSrc}" class="${imageClass} tooltip"/>
                        <div class="tooltip-content" style="display: none;">
                            <h4>${fn:substring(image.title,0,20)}</h4>
                            <c:if test="${useImage eq false}">
                                Not Available for this site.<br />
                            </c:if>
                            Published on <fmt:formatDate value="${image.publishDate}" type="both" pattern="M/d/yyyy" />
                            <c:if test="${not empty image.author}">by ${image.author}</c:if><br/>
                            <c:choose>
                                <c:when test="${empty image.expirationDate}">
                                    Does not expire<br />
                                </c:when>
                                <c:otherwise>
                                    Expires on <fmt:formatDate value="${image.expirationDate}" type="date" dateStyle="short" /><br/>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${empty image.notes}">
                                </c:when>
                                <c:otherwise>
                                    <h5>Photo Notes</h5>
                                    ${image.notes}<br />
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${empty image.agencyCaption}">
                                </c:when>
                                <c:otherwise>
                                    <h5>Agency Caption</h5>
                                    ${image.agencyCaption}<br />
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${empty imageUsage}">
                                </c:when>
                                <c:otherwise>
                                    <h5>Usage</h5>
                                    <ol>
                                    <c:forEach var="usage" items="${imageUsage}" varStatus="status">
                                    	<c:if test="${status.count <= MAX_USAGE}">
                                        <tools:escapeHTML attributeName="usageTitle" originalStr="${usage.title}" />
                                        <li>
                                        <fmt:formatDate value='${usage.publishDate}' type="both" pattern="M/d/yy"/>
                                        <c:choose>
                                            <c:when test="${usage.galleryType}">
                                                Gallery: ${fn:substring(usageTitle,0,20)}..
                                            </c:when>
                                            <c:when test="${usage.blogType}">
                                                Blog: ${fn:substring(usageTitle,0,20)}..
                                            </c:when>
                                        </c:choose>
                                        </li>
                                        </c:if>
                                    </c:forEach>
                                    </ol>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="status-icon-block">
                            <c:if test="${image.locked}">
                                <img class="status-icon lock" src="${pageContext.request.contextPath}/includes/images/lock.png" />
                            </c:if>
                            <c:if test="${useImage eq false}">
                                <img class="status-icon noUsage" src="${pageContext.request.contextPath}/includes/images/noUsage2.png" />
                            </c:if>
                        </div>
                    </div>
   		            <p>
		            	${fn:substring(image.title,0,20)}
		            </p>
		            <p class="dimensions">${imageWidth} x ${imageHeight}</p>
		            <c:choose>
 						<c:when test="${empty imageUsage}">
 						</c:when>
 						<c:otherwise>
 							<c:set var="days" value="30" />
 							<image:usedInPastDays attributeName="usagePastDays"
 									imageUsage="${imageUsage}" days="${days}" />
 							<c:if test="${usagePastDays > 0}" >
 								<p style="color:red;">Used ${usagePastDays}x last ${days} days</p>
 							</c:if>
 						</c:otherwise>
 					</c:choose>
 					<c:choose>
 						<c:when test="${empty image.notes}">
 						</c:when>
 						<c:otherwise>
 							<h4>
 								<img style="border-style:none" src="${pageContext.request.contextPath}/includes/images/notes.png" />
 								Photo Notes
 							</h4>
 						</c:otherwise>
 					</c:choose>

	        	</li>
           	</c:forEach>
		</ul>
	</div>
</div>