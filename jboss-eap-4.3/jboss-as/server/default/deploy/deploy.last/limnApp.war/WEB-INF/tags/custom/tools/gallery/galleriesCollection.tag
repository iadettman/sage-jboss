<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="edition" type="java.lang.String" required="true"%>
<%@ attribute name="size" type="java.lang.String" required="false"%>
<%@ attribute name="reorder" type="java.lang.Boolean" required="false"%>
<%@ attribute name="reorderId" type="java.lang.String" required="false"%>
<%@ attribute name="reorderTitle" type="java.lang.String" required="false"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:if test="${empty size}">
	<c:set var="size" value="50" />
</c:if>
<getters:getTopGalleries attributeName="topGalleries" edition="${edition}" />
<table id="episode_list" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>    
			<td class="section" colspan="7">
				<h3> Collection: Top Galleries ( ${size} ) </h3>
				<c:if test="${reorder eq true}" >
					<p class="prompt">Drag and drop articles to reorder their positions, or remove them from the collection.</p>
					<div class="controls">
					<a class="newbtn smallbtn pubbtn" href="javascript:;" onClick="LimnGallery.PublishOrder('${edition}');">Publish Order</a>
  						OR  
					<a href="galleries.jsp">Cancel</a>
					</div>
				</c:if>
			</td>
		</tr>
		<tr>
			<th>Title</th>
			<th><c:if test="${reorder ne true}" >Published On</c:if>&nbsp;</th>
			<th><c:if test="${reorder ne true}" >Last Modified</c:if>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
		<c:if test="${reorder ne true}" >
		<c:forEach items="${topGalleries}" var="gallery" varStatus="status" begin="0" end="50">
		<c:set var="rowClass"  scope="page" value="row_one"/>
		<tools:escapeQuotes attributeName="galleryTitle" originalStr="${gallery.title}" />
  		<c:if test="${(status.count % 2) == 0}"> 
			<c:set var="rowClass" scope="page" value="row_two"/>
		</c:if>
		<tr id="${gallery.id}" class="rowClass">
			<td class="title">
				<a title="Edit this" href="editGallery.jsp?id=${gallery.id}">${gallery.title}</a>
				<c:if test="${gallery.status ne 'published'}" >
					<span class="brick ${gallery.status}">${gallery.status}</span>
				</c:if>
			</td>
			<td class="pastdate"><fmt:formatDate pattern="MM/dd/yyyy, hh:mm a"
			value="${gallery.publishDate}" /></td>
			<td class="recentdate"><fmt:formatDate pattern="MM/dd/yyyy, hh:mm a"
			value="${gallery.lastModDate}" /></td>
			<td class="fns">
				<c:if test="${gallery.status ne 'published'}" >
					<a class="publink" onClick="LimnGallery.UpdateStatus('published', '${gallery.id}', '${galleryTitle}')" title="Publish this now" href="javascript:;">Publish</a>
					|
				</c:if>
				<c:if test="${gallery.status ne 'hidden'}" >
					<a class="publink" onClick="LimnGallery.UpdateStatus('hidden', '${gallery.id}', '${galleryTitle}')" title="Hide this now" href="javascript:;">Hide</a>
					|
				</c:if>
				<a title="Preview this now" href="preview.jsp?id=${gallery.id}">Preview</a>
			</td>
		</tr>
		</c:forEach>
		</c:if>
	</tbody>
</table>
<c:if test="${reorder eq true}" >
<ul id="sortablelist" class="sortablelist">
<c:if test="${not empty reorderId}" >
	<li id="ref_${reorderId}" class="new">
		<span class="brick handle">DRAG</span>
		<a class="articlelink" href="#">${reorderTitle}</a>
		<div class="clear"></div>
	</li>
</c:if>
<c:forEach items="${topGalleries}" var="gallery" varStatus="status" >
	<li id="ref_${gallery.id}">
		<span class="brick handle">DRAG</span>
		<a class="articlelink" href="#">${gallery.title}</a>
		<a class="deletelink" title="Remove this from the collection" onclick="if (confirmAction('removeArticle')) {removeStory(this)}" href="javascript:;">[x] Remove</a>
		<div class="clear"></div>
	</li>
</c:forEach>
</ul>
 <script type="text/javascript">
 // <![CDATA[
   Sortable.create("sortablelist",
     {dropOnEmpty:true,handle:'handle',constraint:'vertical'});
 // ]]>
 </script>
</c:if>