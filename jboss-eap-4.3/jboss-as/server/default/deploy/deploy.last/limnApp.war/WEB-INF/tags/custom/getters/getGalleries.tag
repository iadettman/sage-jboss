<%@ tag import="java.util.ArrayList" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.Hashtable" %>
<%@ tag import="com.ceg.online.limn.beans.GalleryBean" %>
<%@ tag import="com.ceg.online.limn.helpers.GalleryHelper"%>
<%@ tag import="com.ceg.online.limn.util.HibernateUtil" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="size" type="java.lang.Integer" required="false" %>
<%@ attribute name="start" type="java.lang.Integer" required="false" %>
<%@ attribute name="title" type="java.lang.String" required="false" %> 
<%@ attribute name="edition" type="java.lang.String" required="false" %> 
<%@ attribute name="keyword" type="java.lang.String" required="false" %> 
<%@ attribute name="category" type="java.lang.String" required="false" %> 
<%@ attribute name="dateComparison" type="java.lang.String" required="false" %> 
<%@ attribute name="beginDate" type="java.lang.String" required="false" %> 
<%@ attribute name="endDate" type="java.lang.String" required="false" %> 
<%@ attribute name="status" type="java.lang.String" required="false" %> 
<%@ attribute name="sortBy" type="java.lang.String" required="false" %> 
<%@ attribute name="isCount" type="java.lang.Boolean" required="false" %>
<%
Hashtable<String, String> searchParams = new Hashtable<String, String>();
final Integer DEFAULT_SIZE = 5000;

start = start != null && start >= 0 ? start : 0;
size = size != null && size >= 0 ? size : DEFAULT_SIZE;

if (title != null && !title.trim().equals("")) {
	searchParams.put("title", title.trim());
}
if (edition != null && !edition.trim().equals("")) {
	searchParams.put("edition", edition);
}
if (keyword != null && !keyword.trim().equals("")) {
	searchParams.put("keyword", keyword.trim());
}
if (sortBy != null && !sortBy.trim().equals("")) {
	searchParams.put("sortBy", sortBy);
}
if (dateComparison != null && !dateComparison.trim().equals("")) {
	if (dateComparison.trim().equals(HibernateUtil.EXPIRED) ||
			dateComparison.trim().equals(HibernateUtil.PUBLISHED) ||
			dateComparison.trim().equals(HibernateUtil.MODIFIED)) {
		searchParams.put("dateComparison", dateComparison);
	}
}
if (beginDate != null && !beginDate.trim().equals("")) {
	searchParams.put("beginDate", HibernateUtil.formatHibernateDate(beginDate));
}
if (endDate != null && !endDate.trim().equals("")) {
	searchParams.put("endDate", HibernateUtil.formatHibernateDate(endDate));
}
if (status != null && !status.trim().equals("")) {
	searchParams.put("status", status);
}
if (category != null && !category.trim().equals("")) {
	searchParams.put("category", category);
}
if (isCount != null && isCount.booleanValue()) {
	request.setAttribute(attributeName, GalleryHelper.countGalleries(searchParams));
}
else {
	request.setAttribute(attributeName, GalleryHelper.searchGalleries(searchParams, start, size));
}
%>