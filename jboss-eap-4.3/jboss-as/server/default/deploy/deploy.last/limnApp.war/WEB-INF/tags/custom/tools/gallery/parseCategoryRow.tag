<%@ attribute name="cat" type="com.ceg.online.categories.beans.CategoryBean" required="true"%>
<%@ attribute name="attributeName" type="java.lang.String" required="true"%>
<%@ attribute name="editionid" type="java.lang.Integer" required="true"%>
<%@ attribute name="editionDescr" type="java.lang.String" required="true"%>
<%@ tag import="org.apache.commons.logging.Log"%>
<%@ tag import="org.apache.commons.logging.LogFactory"%>
<%@ tag import="com.ceg.online.data.beans.dto.EditionLU" %>
<%@ tag import="com.ceg.online.categories.beans.CategoryBean" %>
<%@ tag import="com.ceg.online.categories.beans.CategoryTypeBean" %>
<%@ tag import="com.ceg.online.categories.beans.CategoryAliasBean" %>
<%@ tag import="com.ceg.online.categories.beans.CategoryDisplayBean" %>
<%@ tag import="java.util.List"%>
<%@ tag import="java.util.ArrayList"%>
<%@ tag import="com.ceg.online.categories.helpers.HelperFactory" %>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="currentSite" propertyName="tools.config.site"/>
<getters:getToolsSiteId attributeName="siteId" siteName="${currentSite.value}"/>
<%! static Log log = LogFactory.getLog("parseCategoryRow.tag"); %>
<%
if (cat != null && editionid != null && editionid.intValue() > 0) {
	try {
		Integer siteId = (Integer)request.getAttribute("siteId");
		List<CategoryTypeBean> catTypeList = HelperFactory.getCategoryTypeHelper().getCategoryTypes(cat.getId());
		List<CategoryAliasBean> catAliasList = HelperFactory.getCategoryAliasHelper().getAliases(cat.getId());
		List<CategoryBean> catRelatedList = HelperFactory.getCategoryHelper().getRelatedCategories(cat.getId());
		CategoryDisplayBean display = HelperFactory.getCategoryDisplayHelper().getDisplay(cat.getId(), siteId, editionid);
		//Use the string buffer to build the pipe delimited data to return to frontend
		//format is categoryId|category title|category types|category edition|category displayname|category aliases|category related to
		StringBuffer addTo = new StringBuffer();
	
		addTo.append("<span class=\"title\">"+cat.getName() + "</span>");	
		if (catTypeList != null && catTypeList.size() > 0) {
			addTo.append(" <span class=\"types\">(");
			for (CategoryTypeBean catType : catTypeList) {
				addTo.append(catType.getName() + ", ");
			}
			addTo = new StringBuffer(addTo.substring(0, addTo.lastIndexOf(",")));   
			addTo.append( ")</span>");
		}
		
		if (display != null && display.getDisplayName() != null) {
			if (!display.getDisplayName().trim().equals(display.getDisplayName())) {
				addTo.append(" <span class=\"displayName\">"+ editionDescr +" Display: "+ display.getDisplayName() + "</span>");
			}
		}
		else {
			addTo.append("<span class=\"missingDisplayName\">* Does not display in "+ editionDescr +" edition.</span>");
		}
	
		if (catAliasList != null && catAliasList.size() > 0) {
			addTo.append(" <span class=\"aliases\">Aliases: ");
			for (CategoryAliasBean catAlias : catAliasList) {
				addTo.append(catAlias.getName() + ", ");
			}
			addTo = new StringBuffer(addTo.substring(0, addTo.lastIndexOf(",")));	
			addTo.append("</span>");
		}
		
		if (catRelatedList != null && catRelatedList.size() > 0) {
			addTo.append(" <span class=\"relatedTo\">Related to: ");
			for (CategoryBean relatedCat : catRelatedList) {
				addTo.append(relatedCat.getName() + ", ");
			}
			addTo = new StringBuffer(addTo.substring(0, addTo.lastIndexOf(",")));
			addTo.append("</span>");
		}
		request.setAttribute(attributeName, addTo.toString());
	}
	catch (Exception ex) {
		log.error("Caught error in parseCategoryRow.tag", ex);
	}	
}
%>