<%@ tag import="com.ceg.online.limn.helpers.GalleryHelper"%>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="ids" type="java.util.List" required="true" %> 
<%@ attribute name="id" type="java.lang.Integer" required="true" %> 
<%
request.setAttribute(attributeName, ids.contains(id));
%>