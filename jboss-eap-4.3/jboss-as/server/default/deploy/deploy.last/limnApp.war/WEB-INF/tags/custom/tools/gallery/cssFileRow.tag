<%@ attribute name="cssFile" type="java.lang.String" required="true"%>
<tr>
	<td class="label">
		<p>CSS file</p>
	</td>
	<td class="field">
		<input type="text" name="cssFile" size="35" value="${cssFile}" class="textfield title"><br />
		<p class="descr">Applies to all pages</p>
	</td>
</tr>