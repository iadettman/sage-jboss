<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ attribute name="gallery" type="com.ceg.online.limn.beans.GalleryBean" required="true"%>
<%@ attribute name="galleryItemIndex" type="java.lang.Integer" required="true"%>
<%@ attribute name="total" type="java.lang.Integer" required="false"%>

<c:if test="${empty total}" >
	<c:set var="total" value="15"/>
</c:if>
<c:if test="${not empty gallery}">
	<getters:getGalleryImageCount attributeName="size" galleryId="${gallery.id}"/>
	<c:choose>
		<c:when test="${galleryItemIndex+total < size}">
			<c:set var="end" value="${galleryItemIndex+total}" />
		</c:when>
		<c:otherwise>
			<c:set var="end" value="${size}" />		
		</c:otherwise>
	</c:choose>		
	<getters:getGalleryItems attributeName="galleryItems" id="${gallery.id}"
		start="${galleryItemIndex}" end="${end}" />
</c:if>

<tr>
	<td class="label">
		<p>Photos</p>
	</td>
	<td class="field">
		<input type="hidden" name="photo_gallery" styleId="photo_gallery" value=""/>
		<div id="gallery_item_entries">
			<gallery:galleryItemPaging id="${gallery.id}" galleryItemIndex="${galleryItemIndex}"
				size="${size}" total="${total}" position="" />
			<gallery:galleryItemBlocks galleryItems="${galleryItems}" id="${gallery.id}" />
			<gallery:galleryItemPaging id="${gallery.id}" galleryItemIndex="${galleryItemIndex}" 
				size="${size}" total="${total}" position="_bottom" />
		</div>
	</td>
</tr>
