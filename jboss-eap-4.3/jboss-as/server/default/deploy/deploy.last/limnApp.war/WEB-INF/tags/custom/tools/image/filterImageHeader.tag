<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<getters:getDBProperty attributeName="version" propertyName="tools.config.version"/>
<%@ attribute name="total" type="java.lang.String" required="false"%>
<%@ attribute name="processedTitle" type="java.lang.String" required="false"%>
<%@ attribute name="processedStatus" type="java.lang.String" required="false"%>
<%@ attribute name="isImageChooser" type="java.lang.Boolean" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<form action="" method="post" name="imagesListingForm" id="imagesListingForm">  
<input type="hidden" name="status" id="status" value="${status}"/>
<div id="responseDiv" name="responseDiv">&nbsp;</div>
<c:if test="${not empty processedStatus}">
    <dl id="feedback" class="feedback_publish" style="">
        <dt/>
        <c:choose>
            <c:when test="${processedStatus eq 'error'}" >
                <dd>Error:&nbsp;<br />
                </dd>
                <dd>${processedTitle}<br />
                </dd>
            </c:when>
            <c:otherwise>
                <dd>The image has been updated to :&nbsp;${processedStatus}&nbsp:<br /> 
                </dd>
                <dd>${processedTitle}<br />
                </dd>
            </c:otherwise>
        </c:choose>
    </dl>
</c:if>

<div id="filteropen">
	<div class="listfilter">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody>
				<tr>
					<td><strong>Filter:</strong></td>
					<td>and</td></tr>
				<tr>
					<td>
						<select size="5" multiple="multiple" name="sectionUse">
  					  		<option value="all">(all site sections)</option>
			    			<c:forEach items="${sectionUse}" var="sections" varStatus="sectionStatus">
					         	<option value="${section}">${section}</option>
					        </c:forEach>
					    </select>
						<br>
 						</td>	
					<td>
					<%--<script type="text/javascript" src="${pageContext.request.contextPath}/includes/js/dojo/dojo.js?x=${version.value}"></script>--%>
				  	<%--<script language="Javascript">--%>
				       	<%--dojo.require("dojo.widget.DropdownDatePicker");--%>
			      	<%--</script>		--%>
			      	<table border="0" cellpadding="0" cellspacing="0">	        
			      		<tr>
			      		<td><select class="filterselect field"><option>title</option></select></td>
						<td><select class="filterselect operator"><option>contains</option></select></td>
						<td colspan="3"><input type="text" name="title" value="${title}"
								onkeydown="if (event.keyCode == 13) LimnImage.Filter('${isImageChooser}', '${total}' );" class="valuefield" size="30"></td>
						<td rowspan="5">
							<a class="newbtn smallerbtn" title="Filter Images" href="javascript:;" onClick="LimnImage.Filter('${isImageChooser}', '${total}' );" >Filter</a>
							<br><br><a href="#" onclick="LimnImage.ResetFields();">reset fields</a>
						</td>
						</tr>
						<tr>
                        <td><select name="dateComparison" id="dateComparison" class="filterselect field">
		                    <option value="modified">last modified</option>
                            <option value="expired">expire date</option>
                            </select>
                        </td>
						<td valign="middle">&nbsp;from</td>
						<td>
							<%--<div dojoType="dropdowndatepicker" inputName="beginDate" inputId="beginDate" date="${beginDate}" class="datepicker">--%>
						    <%--</div>--%>
                            <input type="text" name="beginDate" id="beginDate" class="datepicker" value="${beginDate}"/>
						</td>	 
						<td align="center">&nbsp;to&nbsp;</td>
						<td>
							<%--<div dojoType="dropdowndatepicker" inputName="endDate" inputId="endDate" date="${filterDateBefore}" class="datepicker">--%>
							<%--</div>--%>
                            <input type="text" name="endDate" id="endDate" class="datepicker" value="${endDate}"/>
						</td>
					</tr>
					<tr>
						<td><select class="filterselect field"><option>agency</option></select></td>
						<td><select class="filterselect operator"><option>contains</option></select></td>
						<td colspan="3"><input type="text" name="agency" id="agency" value="${agency}"
						 		onkeydown="if (event.keyCode == 13) LimnImage.Filter('${isImageChooser}', '${total}' );" class="valuefield" size="30"></td>
					</tr>
					<tr>
						<td><select class="filterselect field"><option>keyword</option></select></td>
						<td><select class="filterselect operator"><option>contains</option></select></td>
						<td colspan="3"><input type="text" name="keyword" id="keyword" value="${keyword}"
								onkeydown="if (event.keyCode == 13) LimnImage.Filter('${isImageChooser}', '${total}' );" class="valuefield" size="30"></td>
					</tr>
					<tr>
						<td><select class="filterselect field"><option>filename</option></select></td>
						<td><select class="filterselect operator"><option>contains</option></select></td>
						<td colspan="3"><input type="text" name="filename" id="filename" value="${filename}"
								onkeydown="if (event.keyCode == 13) LimnImage.Filter('${isImageChooser}', '${total}' );" class="valuefield" size="30"></td>
					</tr>
					<tr>
						<td><select class="filterselect field"><option>size</option></select></td>
						<td colspan="4">
						<select class="filterselect field" name="sizeComparison" id="sizeComparison">
							<option value="equal">is equal to</option>
							<option value="lesser">is less then</option>
							<option value="greater">is greater then</option>
						</select>
						<select id="selectedWidth" class="" style="margin-right: 2px;" name="selectedWidth">
							<option value="">all</option>
							<option <c:if test="${width eq 66}">selected="selected" </c:if> value="66">66</option>
							<option <c:if test="${width eq 102}">selected="selected" </c:if> value="102">102</option>
							<option <c:if test="${width eq 110}">selected="selected" </c:if> value="110">110</option>
							<option <c:if test="${width eq 118}">selected="selected" </c:if> value="118">118</option>
							<option <c:if test="${width eq 119}">selected="selected" </c:if> value="119">119</option>
							<option <c:if test="${width eq 123}">selected="selected" </c:if> value="123">123</option>
							<option <c:if test="${width eq 137}">selected="selected" </c:if> value="137">137</option>
							<option <c:if test="${width eq 150}">selected="selected" </c:if> value="150">150</option>
							<option <c:if test="${width eq 160}">selected="selected" </c:if> value="160">160</option>
							<option <c:if test="${width eq 244}">selected="selected" </c:if> value="244">244</option>
							<option <c:if test="${width eq 279}">selected="selected" </c:if> value="279">279</option>
							<option <c:if test="${width eq 285}">selected="selected" </c:if> value="285">285</option>
							<option <c:if test="${width eq 293}">selected="selected" </c:if> value="293">293</option>
							<option <c:if test="${width eq 300}">selected="selected" </c:if> value="300">300</option>
							<option <c:if test="${width eq 425}">selected="selected" </c:if> value="425">425</option>
						</select>
							&nbsp;x&nbsp; 
						<select id="selectedHeight" class="" style="margin-right: 2px;" name="selectedHeight">
							<option value="">all</option>
							<option <c:if test="${height eq 66}">selected="selected" </c:if> value="66">66</option>
							<option <c:if test="${height eq 79}">selected="selected" </c:if> value="79">79</option>
							<option <c:if test="${height eq 80}">selected="selected" </c:if> value="80">80</option>
							<option <c:if test="${height eq 96}">selected="selected" </c:if> value="96">96</option>
							<option <c:if test="${height eq 102}">selected="selected" </c:if> value="102">102</option>
							<option <c:if test="${height eq 131}">selected="selected" </c:if> value="131">131</option>
							<option <c:if test="${height eq 150}">selected="selected" </c:if> value="150">150</option>
							<option <c:if test="${height eq 159}">selected="selected" </c:if> value="159">159</option>
							<option <c:if test="${height eq 200}">selected="selected" </c:if> value="200">200</option>
							<option <c:if test="${height eq 206}">selected="selected" </c:if> value="206">206</option>
							<option <c:if test="${height eq 300}">selected="selected" </c:if> value="300">300</option>
							<option <c:if test="${height eq 315}">selected="selected" </c:if> value="315">315</option>
							<option <c:if test="${height eq 327}">selected="selected" </c:if> value="327">327</option>
							<option <c:if test="${height eq 473}">selected="selected" </c:if> value="473">473</option>
							<option<c:if test="${height eq 478}">selected="selected" </c:if>  value="478">478</option>
						</select>
						px
						</td>								
					   </tr> 
					  </table>	
					</td>
				</tr>
		  </tbody>
		</table>
	</div>
</div>
</form>