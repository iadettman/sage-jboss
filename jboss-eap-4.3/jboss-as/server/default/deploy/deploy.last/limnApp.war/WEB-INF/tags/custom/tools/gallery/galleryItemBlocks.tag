<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ attribute name="id" type="java.lang.Integer" required="true"%>
<%@ attribute name="galleryItems" type="java.util.List" required="true"%>
<input type="hidden" name="galleryItemSize" id="galleryItemSize" value="${fn:length(galleryItems)}" />
<div id="addedPhotos" name="addedPhotos"></div>
<c:forEach items="${galleryItems}" var="galleryItem" varStatus="status">
	<c:set var="newItem" value="" />
	<c:if test="${newItem}"><script type="text/javascript">photoBlockOpen = photoBlockOpen + 1;</script></c:if>
	<c:if test="${galleryItem.status ne 'deleted'}" >
		<div id="photoGallery_seg${status.count}">
			<div class="photoblock active" id="photoClosed_seg${status.count}" onClick="editPhotoBlock('${status.count}');"
				<c:if test="${newItem || galleryItem.image.status eq 'expired'}">
                	<c:choose>
                    	<c:when test="${newItem }">style="display:none"</c:when>
                        <c:otherwise>
                        	style="background-color:red;"
                        </c:otherwise>
       	            </c:choose>
               </c:if>
         	>
				<table><tbody><tr>
					<td class="photo">
						<c:choose>
							<c:when test="${not empty galleryItem.image.id}">
								<image:imageSrc attributeName="galleryItemSrc" image="${galleryItem.image}" /> 																														
								<img id="galleryItemThumb_seg${status.count}" name="galleryItemThumb_seg${status.count}"
									src="${galleryItemSrc}" class="thumb vertical">	
							</c:when>
							<c:otherwise>
								<img id="galleryItemThumb_seg${status.count}" name="galleryItemThumb_seg${status.count}" class="invisible" />
							</c:otherwise>
						</c:choose>
									
						<c:choose>
							<c:when test="${not empty galleryItem.secondaryImage.id}">
								<image:imageSrc attributeName="secGalleryItemSrc" image="${galleryItem.secondaryImage}" /> 																																												
								<img id="galleryItemThumbSec_seg${status.count}" name="galleryItemThumbSec_seg${status.count}"
									src="${secGalleryItemSrc}" class="thumb xsmall" />
							</c:when>
							<c:otherwise>
								<img id="galleryItemThumbSec_seg${status.count}" name="galleryItemThumbSec_seg${status.count}" class="invisible" />
							</c:otherwise>
						</c:choose>

						<input type="hidden" value="false" id="isDirty_seg${status.count}" name="isDirty_seg${status.count}"/>	
						<input type="hidden" value="${galleryItem.image.id}" id="primaryPhotoId_seg${status.count}" name="primaryPhotoId_seg${status.count}"/>	
						<input type="hidden" value="${secondary}" id="secondaryPhotoId_seg${status.count}" name="secondaryPhotoId_seg${status.count}"/>	
						<input type="hidden" value="<c:out value="${galleryItem.title}" />" id="photoTitle_seg${status.count}" name="photoTitle_seg${status.count}"/>
						<input type="hidden" value="<c:out value="${galleryItem.caption}" />" id="photoCaption_seg${status.count}" name="photoCaption_seg${status.count}"/>
						<input type="hidden"  value="${galleryItem.id}" name="galleryItemId_seg${status.count}" id="galleryItemId_seg${status.count}"/>
						<c:if test="${isRating}">
							<input type="hidden"  value="${galleryItem.rating}" id="photoRating_seg${status.count}" name="photoRating_seg${status.count}"/>
						</c:if>
						<input type="hidden"  value="${galleryItem.adKeywords}" id="photoAdKeywords_seg${status.count}" name="photoAdKeywords_seg${status.count}"/>
					</td>
					<td class="descr">
						<a href="javascript:;" onclick="LimnGalleryItem.DeleteBlock('${id}','${galleryItem.id}', '${status.count}');" class="removelink">[x]</a>
						<span id="displayTitle_seg${status.count}">${galleryItem.title}</span>						
					</td>						  	
				</tr></tbody></table>
			</div>	
					
			<div class="photoblock textblock" id="photoOpen_seg${status.count}"
                <c:if test="${!newItem}">style="display:none;"</c:if> >
				<table>
					<tr>
						<td class="label"><p>Photo #1:</p></td>
						<td class="field">
							<div id="displayPhoto_seg1_${status.count}" class="imageFloat">
								<c:if test="${not empty galleryItem.image.id}">		
									<img id="openGalleryItemThumb_seg${status.count}" name="openGalleryItemThumb_seg${status.count}"
										src="${galleryItemSrc}" class="thumb xsmall" />																
								</c:if>
							</div>		
							<input type="button" value="choose..." onclick="LimnGallery.PopUp('?callingForm=galleryForm&sectionUse=Galleries&sectionUse=Entire Site&property=temp_primaryPhotoId_seg${status.count}&displayPreview=displayPhoto_seg1_${status.count}&numImages=1', 'imageSelector')" class="choose">		
							<input type="hidden" id="temp_primaryPhotoId_seg${status.count}" value="${galleryItem.image.id}"/>				
						</td>
					</tr>
						
					<tr>	
						<td class="label"><p>Photo #2<br>(optional):</p></td>
						<td class="field">			
							<div id="displayPhoto_seg2_${status.count}" class="imageFloat">
								<c:if test="${not empty galleryItem.secondaryImage.id}">
									<img id="openGalleryItemThumbSec_seg${status.count}" name="openGalleryItemThumbSec_seg${status.count}" 
										src="${secGalleryItemSrc}" class="thumb xsmall" />																	
									<a href="javascript:;" class="deletephotolink" onclick="removeGalleryItemImage('displayPhoto_seg2_${status.count}', 'temp_secondaryPhotoId_seg${status.count}', 'secondaryPhotoId_seg${status.count}');">[x]</a>									
								</c:if>							
							</div>			
							<input type="button"  value="choose..." onclick="LimnGallery.PopUp('?callingForm=galleryForm&sectionUse=Galleries&sectionUse=Entire Site&property=temp_secondaryPhotoId_seg${status.count}&displayPreview=displayPhoto_seg2_${status.count}&numImages=1', 'imageSelector')" class="choose">							
							<input type="hidden" id="temp_secondaryPhotoId_seg${status.count}" value="${galleryItem.secondaryImage.id}"/>																													
						</td>
					</tr>
									
					<tr>
						<td class="label"><p>Title:</p></td>
						<td class="field">
							<input type="text" class="textfield large" id="temp_photoTitle_seg${status.count}" />				
						</td>
					</tr>
					
					
					<tr>
						<td class="label"><p>Description:</p></td>
						<td class="field">
							<textarea class="synbody mceEditor" id="temp_photoCaption_seg${status.count}"></textarea>
							<p class="descr">0 characters, 150 characters suggested</p>
						</td>
					</tr>
					<c:if test="${isRating}">
						<tr>
			                <td class="label"><p>Rating:</p></td>
            				<td class="field">
            					<select id="temp_photoRating_seg${status.count}">
            						<option>(choose...)</option>
            						<option value="1">1 star</option>
            						<option value="2">2 stars</option>
            						<option value="3">3 stars</option>
            						<option value="4">4 stars</option>
            						<option value="5">5 stars</option>
            					</select>
            				</td>
              			</tr>
					</c:if>	
				
              		<tr>              		              		
                		<td class="label"><p>Ad keyword:</p></td>
						<td class="field">
							<input class="textfield" type="text" id="temp_photoAdKeywords_seg${status.count}">
							<p class="descr">&nbsp;</p>
						</td>
              		</tr>
              		
              		
					<tr>
                		<td class="label">&nbsp;</td>
                		<td class="field">
                			<input type="button" value="Save" onClick="savePhotoBlock('${status.count}');">
                			&nbsp;&nbsp;OR&nbsp;&nbsp;
                			<a href="javascript:;" onclick="cancelPhotoBlock('${status.count}');">Cancel</a>
                		</td>
              		</tr>
              	</table>
			</div>
		</div>
		<script type="text/javascript">
			// only execute the editor loader if the window has already been loaded
			// (i.e. existing blocks should not be loaded in the loop
			tinyMCE.idCounter=getEditorIndex();
			tinyMCE.execCommand('mceAddControl',false,'temp_photoCaption_seg${status.count}');
		</script>
	</c:if>
</c:forEach>