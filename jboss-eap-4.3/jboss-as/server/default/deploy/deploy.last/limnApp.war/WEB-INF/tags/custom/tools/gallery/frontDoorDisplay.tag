<%@ tag import="com.ceg.online.limn.beans.ImageBean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ attribute name="frontDoorTitle" type="java.lang.String" required="true"%>
<%@ attribute name="frontDoorSubhead" type="java.lang.String" required="true"%>
<%@ attribute name="isPromoted" type="java.lang.Boolean" required="true"%>

<tr>
	<td class="label"><p>Front Door Display</td>
	<td class="field">
	<table>
		<tbody>
		<tr>
			<td class="label"><p>Title:</p></td>
			<td class="field">	    
				<input name="promotedTitle" maxlength="64" size="40" value="${frontDoorTitle}" id="promotedTitle" class="textfield large" type="text"><p class="descr"><span id="smallTitleCount">0</span> characters, <span class="maxLimit">64</span> characters maximum</p>
			</td>
		</tr>
		<tr>
			<td class="label"><p>Subhead:</p></td>
			<td class="field">	
				<textarea name="promotedSubhead" id="promotedSubhead" class="synbody">${frontDoorSubhead}</textarea><p class="descr"><span id="promotedSubheadCount">0</span> characters, <span class="maxLimit">80</span> maximum</p>
			</td>
		</tr>
		<tr>
    		<td class="label"><p></p></td>
	    	<td class="field">          
	        	<input name="promoted" id="promoted" <c:if test='${isPromoted}'>checked</c:if> value="true" onkeypress="return disableEnter(this,event)" type="checkbox" />
		        <img src="/limn/includes/images/starred.gif" border="0" />Star this item
	    	</td>
		</tr>
		</tbody>
	</table>
	</td>
</tr>