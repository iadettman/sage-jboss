<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="agency" tagdir="/WEB-INF/tags/custom/tools/agency"%>
<%@ attribute name="id" type="java.lang.String" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<getters:getAgencies attributeName="agencies" status="published" />
<getters:getToolsEditions attributeName="editions" />
<getters:getToolsSites attributeName="sites" />
<c:if test="${not empty id}" >
	<getters:getImageById attributeName="image" id="${id}" />
	<image:imageSrc attributeName="imageSrc" image="${image}" />
	<image:imageSrc attributeName="largeImageSrc" image="${image}" isLargeImage="${true}"/>
</c:if>
<table border="0" cellpadding="0" cellspacing="0" class="formtable">
<tr><td class="leftcol">			
	<table class="formtable" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="label">
				<p>Title<span class="req">*</span></p>
			</td>
			<td class="field">
			    <input type="hidden" name="author" id="author" value="${pageContext.request.remoteUser}" />
     			<input type="hidden" name="status" value="${status}" />
     			<input type="hidden" name="forwardUrl" value="${pageContext.request.contextPath}/images.jsp" />			      				
     			<input type="hidden" name="imageid" value="${image.id}" />
				<input type="text" name="title" id="title" size="35" value="${image.title}" class="required validate-alphanumber textfield title"><br />
				<p class="req"></p>
			</td>
		</tr>
		<tr>
			<td class="label">
				<p>Keywords</p>
			</td>
			<td class="field">
				<textarea name="keywords" cols="42" rows="3" class="subhead" >${image.keywords}</textarea>
				<p class="descr">
					Comma separated. Enter any additional words to help find this image in a search.
				</p>
		    </td>
		</tr>
		<tr>
			<td class="label">
				<p>Agency Caption</p>
			</td>
			<td class="field">
				<textarea name="agencyCaption" id="agencyCaption" cols="42" rows="3" class="subhead" >${image.agencyCaption}</textarea>
				<p class="descr">
					Enter agency caption here.
				</p>
		    </td>
		</tr>
		<tr>
			<td class="label">
				<p>Photo Editor's Notes</p>
			</td>
			<td class="field">
				<textarea name="notes" id="notes" cols="42" rows="3" class="subhead" >${image.notes}</textarea>
				<p class="descr">
					Enter photo editor's notes here.
				</p>
		    </td>
		</tr>
	    <c:if test="${not empty imageSrc}" >
			<c:if test="${image.sourceWidth > 400}">
				<c:set var="imageWidth" scope="page" value="400"/>
			</c:if>				    
			<tr class="newsection">
				<td class="label"><p>Regular Image</p></td>
				<td class="field">
					<img src="${imageSrc}" <c:if test="${not empty imageWidth}">width="${imageWidth}"</c:if> ><br/>
					<p class="descr">${image.source}</p>
				</td>
			</tr>
		</c:if>
		<c:if test="${not empty largeImageSrc}" >
			<c:if test="${image.largeSourceWidth > 400}">
				<c:set var="imageWidth" scope="page" value="400"/>
			</c:if>
			<tr class="newsection">
				<td class="label"><p>Large Image</p></td>
				<td class="field">
					<img src="${largeImageSrc}" <c:if test="${not empty imageWidth}">width="${imageWidth}"</c:if> ><br/>
					<p class="descr">${image.largeSource}</p>
				</td>
			</tr>
		</c:if>
		<tr class="newsection">
			<td class="label">
				<p>Upload File</p>
			</td>
			<td class="field">
				<table>
					<tr>
						<td class="label">
							<p>Regular:</p>											
						</td>
						<td class="field">
							<input type="file" name="uploadFile" id="uploadFile" size="30" value="" class="choose" />
							<p class="descr">
							 	Select image from local drive.
						    </p>
							<p class="req"></p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="newsection">
			<td class="label"><p>Photographer:</p></td>
			<td class="field">										
	   			<input type="text" name="photographer" size="40" value="${image.photographer}" class="textfield">
			</td>
		</tr>
		<tr class="newsection">
			<td class="label"><p>Agency & License</p></td>
		 	<td class="field">
		 		<c:set var="chosenAgency" value="${image.agencyLicense.agencyLU}" />
		 		<div name="agencyLicenseDiv" id="agencyLicenseDiv">
				<select name="agencyid" id="agencyid" onchange="LimnAgency.UpdateLicense()">
					<option value="">None(Manual)</option>				
					<c:forEach items="${agencies}" var="agency">
						<option value="${agency.id}" <c:if test="${chosenAgency.id eq agency.id}" >
							selected</c:if> >${agency.title}</option>
					</c:forEach>
				</select>
				
				<select name="licenseid" id="licenseid" onchange="LimnAgency.UpdateLicenseValues()">
					<option value="">None(Manual)</option>
					<c:forEach items="${chosenAgency.agencyLicense}" var="agencyLicense">
						<option value="${agencyLicense.id}" <c:if test="${agencyLicense.id eq image.agencyLicenseId}">selected</c:if> >
							${agencyLicense.title}</option>
					</c:forEach>
				</select>	
				<c:forEach items="${chosenAgency.agencyLicense}" var="agencyLicense">
					<agency:setDate attributeName="chosenExpireDate" date="${agencyLicense.expireDate}" 
						duration="${agencyLicense.expireDuration}" dateType="${agencyLicense.expireDateType}"/>
					<agency:setDate attributeName="chosenAutoLockDate" date="${agencyLicense.autoLockDate}" 
						duration="${agencyLicense.autoLockDuration}" dateType="${agencyLicense.autoLockDateType}"/>
					<input type="hidden" id="siteUsage${agencyLicense.id}" name="siteUsage${agencyLicense.id}"
						value="${agencyLicense.sectionUseStr}" />
					<input type="hidden" id="chosenExpireDate${agencyLicense.id}" name="chosenExpireDate${agencyLicense.id}"
						value="<fmt:formatDate value='${chosenExpireDate}' type='both' pattern='MM/dd/yyyy' />"/>
					<input type="hidden" id="chosenAutoLockDate${agencyLicense.id}" name="chosenAutoLockDate${agencyLicense.id}"
						value="<fmt:formatDate value='${chosenAutoLockDate}' type='both' pattern='MM/dd/yyyy' />"/>
				</c:forEach>
				</div>
				<div class="related">
					<h4>Manual Name</h4>
                    <input type="text" name="agency" size="40" value="${image.agency}" class="textfield">
                </div>
			</td>
		</tr>
		<tr class="newsection">
			<td class="label"><p>Expiration<span class="req">*</span></p></td>
		    <td class="field">
		  		<input type="radio" name="allowNullExpiration"  value="no" 
					<c:if test="${image.expirationDate ne null}"> checked="checked" </c:if> />
					Expires On
				<div class="datepicker">				
					<div dojoType="dropdowndatepicker" inputName="expirationDate" inputId="expirationDate" 
				   		<c:if test="${image.expirationDate ne null}">	
			     			date="<fmt:formatDate value='${image.expirationDate}' type='both' pattern='MM/dd/yyyy' />"
			      		</c:if> >
			 		</div>
			 		<p class="req"></p>
					<p class="descr">mm/dd/yyyy</p>
				</div>
				<input type="radio" name="allowNullExpiration"  value="yes" 
		   		<c:if test="${image.expirationDate eq null}"> checked="checked" </c:if> />
				Image does not expire
			</td>
	    </tr>
	    <tr id="autoLock" class="newsection">
			<td class="label"><p>Auto-lock<span class="req">*</span></p></td>
		    <td class="field">
		  		<input type="radio" name="allowNullAutoLock"    value="yes" 
					<c:if test="${image.autoLockDate ne null}"> checked="checked" </c:if> />
					Locks On
				<div class="datepicker">
					<div dojoType="dropdowndatepicker" inputName="autoLockDate" inputId="autoLockDate" 
				   		<c:if test="${image.autoLockDate ne null}">	
			     			date="<fmt:formatDate value='${image.autoLockDate}' type='both' pattern='MM/dd/yyyy' />"
			      		</c:if> >
			 		</div>
					<p class="descr">mm/dd/yyyy</p>
				</div>
				<div>
					<input type="radio" name="allowNullAutoLock" value="usageLock"  
						<c:if test="${image.usageLimit ne null && image.usageLimit > 0}"> checked="checked" </c:if> />
					After 
					<select name="usageLimit" id="usageLimit">
						<option value="1" <c:if test="${image.usageLimit eq 1}">selected</c:if>>1</option>
						<option value="2" <c:if test="${image.usageLimit eq 2}">selected</c:if>>2</option>
						<option value="3" <c:if test="${image.usageLimit eq 3}">selected</c:if>>3</option>
						<option value="4" <c:if test="${image.usageLimit eq 4}">selected</c:if>>4</option>
						<option value="5" <c:if test="${image.usageLimit eq 5}">selected</c:if>>5</option>
						<option value="6" <c:if test="${image.usageLimit eq 6}">selected</c:if>>6</option>
						<option value="7" <c:if test="${image.usageLimit eq 7}">selected</c:if>>7</option>
						<option value="8" <c:if test="${image.usageLimit eq 8}">selected</c:if>>8</option>
						<option value="9" <c:if test="${image.usageLimit eq 9}">selected</c:if>>9</option>
						<option value="10" <c:if test="${image.usageLimit eq 10}">selected</c:if>>10</option>
					</select>
					total uses in
					<select name="usageEdition" id="usageEdition">
						<option value="0"  <c:if test="${image.usageEdition eq 0}">selected</c:if>>All Editions</option>
						<c:forEach items="${editions}" var="ed" >
							<option value="${ed.id}" <c:if test="${image.usageEdition eq ed.id}">selected</c:if>>${ed.descr}</option>
						</c:forEach>
					</select>
					<select name="usageSite" id="usageSite">
						<option value=""  <c:if test="${image.usageSiteId eq 0}">selected</c:if>>All Sites</option>
						<c:forEach items="${sites}" var="site" >
							<option value="${site[0]}" <c:if test="${image.usageSiteId eq site[0]}">selected</c:if>>${site[1]}</option>
						</c:forEach>
					</select>
				</div>
				<br/>
				<input type="radio" name="allowNullAutoLock" value="no"  
		   		<c:if test="${image.autoLockDate eq null && (image.usageLimit eq null || image.usageLimit < 1)}"> checked="checked" </c:if> />
				Image does not lock
			</td>
	    </tr>
	    <tr class="newsection">
	    	<td colspan="2">
	    		<image:imageUsage image="${image}" editions="${editions}" sites="${sites}" />
	    	</td>
	    </tr>
	    <tr class="newsection">
	    	<td colspan="2">
	    		<image:imageRelated image="${image}" editions="${editions}" sites="${sites}" />
	    	</td>
	    </tr>
	    </table>
		</td>
		<td class="rightcol">
			<image:imageRightCol image="${image}" />
		</td>
	</tr>
</table>	