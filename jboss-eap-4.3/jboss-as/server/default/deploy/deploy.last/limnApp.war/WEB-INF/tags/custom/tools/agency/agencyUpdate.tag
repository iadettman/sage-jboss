<%@ tag import="java.util.List" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="agency" tagdir="/WEB-INF/tags/custom/tools/agency"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ attribute name="id" type="java.lang.String" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<%@ attribute name="isCreateNewLicense" type="java.lang.Boolean" required="false"%>
<c:if test="${not empty id}" >
	<getters:getAgencyById attributeName="agencyItem" id="${id}" />
	<getters:getDefaultLicense attributeName="defaultLicense" agency="${agencyItem}" />
</c:if>
<table class="formtable" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="label">
			<p>Name<span class="req">*</span></p>
		</td>
		<td class="field">
    		<input type="hidden" name="agencyid" value="${agencyItem.id}" />	      				
			<input type="text" name="title" id="title" size="35" value="${agencyItem.title}" class="required validate-alphanumber textfield title"><br />
			<p class="req"></p>
		</td>
	</tr>
	<tr>
		<td class="label"><p>Licenses</p></td>
	   	<td class="field">
	   		<input type="hidden" name="licenseSize" id="licenseSize" value="${fn:length(agencyItem.agencyLicense)}" />
	   		<input type="hidden" name="defaultLicenseId" id="defaultLicenseId" value="${defaultLicense.id}" />
	   		<input class="choose" type="button" onclick="LimnAgency.AddLicenseNew(${agencyItem.id})" value="Create New License" name="x"/>
	   		<c:if test="${isCreateNewLicense eq true}">
	   		<div id="licenseDivSeg0" class="photoblock active">
	   			<img border="0" align="right" onclick="if(confirmAction('deleteLicense')){$('licenseDivSeg0').remove()}"
					src="/limn/includes/images/icon_delete.gif"/>

                <div class="form-field-group">
                    <h3>Name</h3>
                    <input type="text" name="licensetitle0" id="licensetitle0" size="25"
                            value="${license.title}" class="required validate-alphanumber textfield title">
                    <span id="defaultLicense0">
                        <a href="javascript:;" onclick="LimnAgency.SetDefaultLicense(0)">Set as default license</a>
                    </span>
                    <div class="clear"></div>
                </div>

                <div class="form-field-group">
                    <h3 class="form-field-group-header">Expiration</h3>
                    <div class="form-field-sub-group">
                        <input type="radio" class="radio" name="allowNullExpiration0" value="never" checked="checked" />
                        <span>Never</span>
                    </div>
                    <div class="form-field-sub-group">
                        <input type="radio" class="radio" name="allowNullExpiration0" value="duration" />
                        <%--<span>Expires in</span>--%>
                        <input type="textbox" name="expireDuration0" id="expireDuration0" size="5"/>
                        <select name="expireDateType0" id="expireDateType0">
                                <option value="days">Days</option>
                                <option value="months">Months</option>
                                <option value="years">Years</option>
                        </select>
                    </div>
                    <div class="form-field-sub-group">
                        <input type="radio" class="radio" name="allowNullExpiration0" value="date" />
                        <%--<span>Expires on</span>--%>
                        <div class="datepicker-block">
                            <div dojoType="dropdowndatepicker" inputName="expiredate0" inputId="expiredate0" date="">
                            </div>
                            <span class="descr">mm/dd/yyyy</span>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="form-field-group">
                    <h3 class="form-field-group-header">Auto-Lock</h3>
                    <div class="form-field-sub-group">
                        <input type="radio" class="radio" name="allowNullAutolock0" value="never" checked="checked" />
                        <span>Never</span>
                    </div>
                    <div class="form-field-sub-group">
                        <input type="radio" class="radio" name="allowNullAutolock0" value="duration" />
                        <%--<span>Locks in</span>--%>
                        <input type="textbox" name="autoLockDuration0" id="autoLockDuration0" size="5"/>
                        <select name="autoLockDateType0" id="autoLockDateType0">
                                <option value="days">Days</option>
                                <option value="months">Months</option>
                                <option value="years">Years</option>
                        </select>
                    </div>
                    <div class="form-field-sub-group">
                        <input type="radio" class="radio" name="allowNullAutolock0" value="date" />
                        <%--<span>Locks on</span>--%>
                        <div class="datepicker-block">
                            <div dojoType="dropdowndatepicker" inputName="autoLockDate0" inputId="autoLockDate0" date="">
                            </div>
                            <span class="descr">mm/dd/yyyy</span>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
			</c:if>
			<input type="hidden" name="defaultLicense" id="defaultLicense" value="0" />

   			<c:forEach var="license" items="${agencyItem.agencyLicense}" varStatus="status">
   				<input type="hidden" name="licenseid${status.count}" id="licenseid${status.count}" value="${license.id}"/>




                <div id="licenseDivSeg${status.count}" class="photoblock active">
                    <img border="0" align="right" onclick="if(confirmAction('deleteLicense')){$('licenseDivSeg${status.count}').remove()}"
                        src="/limn/includes/images/icon_delete.gif"/>

                    <div class="form-field-group">
                        <h3>Name</h3>
                        <input type="text" name="licensetitle${status.count}" id="licensetitle${status.count}" size="25"
                                value="${license.title}" class="required validate-alphanumber textfield title">
                        <span id="defaultLicense${status.count}">
                            <c:choose>
                                <c:when test="${license.defaultLicense}">
                                    <span class="license-status">Default License</span>
                                </c:when>
                                <c:otherwise>
                                    <a href="javascript:;" onclick="LimnAgency.SetDefaultLicense(${status.count})">Set as default license</a>
                                </c:otherwise>
                            </c:choose>
                        </span>
                        <div class="clear"></div>
                    </div>

                    <div class="form-field-group">
                        <h3 class="form-field-group-header">Expiration</h3>
                        <div class="form-field-sub-group">
                            <input type="radio" class="radio" name="allowNullExpiration${status.count}" value="never"
   									<c:if test="${empty license.expireDate and empty license.expireDuration}">checked="checked"</c:if>/>
                            <span>Never</span>
                        </div>
                        <div class="form-field-sub-group">
                            <input type="radio" class="radio" name="allowNullExpiration${status.count}" value="duration" <c:if test="${not empty license.expireDuration}">
   									checked="checked"</c:if>/>
                            <%--<span>Expires in</span>--%>
                            <input type="textbox" name="expireDuration${status.count}" id="expireDuration${status.count}" value="${license.expireDuration}" size="5"/>
                            <select name="expireDateType${status.count}" id="expireDateType${status.count}">
                                <option value="days" <c:if test="${license.expireDateType eq 'days'}">selected</c:if>>Days</option>
                                <option value="months" <c:if test="${license.expireDateType eq 'months'}">selected</c:if>>Months</option>
                                <option value="years" <c:if test="${license.expireDateType eq 'years'}">selected</c:if>>Years</option>
                            </select>
                        </div>
                        <div class="form-field-sub-group">
                            <input type="radio" class="radio" name="allowNullExpiration${status.count}" value="date" <c:if test="${not empty license.expireDate}">
									checked="checked"</c:if>/>
                            <%--<span>Expires on</span>--%>
                            <div class="datepicker-block">
                                <div dojoType="dropdowndatepicker" inputName="expiredate${status.count}" inputId="expiredate${status.count}"
	     							date="<fmt:formatDate value='${license.expireDate}' type='both' pattern='MM/dd/yyyy' />" >
	 							</div>
                                <span class="descr">mm/dd/yyyy</span>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="form-field-group">
                        <h3 class="form-field-group-header">Auto-Lock</h3>
                        <div class="form-field-sub-group">
                            <input type="radio" class="radio" name="allowNullAutolock${status.count}" value="never"
   									<c:if test="${empty license.autoLockDate && empty license.autoLockDuration}">checked="checked"</c:if>/>
                            <span>Never</span>
                        </div>
                        <div class="form-field-sub-group">
                            <input type="radio" class="radio" name="allowNullAutolock${status.count}" value="duration" <c:if test="${not empty license.autoLockDuration}">
   									checked="checked"</c:if>/>
                            <%--<span>Locks in</span>--%>
                            <input type="textbox" name="autoLockDuration${status.count}" id="autoLockDuration${status.count}" value="${license.autoLockDuration}" size="5"/>
                            <select name="autoLockDateType${status.count}" id="autoLockDateType${status.count}">
                                <option value="days" <c:if test="${license.autoLockDateType eq 'days'}">selected</c:if>>Days</option>
                                <option value="months" <c:if test="${license.autoLockDateType eq 'months'}">selected</c:if>>Months</option>
                                <option value="years" <c:if test="${license.autoLockDateType eq 'years'}">selected</c:if>>Years</option>
                            </select>
                        </div>
                        <div class="form-field-sub-group">
                            <input type="radio" class="radio" name="allowNullAutolock${status.count}" value="date" <c:if test="${not empty license.autoLockDate}">
									checked="checked"</c:if>/>
                            <%--<span>Locks on</span>--%>
                            <div class="datepicker-block">
                                <div dojoType="dropdowndatepicker" inputName="autoLockDate${status.count}" inputId="autoLockDate${status.count}"
	     							date="<fmt:formatDate value='${license.autoLockDate}' type='both' pattern='MM/dd/yyyy' />" >
	 							</div>
                                <span class="descr">mm/dd/yyyy</span>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
   			</c:forEach>
		</td>
    </tr>
</table>