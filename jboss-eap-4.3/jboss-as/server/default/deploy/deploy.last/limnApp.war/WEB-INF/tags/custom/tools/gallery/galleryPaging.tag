<%@ tag import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="size" type="java.lang.Integer" required="true"%>
<%@ attribute name="total" type="java.lang.Integer" required="false"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<%@ attribute name="category" type="java.lang.String" required="false"%>
<%@ attribute name="galleryIndex" type="java.lang.Integer" required="false"%>
<c:if test="${galleryIndex eq null}" >
	<c:set var="galleryIndex" value="0"/>
</c:if>
<c:if test="${total eq null}" >
	<c:set var="total" value="50"/>
</c:if>
<div class="paging">
	<c:choose>
		<c:when test="${galleryIndex eq 0}">
			<a href="javascript:;" class="disabledlink" >First</a>&nbsp;|&nbsp;
			<a href="javascript:;" class="disabledlink" >< Prev</a>
		</c:when>
		<c:otherwise>
			<a href="javascript:;" class="next" onClick="LimnGallery.FilterByStartIndex('0', '${status}', '${sortBy}', '${category}')">First</a>&nbsp;|&nbsp;
			<a href="javascript:;" class="next" onClick="LimnGallery.FilterByStartIndex('${galleryIndex - total}', '${status}', '${sortBy}', '${category}')">< Prev</a>
		</c:otherwise>
	</c:choose>		
    |
   	<c:choose>
		<c:when test="${size < total}">
			${galleryIndex}&nbsp;of&nbsp;${size}
		</c:when>
		<c:otherwise>	
			<select name="selectImageIndex" id="selectImageIndex" onChange="LimnGallery.FilterByStartIndex(this.options[selectedIndex].value, 
				'${status}', '${sortBy}', '${category}')" >
				<c:forEach begin="0" end="${(size-1) / total}" varStatus="index" >
					<option value="${(total*index.count) - total}" <c:if test="${(total*index.count) - total eq galleryIndex}">SELECTED</c:if>>
						${(total*index.count) - total + 1}&nbsp;-&nbsp;
						<c:choose>
							<c:when test="${galleryIndex + (total*index.count) > size}">
								${size}
							</c:when>
							<c:otherwise>
								${(total*index.count)}
							</c:otherwise>
						</c:choose>		
					</option>
				</c:forEach>				
			</select>
			&nbsp;of&nbsp;${size}
            |
			<c:choose>
				<c:when test="${galleryIndex + total >=  size}">
					<a href="javascript:;" class="disabledlink">Next ></a>&nbsp;|&nbsp;
					<a href="javascript:;" class="disabledlink">Last</a>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${(size % total) eq 0}">
							<c:set var="lastIndex" value="${size - total}" scope="page" />
						</c:when>
						<c:otherwise>
							<c:set var="lastIndex" value="${size - (size % total)}" scope="page" />
						</c:otherwise>
					</c:choose>	
					<a href="javascript:;" class="next" onClick="LimnGallery.FilterByStartIndex('${galleryIndex + total}', '${status}', '${sortBy}', '${category}')">Next ></a>&nbsp;|&nbsp;
					<a href="javascript:;" class="next" onClick="LimnGallery.FilterByStartIndex('${lastIndex}', '${status}', '${sortBy}', '${category}')">Last</a>
				</c:otherwise>
			</c:choose>		
		</c:otherwise>	
	</c:choose>
</div>