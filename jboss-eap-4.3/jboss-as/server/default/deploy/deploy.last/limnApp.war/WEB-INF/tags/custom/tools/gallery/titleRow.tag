<%@ attribute name="title" type="java.lang.String" required="true"%>
<tr>
	<td class="label">
		<p>Title<span class="req">*</span></p>
	</td>
	<td class="field">
		<input type="text" class="required validate-alphanumber textfield title" name="title" id="requiredTitle" size="35" value="${title}" class="textfield title"><br />
		<p class="descr">
			<span id="titleCount">20</span>
			characters,
			<span class="maxLimit">100</span>
			characters maximum
		</p>
	</td>
</tr>