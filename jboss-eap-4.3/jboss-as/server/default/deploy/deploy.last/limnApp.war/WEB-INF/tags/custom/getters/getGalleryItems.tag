<%@ tag import="com.ceg.online.limn.helpers.GalleryHelper" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="id" type="java.lang.Integer" required="true" %>
<%@ attribute name="start" type="java.lang.Integer" required="true" %>
<%@ attribute name="end" type="java.lang.Integer" required="true" %>
<%
if (id != null && id.intValue() >= 0 && start != null && end != null ) {
	request.setAttribute(attributeName, GalleryHelper.getGalleryItems(id, start, end));
}
%>