<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="req"   uri="http://jakarta.apache.org/taglibs/request-1.0" %>
<%@ attribute name="image" type="com.ceg.online.limn.beans.ImageBean" required="false" %>
<tools:escapeQuotes attributeName="imageTitle" originalStr="${image.title}" />
<c:if test="${image.id != null}">
<div class="helpinfo">
	<ul>
		<li><h4>Last Modified</h4>
			<c:choose>
				<c:when test="${!empty image.lastModDate}">
		    		<fmt:formatDate value="${image.lastModDate}" 
		    			type="both" pattern="M/d/yy, h:mma" />
		    		<c:if test="${not empty image.author}">by ${image.author}</c:if>
				</c:when>
				<c:otherwise>
			 		N/A
	    		</c:otherwise>
			</c:choose>	
		</li>
		<li><h4>Expires</h4>
			<c:choose>
				<c:when test="${!empty image.expirationDate}">
		    		<fmt:formatDate value="${image.expirationDate}" 
		    			type="both" pattern="M/d/yy, h:mma" />
				</c:when>
				<c:otherwise>
			 		Does not expire
	    		</c:otherwise>
			</c:choose>	
		</li>
		<li><h4>Auto Locks On</h4>
			<c:choose>
				<c:when test="${!empty image.autoLockDate}">
		    		<fmt:formatDate value="${image.autoLockDate}" 
		    			type="both" pattern="M/d/yy, h:mma" />
				</c:when>
				<c:otherwise>
			 		N/A
	    		</c:otherwise>
			</c:choose>	
		</li>
		<li>
			<c:if test="${(image.status eq 'expired')}">
   	  	  	    <span class="brick expired">expired</span>
			</c:if>
			<c:if test="${image.locked}">
     			<span class="brick locked">
     				locked
     			</span>
     		</c:if>
		</li>
	</ul>
</div>
</c:if>
<c:if test="${image.id != null && image.status ne 'expired'}" >
	<div class="helpinfo">
		<ul>
			<li>
				<c:choose>
					<c:when test="${image.locked}">
						<c:set var="checkLock" value="checked" />
					</c:when>
					<c:otherwise>
						<c:set var="checkLock" value="" />
					</c:otherwise>
				</c:choose>
				<input id="isLocked" type="checkbox" ${checkLock} name="isLocked" value="locked" 
					<req:isUserInRole role="lock image" value="false">disabled</req:isUserInRole> />
				<img src="${pageContext.request.contextPath}/includes/images/lock.gif" />Lock Image
				<req:isUserInRole role="lock image" value="false">
					<p class="descr">
					Image can only be unlocked by a Photo Administrator
					</p>
				</req:isUserInRole>
			</li>
		</ul>
	</div>
</c:if>
<c:if test="${image.id != null}">
<ul class="editpagefns">
	<li>  <!--  Publishin/Expire -->
     	<div class="listcontrols">
			<a class="newbtn smallerbtn action-form-submit">Update</a>		
      	  	<a class="newbtn smallerbtn" title="Expire Now" href="javascript:;" onClick="LimnImage.UpdateImage('${image.id}', 'expired', '${imageTitle}');" >Expire Now</a>      	 
      	  	<a class="newbtn smallerbtn" title="Delete" href="javascript:;" onClick="LimnImage.UpdateImage('${image.id}', 'deleted', '${imageTitle}');">Delete</a>    
      	</div>
		<div class="listcontrols">
      	  	<a class="newbtn smallerbtn" title="Copy Image Details" href="uploadImages.jsp?imageId=${image.id}">Copy Image Details</a>    
     	</div>
	</li>
</ul>
</c:if>