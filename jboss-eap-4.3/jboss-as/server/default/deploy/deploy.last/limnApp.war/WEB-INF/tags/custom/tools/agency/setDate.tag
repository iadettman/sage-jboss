<%@ tag import="java.util.Date" %>
<%@ tag import="java.util.Calendar" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="date" type="java.util.Date" required="true"%>
<%@ attribute name="duration" type="java.lang.Integer" required="true"%>
<%@ attribute name="dateType" type="java.lang.String" required="true"%>
<%
	Date now = new Date();
	Calendar cal;
	
	if (duration != null && duration > 0) {
		cal = Calendar.getInstance();
		if (dateType.equals("days")) {
			cal.add(Calendar.DAY_OF_YEAR, duration);
		}
		if (dateType.equals("months")) {
			cal.add(Calendar.MONTH, duration);
		}
		if (dateType.equals("years")) {
			cal.add(Calendar.YEAR, duration);
		}
		date = cal.getTime();
	}
	request.setAttribute(attributeName, date);
%>