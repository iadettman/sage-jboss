<%@ tag import="java.util.Date"%>
<%@ tag import="java.lang.Boolean"%>
<%@ tag import="java.util.Calendar"%>
<%@ tag import="java.text.DateFormat"%>
<%@ tag import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="date" type="java.util.Date" required="true"%>
<%
	if(date != null) {
		DateFormat am = new SimpleDateFormat("a");
		request.setAttribute("am", new Boolean(am.format(date).equals("AM")));
		DateFormat time = new SimpleDateFormat("h:mm");
		request.setAttribute("timeOfDay", time.format(date));
		DateFormat monthDayYear = new SimpleDateFormat("MM/dd/yyyy");
		request.setAttribute("standardDate", monthDayYear.format(date));
	} else {
		request.setAttribute("am", new Boolean("true"));
	}
%>
<tr>
	<td class="label"><p>Published On</p></td>
    <td class="field">
        <div id="publishDateContainer">
            <!--<div dojoType="dropdowndatepicker" inputName="publishDateStr" inputId="publishDateStr" date="${standardDate}" class="datepicker"></div>-->
            <input type="text" name="publishDateStr" id="publishDateStr" class="datepicker" value="${standardDate}" />
        </div>
        <div id="publishTimeContainer">
            Time: <input type="text" name="publishTimeStr" id="publishTimeStr" value="${timeOfDay}" class="textfield timepicker"/>
            <select id="selectedPublishAMPM" name="selectedPublishAMPM">
				<option value="A.M." <c:if test="${am}">selected="selected"</c:if>>A.M.</option>
				<option value="P.M." <c:if test="${not am}">selected="selected"</c:if>>P.M.</option>
			</select>
        </div>
        <div class="clear"></div>
    </td>
</tr>