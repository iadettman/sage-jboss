<%@ tag import="java.util.List" %>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ attribute name="images" type="java.util.List" required="true"%>
<%@ attribute name="status" type="java.lang.String" required="true"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<!--  Listing -->
<c:choose>
   <c:when test="${(sortBy eq 'title desc')}">
		<c:set var="titleSort" scope="page" value="title asc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'title asc')}">
		<c:set var="titleSort" scope="page" value="title desc"/>
		<c:set var="titleClass" scope="page" value="sorted"/>		
   </c:when>
   <c:otherwise>
		<c:set var="titleSort" scope="page" value="title desc"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${(sortBy eq 'lastmoddate desc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate asc"/>
		<c:set var="lastModClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'lastmoddate asc')}">
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>		
		<c:set var="lastModClass" scope="page" value="sorted"/>				
   </c:when>
   <c:otherwise>
		<c:set var="lastModSort" scope="page" value="lastmoddate desc"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${(sortBy eq 'publishdate desc')}">
		<c:set var="publishSort" scope="page" value="publishdate asc"/>
		<c:set var="publishClass" scope="page" value="sorted"/>		
   </c:when>
   <c:when test="${(sortBy eq 'publishdate asc')}">
		<c:set var="publishSort" scope="page" value="publishdate desc"/>
		<c:set var="publishClass" scope="page" value="sorted"/>				
   </c:when>
   <c:otherwise>
		<c:set var="publishSort" scope="page" value="publishdate desc"/>
   </c:otherwise>
</c:choose>
<c:choose>
   <c:when test="${(sortBy eq 'expiredate desc')}">
		<c:set var="expireSort" scope="page" value="expiredate asc"/>
		<c:set var="expireClass" scope="page" value="sort"/>				
   </c:when>
   <c:when test="${(sortBy eq 'expiredate asc')}">
		<c:set var="expireSort" scope="page" value="expiredate desc"/>
		<c:set var="expireClass" scope="page" value="sort"/>				
   </c:when>
   <c:otherwise>
		<c:set var="expireSort" scope="page" value="expiredate desc"/>
   </c:otherwise>
</c:choose>
<form id="image_checkbox_form" action="" method="post" >
<table id="episode_list" border="0" cellpadding="0" cellspacing="0" width="100%">
 	<tbody>
	<tr>
  		<th>&nbsp;</th>
  		<th>&nbsp;</th>
  		<th class="${titleClass}"><a href="javascript:;" onClick="LimnImage.FilterBySort('${titleSort}', '${status}');" >Title</a></th>
  		<th class="${lastModClass}">
     		<img src="${pageContext.request.contextPath}/includes/images/lock.gif" />
     	</th>
  		<th class="${lastModClass}" style="text-align:center;"># Used</th>
   		<th class="${lastModClass}">Last Used</th> 		
  		<th class="${publishClass}"><a href="javascript:;" onClick="LimnImage.FilterBySort('${publishSort}', '${status}');" >Last Modified</a></th>
  		<th class="${expireClass}" style="text-align:center;"><a href="javascript:;" onClick="LimnImage.FilterBySort('${expireSort}', '${status}');" >Expires</a></th>
  		<th>&nbsp;</th>
	</tr>
   	<c:forEach var="image" items="${images}" varStatus="status">
   	<getters:getImageUsageImageId attributeName="imageUsage" id="${image.id}" />
   	<tools:escapeQuotes attributeName="imageTitle" originalStr="${image.title}" />
  	<c:set var="rowClass"  scope="page" value="row_one"/>
  	<image:imageSrc attributeName="imageSrc" image="${image}" />
  	<c:if test="${(status.count % 2) == 0}"> 
		<c:set var="rowClass" scope="page" value="row_two"/>
	</c:if>
	<c:if test="${(image.status eq 'draft')}">
         <c:set var="rowClass" scope="page" value="${rowClass} draft"/>
	</c:if>
    <c:set var="imageWidth"  scope="page" value="${image.sourceWidth}"/>
	<c:set var="imageHeight" scope="page" value="${image.sourceHeight}"/>
    <c:set var="imageClass"  scope="page" value="vertical"/>
	<c:if test="${(image.sourceWidth >= image.sourceHeight)}">
		<c:set var="imageClass" scope="page" value="horizontal"/>
	</c:if>
    <tr class="${rowClass}">
         <td class="checkselect">
         	<input type="checkbox" id="imagesSelected" name="imagesSelected" value="${image.id}" />
		</td>
        <td class="imagethumb">
     		<a id="imageSource${status.count}" href="${imageSrc}"  rel="lightbox[rel]" title="${imageTitle}<br/>${image.sourceWidth}w X ${image.sourceHeight}h<br/> ${image.photographer}&nbsp;${image.agency}">
  				<img src="${imageSrc}" class="${imageClass}" />
			</a>
		</td>
		<td class="title">
			<a id="imageName${status.count}" href="editImage.jsp?imageId=${image.id}" class="articlelink">${fn:substring(image.title,0,80)}</a>
 	  		<p class="descr">${image.sourceWidth} x ${image.sourceHeight} px</p>
 		</td>
 		<td class="title">
 			<c:if test="${image.locked}">
     			<img src="${pageContext.request.contextPath}/includes/images/lock.gif" />
     		</c:if>
 		</td>
 		<c:choose>
 			<c:when test="${empty image.expirationDate}">
 				<c:set var="dateClass" scope="page" value="recentdate"/>
 			</c:when>
 			<c:otherwise>
				<c:set var="dateClass" scope="page" value="pastdate"/>
     		</c:otherwise>
 		</c:choose>
		<td ALIGN="center">
 			<c:if test="${not empty imageUsage}">
 				${fn:length(imageUsage)}
 			</c:if>
        </td>
		<td>
        	<c:choose>
 				<c:when test="${empty imageUsage}">
 					<p class="descr">n/a</p>
 				</c:when>
 				<c:otherwise>
					<fmt:formatDate value="${imageUsage[0].publishDate}" type="both" pattern="M/d/yy, h:mma" />
						<c:choose>
							<c:when test="${imageUsage[0].galleryType}">
								<p class="descr">Gallery</p>
							</c:when>
							<c:when test="${imageUsage[0].blogType}">
								<p class="descr">Blog</p>
							</c:when>
						</c:choose>
     			</c:otherwise>
 			</c:choose>	         
         </td>
         <td>
         	<c:if test="${not empty image.lastModDate}">
           		<fmt:formatDate value="${image.lastModDate}" type="both" pattern="M/d/yy, h:mma" />
           		<c:if test="${not empty image.author}">
           			<p class="descr">last updated by ${image.author}</p>
           		</c:if>
           	</c:if>
         </td>
         <td ALIGN="center" class="recentdate"<c:if test="${image.expired}"> style="background-color:red;"</c:if>>
           	<c:choose>
    			<c:when test="${empty image.expirationDate}">
	  				never    
     			</c:when>    			        
     			<c:otherwise>
					<fmt:formatDate value="${image.expirationDate}" type="both" pattern="M/dd/yy" />
     			</c:otherwise>
  			</c:choose>
         </td>    
         <td ALIGN="center">
         	<a href="uploadImages.jsp?imageId=${image.id}">
				<img src="${pageContext.request.contextPath}/includes/images/copyMetadata.png" style="border-style: none" />
			</a>
         </td>                                      
    </tr>
    </c:forEach>
    <!--  End of Listing -->
   </tbody>
</table>
</form>