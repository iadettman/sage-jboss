<%@ tag import="java.util.List" %>
<%@ tag import="com.ceg.online.limn.helpers.SiteHelper" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true"%>
<%@ attribute name="image" type="com.ceg.online.limn.beans.ImageBean" required="true" %>
<%@ attribute name="isLargeImage" type="java.lang.Boolean" required="false"%>
<%
	final String START_PREFIX = "http://";
	String link;
	
	if (isLargeImage != null && isLargeImage.booleanValue()) {
		link = image.getLargeSource() != null && !image.getLargeSource().trim().equals("")
			? image.getFilePath() + "/" + image.getLargeSource() : null;
	}
	else {
		link = image.getSource() != null && !image.getSource().trim().equals("")
			? image.getFilePath() + "/" + image.getSource() : null;
	}	
	
	if (link != null && !link.trim().startsWith(START_PREFIX)) {
		link = SiteHelper.getImageHost() + link.trim();
	}
	request.setAttribute(attributeName,link);
%>