<%@ tag import="com.ceg.online.limn.helpers.ImageHelper" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="id" type="java.lang.Integer" required="true" %>
<%
if (id != null && id.intValue() >= 0) {
	request.setAttribute(attributeName, ImageHelper.getImageById(id));
}
%>