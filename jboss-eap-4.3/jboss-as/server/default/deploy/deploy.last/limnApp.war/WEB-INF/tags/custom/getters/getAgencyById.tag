<%@ tag import="com.ceg.online.limn.helpers.AgencyHelper" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="id" type="java.lang.Integer" required="true" %>
<%
if (id != null && id.intValue() >= 0) {
	request.setAttribute(attributeName, AgencyHelper.getAgencyById(id));
}
%>