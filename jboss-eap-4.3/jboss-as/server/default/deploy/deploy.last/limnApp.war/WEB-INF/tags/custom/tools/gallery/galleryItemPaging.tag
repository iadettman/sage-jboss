<%@ tag import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="id" type="java.lang.Integer" required="true"%>
<%@ attribute name="size" type="java.lang.Integer" required="true"%>
<%@ attribute name="galleryItemIndex" type="java.lang.Integer" required="false"%>
<%@ attribute name="total" type="java.lang.Integer" required="false"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<%@ attribute name="category" type="java.lang.String" required="false"%>
<%@ attribute name="position" type="java.lang.String" required="false"%>
<c:if test="${galleryItemIndex eq null}" >
	<c:set var="galleryItemIndex" value="0"/>
</c:if>
<c:if test="${total eq null}" >
	<c:set var="total" value="15"/>
</c:if>

<table width="100%">
<tbody>
	<tr>
		<td>
			<div class="paging">
				<a class="addtextblock" onclick="if (confirmPhotoBlocks()) {LimnGallery.PopUp('?callingForm=galleryForm&sectionUse=Galleries&sectionUse=Entire Site&property=thumbnailContentID&displayPreview=display_thumbnail_&numImages=4&selectedImagesStr=photo_gallery', 'imageSelector')};" href="javascript:;"> +Add photo(s)</a>
					| 					
				<a onclick="if (confirmPhotoBlocks()) {LimnGallery.SaveGallery('editGallery.jsp?galleryItemIndex=${galleryItemIndex}&reorder=true');}; return false;" href="javascript:;">Reorder</a>
			</div>
			</td>
		<td>
			<div class="paging right">
				<c:choose>
					<c:when test="${galleryItemIndex eq 0}">
						<a href="javascript:;" class="disabledlink" >First</a>&nbsp;|&nbsp;
						<a href="javascript:;" class="disabledlink" >< Prev</a>
					</c:when>
					<c:otherwise>
						<a href="javascript:;" class="next" onClick="LimnGalleryItem.FilterByStartIndex('0', '${id}')">First</a>&nbsp;|&nbsp;
						<a href="javascript:;" class="next" onClick="LimnGalleryItem.FilterByStartIndex('${galleryItemIndex - total}', '${id}')">< Prev</a>
					</c:otherwise>
				</c:choose>		
    			|
   				<c:choose>
					<c:when test="${size < total}">
						${galleryItemIndex}&nbsp;of&nbsp;<span id="num_of_images${position}">${size}</span>  
					</c:when>
					<c:otherwise>	
						<select name="selectImageIndex" id="selectImageIndex" onChange="LimnGalleryItem.FilterByStartIndex(this.options[selectedIndex].value, '${id}')" >
						<c:forEach begin="0" end="${(size-1) / total}" varStatus="index" >
							<option value="${(total*index.count) - total}" <c:if test="${(total*index.count) - total eq galleryItemIndex}">SELECTED</c:if>>
								${(total*index.count) - total + 1}&nbsp;-&nbsp;
								<c:choose>
									<c:when test="${galleryItemIndex + (total*index.count) > size}">
										${size}
									</c:when>
									<c:otherwise>
										${(total*index.count)}
									</c:otherwise>
								</c:choose>		
							</option>
						</c:forEach>				
						</select>
						&nbsp;of&nbsp;<span id="num_of_images${position}">${size}</span>
            			|
						<c:choose>
							<c:when test="${galleryItemIndex + total >=  size}">
								<a href="javascript:;" class="disabledlink">Next ></a>&nbsp;|&nbsp;
								<a href="javascript:;" class="disabledlink">Last</a>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${(size % total) eq 0}">
										<c:set var="lastIndex" value="${size - total}" scope="page" />
									</c:when>
									<c:otherwise>
									<c:set var="lastIndex" value="${size - (size % total)}" scope="page" />
									</c:otherwise>
								</c:choose>	
								<a href="javascript:;" class="next" onClick="LimnGalleryItem.FilterByStartIndex('${galleryItemIndex + total}', '${id}')">Next ></a>&nbsp;|&nbsp;
								<a href="javascript:;" class="next" onClick="LimnGalleryItem.FilterByStartIndex('${lastIndex}', '${id}')">Last</a>
							</c:otherwise>
						</c:choose>		
					</c:otherwise>	
				</c:choose>
			</div>
		</td>
	</tr>
</tbody>
</table>
	