<%@tag import="com.ceg.online.api.HelperFactory"%><%@
attribute name="attributeName" type="java.lang.String" required="true"%><%@
attribute name="propertyName" type="java.lang.String" required="true"%>
<%
request.setAttribute(attributeName, HelperFactory.getPropertyHelper().getPropertyByName(propertyName));
%>