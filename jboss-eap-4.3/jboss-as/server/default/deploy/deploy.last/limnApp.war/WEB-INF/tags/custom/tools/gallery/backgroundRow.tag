<%@ tag import="com.ceg.online.limn.beans.ImageBean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ attribute name="background" type="com.ceg.online.limn.beans.ImageBean" required="true"%>
<tr>
	<td class="label"><p>Background</p></td>
	<td class="field">
		<div id="displayBackground" class="imageFloat">
            <c:if test="${not empty background}">
              	<image:imageSrc attributeName="imageSrcBackground" image="${background}" />      
		        <img src="${imageSrcBackground}" class="thumb thumbnail" />
		        <a class="deletephotolink" onclick="LimnGallery.RemoveImage('displayBackground', 'backgroundContentID');" href="javascript:;">[x]</a>
            </c:if>
        </div>
        <input type="hidden" name="backgroundLink" value="${background.link}" id="backgroundLink">
		<input type="button" name="x" value="choose..." onclick="LimnGallery.PopUp('?callingForm=galleryForm&property=backgroundContentID&displayPreview=displayBackground&numImages=1&displayProperty=backgroundLink', 'imageSelector')" class="choose">
		<div>
		<input type="hidden" name="backgroundContentID" value="${background.id}" id="backgroundContentID">
		</div>
	</td>
</tr>