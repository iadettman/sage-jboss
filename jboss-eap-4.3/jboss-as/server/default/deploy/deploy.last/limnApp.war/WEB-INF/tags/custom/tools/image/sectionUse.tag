<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ attribute name="image" type="com.ceg.online.limn.beans.ImageBean" required="false" %>
<getters:getDBProperty propertyName="tools.config.site.image.usage" attributeName="siteUsage" />
<div>
	<h4>
		Image Usageaaa
		<p class="descr">Share with these sections</p>
	</h4>
	<c:forEach items="${siteUsage.value}" var="section" >
		<c:choose>
			<c:when test="${image.id != null}">
				<c:forEach items="${image.sectionUse}" var="imageSection" >
					<c:if test="${imageSection eq section}">
						<c:set var="checkedVar" value="checked" />
					</c:if>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:set var="checkedVar" value="checked" />
			</c:otherwise>
		</c:choose>
		<div style="text-transform : capitalize;"><input type="checkbox" ${checkedVar} name="sectionUsage" value="${section}" />${section}</div>
		<c:set var="checkedVar" value="" />
	</c:forEach>
</div>