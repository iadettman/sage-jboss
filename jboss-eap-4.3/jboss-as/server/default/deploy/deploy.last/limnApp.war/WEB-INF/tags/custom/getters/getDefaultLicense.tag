<%@ tag import="com.ceg.online.limn.helpers.AgencyHelper" %>
<%@ tag import="com.ceg.online.limn.dto.AgencyLicense" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="agency" type="com.ceg.online.limn.dto.AgencyLU" required="true" %>
<%
if (agency != null && agency.getAgencyLicense() != null) {
	for (AgencyLicense license : agency.getAgencyLicense()) {
		if (license.isDefaultLicense()) {
			request.setAttribute(attributeName, license);
		}
	}
}
%>