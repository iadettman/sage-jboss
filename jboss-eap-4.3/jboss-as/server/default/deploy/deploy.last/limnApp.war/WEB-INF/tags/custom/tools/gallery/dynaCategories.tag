<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="gallery" type="com.ceg.online.limn.beans.impl.GalleryImpl" required="true"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<ul id="gallery_item_categories" class="dynitemlist chosenitems">
	<c:forEach items="${gallery.categories}" var="category" varStatus="status">
		<gallery:parseCategoryRow attributeName="parsedHtml"
			 cat="${category}" editionid="${gallery.edition.id}" editionDescr="${gallery.edition.descr}"/>
		<gallery:dynaCategory id="${category.id}" name="${parsedHtml}" index="${status.count - 1}"/>
	</c:forEach>
</ul>