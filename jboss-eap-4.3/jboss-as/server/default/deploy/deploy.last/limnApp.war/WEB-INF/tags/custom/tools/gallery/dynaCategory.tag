<%@ attribute name="id" type="java.lang.String" required="true"%>
<%@ attribute name="name" type="java.lang.String" required="true"%>
<%@ attribute name="index" type="java.lang.String" required="true"%>
<li id="category_${id}" class="gallery_entry_category">
	${name}
	<a class="removelink" onclick="removeThis(this)" title="Remove"></a>	
	<input type="hidden" name="category" value="${id}" id="category"/>
</li>