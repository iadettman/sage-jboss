<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="originalStr" type="java.lang.String" required="true" %>
<%
	if (originalStr != null && !originalStr.trim().equals("")) {
		request.setAttribute(attributeName, originalStr.replaceAll("\"", "").replaceAll("'", "\\\\'"));
	}
%>