<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ attribute name="gallery" type="com.ceg.online.limn.beans.GalleryBean" required="true"%>
<%@ attribute name="galleryItemIndex" type="java.lang.Integer" required="true"%>
<%@ attribute name="total" type="java.lang.Integer" required="false"%>
<c:if test="${empty total}" >
	<c:set var="total" value="30"/>
</c:if>
<c:if test="${not empty gallery}">
	<getters:getGalleryImageCount attributeName="size" galleryId="${gallery.id}"/>
	<c:choose>
		<c:when test="${galleryItemIndex+total < size}">
			<c:set var="end" value="${galleryItemIndex+total}" />
		</c:when>
		<c:otherwise>
			<c:set var="end" value="${size}" />		
		</c:otherwise>
	</c:choose>	
	<getters:getGalleryItems attributeName="galleryItems" id="${gallery.id}"
		start="${galleryItemIndex}" end="${galleryItemIndex+total}" />
</c:if>
<tr>
	<td class="label">
		<p>Photos<span class="req">*</span></p>
	</td>
	<td class="field">
	<div id="gallery_item_entries">
		<div class="controls">
			<a class="editpagefns" href="javascript:;" onclick="LimnGalleryItem.SaveOrder('${gallery.id}','${galleryItemIndex}');">
				Save Order
			</a>
 			OR  
			<a href="editGallery.jsp?id=${gallery.id}">Cancel</a>
		</div>
		<div class="paging center">  ${galleryItemIndex} - ${end} of ${size}</div>
		<div id="reorderPhotos">
			<c:forEach items="${galleryItems}" var="galleryItem" varStatus="status" >
				<c:if test="${galleryItem.status ne 'deleted'}" >
					<image:imageSrc attributeName="galleryItemSrc" image="${galleryItem.image}" /> 																								
					<div id="photoGalleryReorder_${galleryItem.id}" class="photoblock" style="position: relative;">
						<div class="photoblock-item">
                            <div class="drag"><span class="brick handle">DRAG</span></div>
                            <div class="photo">
                                <input type="hidden" value="${galleryItem.id}"
									name="galleryItemId" id="galleryItemId"/>
							    <img class="thumb vertical" src="${galleryItemSrc}"/></div>
                            <div class="descr"><span>${galleryItem.title}</span></div>
                            <div class="clear"></div>
                        </div>
					</div>
				</c:if>
			</c:forEach>			
		</div>
		<script type="text/javascript">
			Sortable.create('reorderPhotos', {tag:'div', handle:'handle', constraint:'vertical'});
		</script>
		<div class="controls">
			<a class="editpagefns" href="javascript:;" onclick="LimnGalleryItem.SaveOrder('${gallery.id}','${galleryItemIndex}');">
				Save Order
			</a>
 			OR  
			<a href="editGallery.jsp?id=${gallery.id}">Cancel</a>
		</div>
	</div>
	</td>
</tr>
