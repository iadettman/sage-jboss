<%@ tag import="com.ceg.online.helpers.EditionHelper"%>
<%@ attribute name="attributeName" type="java.lang.String" required="true"%>
<%@ attribute name="editionId" type="java.lang.String" required="true"%>
<%
	if(editionId != null && !editionId.trim().equals("")) {
		request.setAttribute(attributeName, EditionHelper.getEditionById(Integer.parseInt(editionId)));
	}
%>