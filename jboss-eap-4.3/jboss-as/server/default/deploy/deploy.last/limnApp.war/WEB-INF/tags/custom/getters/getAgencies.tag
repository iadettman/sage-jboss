<%@ tag import="java.util.ArrayList" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.Hashtable" %>
<%@ tag import="com.ceg.online.limn.helpers.AgencyHelper" %>
<%@ attribute name="attributeName" type="java.lang.String" required="true" %>
<%@ attribute name="size" type="java.lang.Integer" required="false" %>
<%@ attribute name="start" type="java.lang.Integer" required="false" %>
<%@ attribute name="sortBy" type="java.lang.String" required="false" %>
<%@ attribute name="status" type="java.lang.String" required="false" %>
<%
Hashtable<String, String> searchParams = new Hashtable<String, String>();
final Integer DEFAULT_SIZE = 5000;

start = start != null && start >= 0 ? start : 0;
size = size != null && size >= 0 ? size : DEFAULT_SIZE;

if (sortBy != null && !sortBy.trim().equals("")) {
	searchParams.put("sortBy", sortBy);
}
if (status != null && !status.trim().equals("")) {
	searchParams.put("status", status);
}
request.setAttribute(attributeName, AgencyHelper.searchAgencies(searchParams, start, size));
%>