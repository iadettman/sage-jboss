<%@ tag import="com.ceg.online.toolbox.CookieUtil"%>
<%@ tag import="com.ceg.properties.helpers.PropertyHelper" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
	<c:when test="${empty param.edition}">
		<%-- If the edition parameter is empty, try to get it from the cookie --%>
		<%
			String authEditionCookie = PropertyHelper.getPropertyByName("tools.config.cookies.edition").getValue();
			String edition = CookieUtil.getCookie(request, authEditionCookie);
			request.setAttribute("edition", edition);
		%>
		<c:if test="${empty edition}">
			<%-- If that fails, then set it to the default edition --%>
			<c:set var="edition" value="us" scope="request"/>
		</c:if>
	</c:when>
	<c:otherwise>
		<%-- Otherwise set it to the passed edition --%>
		<c:set var="edition" value="${param.edition}" scope="request"/>
	</c:otherwise>
</c:choose>