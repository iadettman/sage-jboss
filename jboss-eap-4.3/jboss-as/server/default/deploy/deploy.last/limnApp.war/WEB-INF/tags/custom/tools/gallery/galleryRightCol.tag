<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="tools" tagdir="/WEB-INF/tags/custom/tools"%>
<%@ taglib prefix="gallery" tagdir="/WEB-INF/tags/custom/tools/gallery"%>
<%@ attribute name="gallery" type="com.ceg.online.limn.beans.GalleryBean" required="true" %>
<%@ attribute name="edition" type="java.lang.String" required="true" %>
<c:if test="${not empty gallery}">
<tools:escapeQuotes attributeName="galleryTitle" originalStr="${gallery.title}" />
<div class="helpinfo">
	<ul>
		<li>Published On:
			<c:choose>
				<c:when test="${!empty gallery.publishDate}">
		    		<fmt:formatDate value="${gallery.publishDate}" 
		    			type="both" pattern="M/d/yy, h:mma" />
				</c:when>
				<c:otherwise>
			 		Not published
	    		</c:otherwise>
			</c:choose>	
		</li>
		<li>Last Modified:
			<c:choose>
				<c:when test="${!empty gallery.lastModDate}">
		    		<fmt:formatDate value="${gallery.lastModDate}" 
		    			type="both" pattern="M/d/yy, h:mma" />		    		
				</c:when>
				<c:otherwise>
			 		N/A
	    		</c:otherwise>
			</c:choose>	
		</li>
		<li>
			<c:choose>
			     <c:when test="${(gallery.status eq 'draft')}">
   	 	  	    	<span class="brick draft">draft</span>
       			</c:when>
		    	<c:when test="${(gallery.status eq 'edited')}">
  	  	  		   	<span class="brick edited">edited</span>
         		</c:when>
   		    	<c:when test="${(gallery.status eq 'published')}">
  		  	  	   	<span class="brick published">published</span>
         		</c:when>        
         		<c:when test="${(gallery.status eq 'hidden')}">
  		  	  	   	<span class="brick hidden">hidden</span>
         		</c:when>      			           			
			</c:choose>
		</li>
	</ul>
</div>
<getters:getToolsEditions attributeName="editions"/>

<ul class="editpagefns">
	<li>  <!--  Publishin/Expire -->
     	<div class="listcontrols">
			<a class="editpagefns" onclick="LimnGallery.UpdateStatus('deleted', '${gallery.id}', '${galleryTitle}')" href="javascript:;">
				<img width="9" height="10" border="0" src="/limn/includes/images/icon_delete.gif"/>
				Delete
			</a>
			<c:if test="${fn:length(editions) > 1}" >
			<a class="editpagefns" href="javascript:;" onclick="if (confirmAction('duplicateGallery')) {LimnGallery.SaveGallery('duplicateGallery.jsp?');}; return false;">
				Duplicate Gallery
			</a>
			</c:if>
     	</div>
	</li>
</ul>
</c:if>

<c:if test="${fn:length(editions) > 1}" >
<div class="categories">
	<h4>
		Editions
		<p class="descr">Share with these editions</p>
	</h4>
	<c:forEach items="${editions}" var="ed" >
		<c:if test="${ed.edition ne edition}" >
			<c:set var="selected" value="" />
			<c:choose>
				<c:when test="${empty gallery.id && edition eq 'us' 
					&& (ed.edition eq 'ca' || ed.edition eq 'uk' || ed.edition eq 'au')}" >
					<c:set var="selected" value="checked='checked'" />
				</c:when>
				<c:otherwise>
					<c:forEach items="${gallery.sharedEditionIds}" var="sharedIds" >
						<c:if test="${sharedIds eq ed.id}" >
							<c:set var="selected" value="checked='checked'" />
						</c:if>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			<div class="choice">
				<input type="checkbox" ${selected} onkeypress="return disableEnter(this,event)" value="${ed.id}" name="selectedEditions"/>
				<img src="/limn/includes/images/flag_${ed.edition}.gif"/>
				${ed.descr} 
			</div>
		</c:if>
	</c:forEach>
</div>
</c:if>

<div class="categories">
	<gallery:galleryCategories gallery="${gallery}" edition="${edition}" />
</div>

<div>
	<h4>
		Keywords
		<p class="descr">Comma separated. Used for search engine metatags and initial user tags</p>
	</h4>
	<textarea rows="6" cols="40" name="keywords" >${gallery.keywordStr}</textarea>
	<h4>
		<p class="descr">
			Ad Keywords:
			<input class="textfield adkeyword" type="text" value="${gallery.adKeywordStr}" name="adKeywords"/>
		</p>
	</h4>
	<h4>
		<p class="descr">
			Display Options:
			<input class="textfield adkeyword" type="text" value="${gallery.displayOptions}" name="displayOptions"/>
		</p>
	</h4>
	<h4>
		<p class="descr">
			Unique name
			<input class="textfield adkeyword" type="text" value="${gallery.uniqueName}" size="40" name="uniqueName"/>
		</p>
	</h4>
</div>