<%@ taglib prefix="image" tagdir="/WEB-INF/tags/custom/tools/image"%>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn"   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="size" type="java.lang.Integer" required="true"%>
<%@ attribute name="images" type="java.util.List" required="true"%>
<%@ attribute name="status" type="java.lang.String" required="false"%>
<%@ attribute name="sortBy" type="java.lang.String" required="false"%>
<%@ attribute name="imageIndex" type="java.lang.String" required="false"%>
<div id="image_entries" >	  
<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr>
	<td class="leftcol">
		<!--  Paging -->
	  	<table width="100%" cellpadding="0" cellspacing="0" border="0">
 			<tr>
	       	<td>
   				<div class="paging">
   			    	<strong>Show:</strong>
		       	 	<select name="select" class="" onChange="LimnImage.FilterByStatus(this.options[selectedIndex].text)">
	            		<option>(all)</option>
                 	  	<option <c:if test="${status eq 'Expired'}">selected</c:if>>Expired</option>
                	</select> 
                	images&nbsp;
                </div>
 		   	</td>
   		   	<td align="right">			 
				<image:imagePaging size="${imageSize}" imageIndex="${imageIndex}" status="${status}" sortBy="${sortBy}" />	     			    			
   		   	</td>
     		</tr>
		</table>
     	 <!--  End Paging -->
     	 <!--  Publishin/Expire -->
      	 <div class="listcontrols">
      	  	<a class="newbtn smallerbtn" title="Expire Images" href="javascript:;" onClick="LimnImage.UpdateStatus('expired');" >Expire Now</a>      	 
      	  	<a class="newbtn smallerbtn" title="Delete Images" href="javascript:;" onClick="LimnImage.UpdateStatus('deleted');" >Delete</a>      	 
			&nbsp;&nbsp;Check: <a href="javascript:void(0)" onClick="LimnImage.CheckAll();">All</a>, <a href="javascript:void(0)" onClick="LimnImage.UncheckAll();">None</a>
     	  </div>
     	  <!--  End of Publishin/Expire -->      
	  	<image:imageListings images="${images}" status="${status}" sortBy="${sortBy}"/>
	  <!--  Paging -->
	  <table width="100%" cellpadding="0" cellspacing="0" border="0">
 			 <tr>
	       <td>
   			 <div class="paging">
   			     <strong>Show:</strong>
	        	 <select name="select" class="" onChange="LimnImage.FilterByStatus(this.options[selectedIndex].text)">
	            	  <option>(all)</option>
                  	  <option <c:if test="${status eq 'Expired'}">selected</c:if>>Expired</option>
                 </select>
                   images&nbsp;
                </div>
 			   </td>
   		   <td align="right">
				<image:imagePaging size="${imageSize}" imageIndex="${imageIndex}" status="${status}" sortBy="${sortBy}" />	     			    			
   		   </td>
		</tr>
    </table>
    <!--  End Paging -->
    </td>
	<td class="configbar rightcol">
		<a href="${pageContext.request.contextPath}/admin/uploadImages.jsp" class="newbtn">Add Image(s)</a>
		<a href="${pageContext.request.contextPath}/admin/agencies.jsp" class="newbtn">Edit Agencies</a>
	</td>
    </tr>
</table>
</div>