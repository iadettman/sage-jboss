<%@page import="java.util.Map"%>
<%@ page import="com.ceg.online.toolbox.CookieUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="getters" tagdir="/WEB-INF/tags/custom/getters"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<getters:getDBProperty attributeName="pageTitle" propertyName="tools.ui.labels.page.title"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>${pageTitle.value}</title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="imagetoolbar" content="no">
<%
    //request.getSession().invalidate();
    pageContext.setAttribute("sage_username", request.getRemoteUser());
%>
<link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
<getters:getDBProperty attributeName="site" propertyName="tools.config.site"/>
<style type="text/css">
	body.login caption {font-family:Arial, Helvetica, sans-serif; font-weight:bold; letter-spacing:-.04em; font-size:2em; text-align:center; color:#fff; margin:0 auto; padding:.3em 0; background:#196d85 url(${pageContext.request.contextPath}/includes/images/${site.value}_login_box.gif) center repeat-x;}
</style>

<script language="JavaScript" type="text/javascript">

    //check to make sure this page isn't nested in an iFrame
    if (top != self)
    {
        top.location.href = location.href;
    }

/* Initialize page */
window.onload = function() {
	document.forms.loginform.username.focus();// focus cursor on login field

}

function changeClass(elem,theclassname) {
	if( document.getElementById ) {
		document.getElementById(elem).className = theclassname;
		};
}

function showerrmsg(errid) {
	if( document.getElementById ) {
		document.getElementById(errid).style.display = "block";
		};
}

function hideerrmsg(errid) {
	if( document.getElementById ) {
		document.getElementById(errid).style.display = "none";
		};
}
</script>
<getters:getDBProperty attributeName="loginTitle" propertyName="tools.ui.labels.login.title"/>

</head><body class="login">
<table class="verttable" border="0" cellpadding="0" cellspacing="1">
<form name="loginform" id="loginform" action="${pageContext.request.contextPath}/j_security_check" method="post">
<tbody><tr><td>&nbsp;</td><td>
	<div class="top_box">&nbsp;

	<div class="table_box">
	<table class="form_table" id="form_table" border="0" cellpadding="0" cellspacing="0">
		<caption>${loginTitle.value}</caption>
        <tbody>
        <tr><td colspan="2">&nbsp;
		<c:if test="${param.error}">
        	<div class="form_err" id="msg_err_empty"><p><strong>That log in was incorrect.</strong><br>Please check your info and try again. (<a href="javascript:alert('Detailed and technical error message goes here.')">details</a>)</p></div>
        </c:if>
		</td></tr>
        <tr>
        	<td class="label">Username</td>
        	<td class="field"><input name="j_username" id="username" class="bigfield" type="text" value="${sage_username}"></td>
        </tr>
		<tr><td class="label">Password</td><td class="field"><input name="j_password" class="bigfield" type="password"></td></tr>


        <tr><td>&nbsp;</td><td style="padding-top: 1em;"><input value="Login" class="newbtn" type="submit"></td></tr>
		<tr><td>&nbsp;</td><td style="padding-top: 2.2em;"><a href="javascript:changeClass('problem_text','help_page_show');changeClass('form_table','help_page');">Having problems logging in?</a></td></tr>
	</tbody></form></table>
	<div class="help_page" id="problem_text" style="color: rgb(0, 0, 0);">
		<p><strong>Having problems logging in?</strong></p>
		<p>Make sure you enter the exact username and password that you use to log into
		   your E! Entertainment computer. You only have 3 chances to sign into a
		   particular account before it is locked for security reasons.<br>
		   <br>If you continue to have problems,<br>contact the IT Help Desk at <strong>(323) 954-2589</strong>
		   <br>or email: <a href="mailto:it-helpdesk@eentertainment.com">it-helpdesk@eentertainment.com</a>
		</p>

		<p style="margin: 0pt; padding: 1.3em 0pt 0pt; text-align: center;"><a href="javascript:changeClass('problem_text','help_page');changeClass('form_table','form_table');"> Back to Log in</a></p>
	</div>
	</div>
	<div class="bottom_box">
	</div>
	</div>
</td><td>&nbsp;</td></tr>
<tr><td></td><td colspan="2"><p>Site Tools build timestamp: ${appBuildDate}</p></td></tr>

</tbody></table>

</body></html>
