<%@ page isErrorPage="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% response.setStatus(HttpServletResponse.SC_FORBIDDEN); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Access to the requested resource has been denied</title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="imagetoolbar" content="no">

<link href="${pageContext.request.contextPath}/includes/css/main.css?x=${version.value}" rel="stylesheet" type="text/css">
<style type="text/css">
	body.login caption {font-family:Arial, Helvetica, sans-serif; font-weight:bold; letter-spacing:-.04em; font-size:2em; text-align:center; color:#fff; margin:0 auto; padding:.3em 0; background:#196d85 url(/blogtools/includes/images/eonline_login_box.gif) center repeat-x;}
</style>
</head>
<body class="login">
<table cellspacing="1" cellpadding="0" border="0" class="verttable">
<tbody>
<tr><td>
	<div class="top_box">&nbsp;
		<div class="table_box">
			<div class="help_page_show" id="problem_text">
				<p><strong>Access to the requested resource has been denied.</strong></p>
				<br />
				The page is not accessible based on your privileges.
				If you believe this to be an error, report the issue to <a href="mailto:sage-support@comcastnets.com">sage-support@comcastnets.com</a>
				<br/><br/><br/>
				<a href="/blogtools/admin/">Return to tools</a>
				<br/><br/>
			</div>
		</div>
		<div class="bottom_box">
		</div>
	</div>
</td></tr>
</tbody></table>
</body></html>