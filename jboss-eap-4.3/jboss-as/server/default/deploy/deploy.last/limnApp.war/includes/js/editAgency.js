// JavaScript Document

jQuery(document).ready(function(){
	
	var myValidator = jQuery('form#agencyForm').validate();	
	
	jQuery('a.action-form-submit').click(function(){
		if (jQuery('form#agencyForm').valid() && LimnAgency.IsValidExpirationDate()) {
			LimnAgency.SaveAgency('published');
		}											  
	});
});